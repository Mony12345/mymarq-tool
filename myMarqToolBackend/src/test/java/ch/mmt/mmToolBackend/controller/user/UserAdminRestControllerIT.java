package ch.mmt.mmToolBackend.controller.user;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.mmt.mmToolBackend.BaseIT;
import ch.mmt.mmtoolbackend.MmtBackendApplication;
import ch.mmt.mmtoolbackend.api.FilterAPIs;
import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.api.model.UserModel;
import ch.mmt.mmtoolbackend.api.search.model.UserSearchModel;
import ch.mmt.mmtoolbackend.datatypes.Language;
import ch.mmt.mmtoolbackend.datatypes.Role;
import ch.mmt.mmtoolbackend.domain.model.User;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = { MmtBackendApplication.class })
@TestPropertySource(locations = { "classpath:integrationtest.properties" })
public class UserAdminRestControllerIT extends BaseIT {

	protected static final String USER_API_PATH = BASE_ADMIN_API_PATH + "user/";

	private List<User> users = new ArrayList<>();

	private int adminsCount = 0;

	private int companyCount = 0;

	private int consultantCount = 0;

	@Before
	@Override
	public void setup() throws ServiceException {
		super.setup();
		adminsCount++; //super method create an admin test user

		//create test users
		for (int i = 0; i < RandomUtils.nextInt(1, 21); i++) {
			User user = createRandomUser();
			users.add(user);

			if (user.getRole().isAdmin()) {
				adminsCount++;
			}
			if (user.getRole().isCompany()) {
				companyCount++;
			}
			if (user.getRole().isConsultant()) {
				consultantCount++;
			}
		}
	}

	@After
	@Override
	public void cleanup() throws ServiceException {
		userRepository.deleteAll(users);
		super.cleanup();
	}

	@Test
	public void accessAPIWithoutTokenTest() throws Exception {
		mockMvc.perform(post(USER_API_PATH + "/search")).andExpect(status().isUnauthorized());
	}

	@Test
	public void accessAPIWithNoAdminUser() throws Exception {
		User user = null;
		try {
			user = createRandomUser(Role.COMPANY);
			ObjectMapper mapper = new ObjectMapper();
			final String accessToken = obtainAccessToken(user);

			mockMvc.perform(post(USER_API_PATH + "/search")
					.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
					.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isForbidden());
		} finally {
			if (user != null) {
				userRepository.delete(user);
			}
		}
	}

	@Test
	public void testCreateGetUpdateAndDeleteUser() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		final String accessToken = obtainAccessToken();

		//ACT 1
		UserModel newUser = new UserModel();
		newUser.email = generateRandomEmail();
		newUser.password = TEST_PASSWORD;
		newUser.language = Language.ITA;
		newUser.role = Role.COMPANY;

		String newUserJson = mapper.writeValueAsString(newUser);

		ResultActions results = mockMvc
				.perform(post(USER_API_PATH)
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
						.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(newUserJson))
				.andExpect(status().isCreated());

		String location = results.andReturn().getResponse().getHeader(HttpHeaders.LOCATION);
		String[] locationSplitted = location.split("/");
		String newUserUUID = locationSplitted[locationSplitted.length - 1];

		//ACT 2
		String op = "replace";
		String path = "firstname";
		String firstname = RandomStringUtils.randomAlphabetic(20);

		String patch = "[{\"op\":\"" + op + "\",\"path\":\"/" + path + "\",\"value\":\"" + firstname + "\"}]";

		mockMvc.perform(
				patch(USER_API_PATH + newUserUUID).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("firstname", Matchers.is(firstname)));

		//ACT 3
		op = "replace";
		path = "role";
		String role = Role.CONSULTANT.toString();

		patch = "[{\"op\":\"" + op + "\",\"path\":\"/" + path + "\",\"value\":\"" + role + "\"}]";

		mockMvc.perform(
				patch(USER_API_PATH + newUserUUID).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("role", Matchers.is(role)));

		//ACT 4
		op = "replace";
		path = "email2";
		String email = generateRandomEmail();

		patch = "[{\"op\":\"" + op + "\",\"path\":\"/" + path + "\",\"value\":\"" + email + "\"}]";

		mockMvc.perform(
				patch(USER_API_PATH + newUserUUID).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isBadRequest());

		//ACT5
		results = mockMvc
				.perform(get(USER_API_PATH + newUserUUID)
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken))
				.andExpect(status().isOk());

		UserModel user = mapper.readValue(results.andReturn().getResponse().getContentAsString(),
				new TypeReference<UserModel>() {
				});

		Assert.assertNotNull(user);
		Assert.assertEquals(newUser.email, user.email);

		//ACT6
		results = mockMvc
				.perform(delete(USER_API_PATH + newUserUUID)
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken))
				.andExpect(status().isNoContent());
	}

	@Test
	public void testDeleteHimself() throws Exception {
		final String accessToken = obtainAccessToken();
		//ACT6
		mockMvc.perform(delete(USER_API_PATH + testUser.getUuid())
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken))
				.andExpect(status().isForbidden());

	}

	@Test
	public void testSearchUsers() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		final String accessToken = obtainAccessToken();

		// ACT 1
		UserSearchModel searchModel = new UserSearchModel();
		String searchModelJson = mapper.writeValueAsString(searchModel);

		ResultActions results = mockMvc
				.perform(post(USER_API_PATH + "/search")
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
						.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
						.header(FilterAPIs.RANGE_HEADER_NAME, "members=0-30")
						.content(searchModelJson))
				.andExpect(status().isOk());

		List<UserModel> respUsers = mapper.readValue(results.andReturn().getResponse().getContentAsString(),
				new TypeReference<List<UserModel>>() {
				});

		Assert.assertNotNull(respUsers);
		Assert.assertEquals(users.size() + 1, respUsers.size()); //+ test user

		// ACT 2
		searchModel = new UserSearchModel();
		searchModel.email = users.get(0).getEmail();
		searchModelJson = mapper.writeValueAsString(searchModel);

		results = mockMvc
				.perform(post(USER_API_PATH + "/search")
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
						.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
						.header(FilterAPIs.RANGE_HEADER_NAME, "members=0-30")
						.content(searchModelJson))
				.andExpect(status().isOk());

		respUsers = mapper.readValue(results.andReturn().getResponse().getContentAsString(),
				new TypeReference<List<UserModel>>() {
				});

		Assert.assertNotNull(respUsers);
		Assert.assertEquals(1, respUsers.size());

		// ACT 3
		searchModel = new UserSearchModel();
		searchModel.role = Role.CONSULTANT;
		searchModelJson = mapper.writeValueAsString(searchModel);

		results = mockMvc
				.perform(post(USER_API_PATH + "/search")
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
						.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
						.header(FilterAPIs.RANGE_HEADER_NAME, "members=0-30")
						.content(searchModelJson))
				.andExpect(consultantCount > 0 ? status().isOk() : status().isNoContent());

		if (consultantCount > 0) {
			respUsers = mapper.readValue(results.andReturn().getResponse().getContentAsString(),
					new TypeReference<List<UserModel>>() {
					});

			Assert.assertNotNull(respUsers);
			Assert.assertEquals(consultantCount, respUsers.size());
		}

		// ACT 4
		searchModel = new UserSearchModel();
		searchModel.role = Role.COMPANY;
		searchModelJson = mapper.writeValueAsString(searchModel);

		results = mockMvc
				.perform(post(USER_API_PATH + "/search")
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
						.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
						.header(FilterAPIs.RANGE_HEADER_NAME, "members=0-30")
						.content(searchModelJson))
				.andExpect(companyCount > 0 ? status().isOk() : status().isNoContent());

		if (companyCount > 0) {
			respUsers = mapper.readValue(results.andReturn().getResponse().getContentAsString(),
					new TypeReference<List<UserModel>>() {
					});

			Assert.assertNotNull(respUsers);
			Assert.assertEquals(companyCount, respUsers.size());
		}

		// ACT 5
		searchModel = new UserSearchModel();
		searchModel.role = Role.ADMIN;
		searchModelJson = mapper.writeValueAsString(searchModel);

		results = mockMvc
				.perform(post(USER_API_PATH + "/search")
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
						.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
						.header(FilterAPIs.RANGE_HEADER_NAME, "members=0-30")
						.content(searchModelJson))
				.andExpect(adminsCount > 0 ? status().isOk() : status().isNoContent());

		if (adminsCount > 0) {
			respUsers = mapper.readValue(results.andReturn().getResponse().getContentAsString(),
					new TypeReference<List<UserModel>>() {
					});

			Assert.assertNotNull(respUsers);
			Assert.assertEquals(adminsCount, respUsers.size()); //+ test user
		}
	}

}
