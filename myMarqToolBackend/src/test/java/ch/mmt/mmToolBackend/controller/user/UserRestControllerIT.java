package ch.mmt.mmToolBackend.controller.user;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.mmt.mmToolBackend.BaseIT;
import ch.mmt.mmtoolbackend.MmtBackendApplication;
import ch.mmt.mmtoolbackend.api.model.UserModel;
import ch.mmt.mmtoolbackend.datatypes.Role;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = { MmtBackendApplication.class })
@TestPropertySource(locations = { "classpath:integrationtest.properties" })
public class UserRestControllerIT extends BaseIT {

	protected static final String USER_API_PATH = BASE_MMT_API_PATH + "user/";

	@Test
	public void accessAPIWithoutTokenTest() throws Exception {
		mockMvc.perform(post(USER_API_PATH)).andExpect(status().isUnauthorized());
	}

	@Test
	public void testGetAndUpdateUser() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		final String accessToken = obtainAccessToken();

		//ACT 1
		String op = "replace";
		String path = "firstname";
		String firstname = RandomStringUtils.randomAlphabetic(20);

		String patch = "[{\"op\":\"" + op + "\",\"path\":\"/" + path + "\",\"value\":\"" + firstname + "\"}]";

		mockMvc.perform(
				patch(USER_API_PATH + testUser.getUuid()).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("firstname", Matchers.is(firstname)));

		//ACT 2
		op = "replace";
		path = "firstname";
		firstname = RandomStringUtils.randomAlphabetic(20);

		patch = "[{\"op\":\"" + op + "\",\"path\":\"/" + path + "\",\"value\":\"" + firstname + "\"}]";

		mockMvc.perform(
				patch(USER_API_PATH + UUID.randomUUID()).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isForbidden());

		//ACT 3
		op = "replace";
		path = "role";
		String role = Role.CONSULTANT.toString();

		patch = "[{\"op\":\"" + op + "\",\"path\":\"/" + path + "\",\"value\":\"" + role + "\"}]";

		mockMvc.perform(
				patch(USER_API_PATH + testUser.getUuid()).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isBadRequest());

		//ACT 4
		ResultActions results = mockMvc
				.perform(get(USER_API_PATH + testUser.getUuid())
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken))
				.andExpect(status().isOk());

		UserModel user = mapper.readValue(results.andReturn().getResponse().getContentAsString(),
				new TypeReference<UserModel>() {
				});

		Assert.assertNotNull(user);
		Assert.assertEquals(testUser.getEmail(), user.email);

		//ACT 5
		mockMvc
				.perform(get(USER_API_PATH + UUID.randomUUID())
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken))
				.andExpect(status().isForbidden());

	}

	@Test
	public void testDeleteHimself() throws Exception {
		final String accessToken = obtainAccessToken();
		//ACT6
		mockMvc.perform(delete(USER_API_PATH + testUser.getUuid())
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken))
				.andExpect(status().isMethodNotAllowed());

	}
}
