package ch.mmt.mmToolBackend.controller.consultation;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.mmt.mmToolBackend.BaseIT;
import ch.mmt.mmtoolbackend.MmtBackendApplication;
import ch.mmt.mmtoolbackend.api.FilterAPIs;
import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.api.model.ConsultationModel;
import ch.mmt.mmtoolbackend.api.search.model.ConsultationSearchModel;
import ch.mmt.mmtoolbackend.domain.model.Consultation;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = { MmtBackendApplication.class })
@TestPropertySource(locations = { "classpath:integrationtest.properties" })
public class ConsultationRestControllerIT extends BaseIT {

	protected static final String PROFILE_API_PATH = BASE_MMT_API_PATH + "consultation/";

	private List<Consultation> consultations = new ArrayList<>();

	@Before
	@Override
	public void setup() throws ServiceException {
		super.setup();

		//create  test profiles
		for (int i = 0; i < RandomUtils.nextInt(1, 21); i++) {
			Consultation profile = createRandomConsultation();
			consultations.add(profile);
		}
	}

	@After
	@Override
	public void cleanup() throws ServiceException {
		consultationRepository.deleteAll(consultations);
		super.cleanup();
	}

	@Test
	public void accessAPIWithoutTokenTest() throws Exception {
		mockMvc.perform(post(PROFILE_API_PATH + "/search")).andExpect(status().isUnauthorized());
	}

	@Test
	public void testCreateGetUpdateAndDeleteProfile() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		final String accessToken = obtainAccessToken();

		//ACT 1
		ConsultationModel newprofile = new ConsultationModel();
		newprofile.email = generateRandomEmail();
		newprofile.firstname = TEST_USER_FIRSTNAME;
		newprofile.lastname = TEST_USER_LASTNAME;

		String newprofileJson = mapper.writeValueAsString(newprofile);

		ResultActions results = mockMvc
				.perform(post(PROFILE_API_PATH)
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
						.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(newprofileJson))
				.andExpect(status().isCreated());

		String location = results.andReturn().getResponse().getHeader(HttpHeaders.LOCATION);
		String[] locationSplitted = location.split("/");
		String newprofileUUID = locationSplitted[locationSplitted.length - 1];

		//ACT 2
		String op = "replace";
		String path = "firstname";
		String firstname = RandomStringUtils.randomAlphabetic(20);

		String patch = "[{\"op\":\"" + op + "\",\"path\":\"/" + path + "\",\"value\":\"" + firstname + "\"}]";

		mockMvc.perform(
				patch(PROFILE_API_PATH + newprofileUUID).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("firstname", Matchers.is(firstname)));

		//ACT 2
		op = "replace";
		path = "firstname2";
		firstname = RandomStringUtils.randomAlphabetic(20);

		patch = "[{\"op\":\"" + op + "\",\"path\":\"/" + path + "\",\"value\":\"" + firstname + "\"}]";

		mockMvc.perform(
				patch(PROFILE_API_PATH + newprofileUUID).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isBadRequest());

		//ACT 3
		op = "replace";
		path = "fingerprint";
		String pattern = "A";

		patch = "[";
		for (int i = 1; i <= 10; i++) {
			patch = patch + "{\"op\":\"" + op + "\",\"path\":\"/" + path + i + "\",\"value\":\"" + pattern + "\"},";
		}
		patch = patch.substring(0, patch.length() - 1);
		patch = patch + "]";

		mockMvc.perform(
				patch(PROFILE_API_PATH + newprofileUUID).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("fingerprint1", Matchers.is(pattern)));

		//ACT 3
		op = "replace";
		path = "fingerprint";
		pattern = "L";

		patch = "[";
		for (int i = 1; i <= 10; i++) {
			patch = patch + "{\"op\":\"" + op + "\",\"path\":\"/" + path + i + "\",\"value\":\"" + pattern + "\"},";
		}
		patch = patch.substring(0, patch.length() - 1);
		patch = patch + "]";

		mockMvc.perform(
				patch(PROFILE_API_PATH + newprofileUUID).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("fingerprint1", Matchers.is(pattern)));

		//ACT 3
		op = "replace";
		path = "fingerprint";
		pattern = "W";

		patch = "[{\"op\":\"" + op + "\",\"path\":\"/" + path + "1\",\"value\":\"W\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "2\",\"value\":\"C\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "3\",\"value\":\"T\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "4\",\"value\":\"LA\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "5\",\"value\":\"L\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "6\",\"value\":\"L\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "7\",\"value\":\"L\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "8\",\"value\":\"L\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "9\",\"value\":\"L\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "10\",\"value\":\"L\"}]";

		mockMvc.perform(
				patch(PROFILE_API_PATH + newprofileUUID).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("fingerprint1", Matchers.is(pattern)));

		//ACT 3
		op = "replace";
		path = "fingerprint";
		pattern = "C";

		patch = "[{\"op\":\"" + op + "\",\"path\":\"/" + path + "1\",\"value\":\"C\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "2\",\"value\":\"C\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "3\",\"value\":\"T\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "4\",\"value\":\"LA\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "5\",\"value\":\"L\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "6\",\"value\":\"L\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "7\",\"value\":\"L\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "8\",\"value\":\"L\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "9\",\"value\":\"L\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "10\",\"value\":\"L\"}]";

		mockMvc.perform(
				patch(PROFILE_API_PATH + newprofileUUID).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("fingerprint1", Matchers.is(pattern)));

		//ACT 3
		op = "replace";
		path = "fingerprint";
		pattern = "C";

		patch = "[{\"op\":\"" + op + "\",\"path\":\"/" + path + "1\",\"value\":\"C\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "2\",\"value\":\"C\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "3\",\"value\":\"C\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "4\",\"value\":\"C\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "5\",\"value\":\"C\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "6\",\"value\":\"C\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "7\",\"value\":\"C\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "8\",\"value\":\"C\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "9\",\"value\":\"C\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "10\",\"value\":\"C\"}]";

		mockMvc.perform(
				patch(PROFILE_API_PATH + newprofileUUID).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("fingerprint1", Matchers.is(pattern)));

		//ACT 3
		op = "replace";
		path = "fingerprint";
		pattern = "LT";

		patch = "[{\"op\":\"" + op + "\",\"path\":\"/" + path + "1\",\"value\":\"LT\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "2\",\"value\":\"LT\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "3\",\"value\":\"LT\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "4\",\"value\":\"LT\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "5\",\"value\":\"LT\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "6\",\"value\":\"LT\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "7\",\"value\":\"LT\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "8\",\"value\":\"LT\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "9\",\"value\":\"LT\"},"
				+ "{\"op\":\"" + op + "\",\"path\":\"/" + path + "10\",\"value\":\"LT\"}]";

		mockMvc.perform(
				patch(PROFILE_API_PATH + newprofileUUID).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("fingerprint1", Matchers.is(pattern)));

		//ACT 3
		op = "replace";
		path = "fingerprint";
		pattern = "LT";

		patch = "[";
		for (int i = 1; i <= 5; i++) {
			patch = patch + "{\"op\":\"" + op + "\",\"path\":\"/" + path + i + "\",\"value\":\"" + pattern + "\"},";
		}

		pattern = "P";
		for (int i = 6; i <= 10; i++) {
			patch = patch + "{\"op\":\"" + op + "\",\"path\":\"/" + path + i + "\",\"value\":\"" + pattern + "\"},";
		}

		patch = patch.substring(0, patch.length() - 1);
		patch = patch + "]";

		mockMvc.perform(
				patch(PROFILE_API_PATH + newprofileUUID).header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken).header(
						HttpHeaders.CONTENT_TYPE, "application/json-patch+json").content(patch))
				.andExpect(
						status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("fingerprint10", Matchers.is(pattern)));

		//ACT 4
		results = mockMvc
				.perform(get(PROFILE_API_PATH + newprofileUUID)
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken))
				.andExpect(status().isOk());

		ConsultationModel consultation = mapper.readValue(results.andReturn().getResponse().getContentAsString(),
				new TypeReference<ConsultationModel>() {
				});

		Assert.assertNotNull(consultation);
		Assert.assertEquals(newprofile.email, consultation.email);

		//ACT 4
		results = mockMvc
				.perform(get(PROFILE_API_PATH + newprofileUUID + "/export")
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken))
				.andExpect(status().isOk());

		//ACT 4
		results = mockMvc
				.perform(get(PROFILE_API_PATH + newprofileUUID + "/1")
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken))
				.andExpect(status().isOk());

		//ACT 5
		results = mockMvc
				.perform(delete(PROFILE_API_PATH + newprofileUUID)
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken))
				.andExpect(status().isNoContent());

	}

	@Test
	public void testSearchProfiles() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		final String accessToken = obtainAccessToken();

		// ACT 1
		ConsultationSearchModel searchModel = new ConsultationSearchModel();
		String searchModelJson = mapper.writeValueAsString(searchModel);

		ResultActions results = mockMvc
				.perform(post(PROFILE_API_PATH + "/search")
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
						.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
						.header(FilterAPIs.RANGE_HEADER_NAME, "members=0-30")
						.content(searchModelJson))
				.andExpect(status().isNoContent());

	}

	@Test
	public void exportProfileTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		final String accessToken = obtainAccessToken();

		ResultActions results = mockMvc
				.perform(get(PROFILE_API_PATH + "/exportList")
						.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken))
				.andExpect(status().isOk());

	}

}