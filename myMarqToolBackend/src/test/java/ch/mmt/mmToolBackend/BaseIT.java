package ch.mmt.mmToolBackend;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.datatypes.Language;
import ch.mmt.mmtoolbackend.datatypes.Role;
import ch.mmt.mmtoolbackend.domain.model.Consultation;
import ch.mmt.mmtoolbackend.domain.model.User;
import ch.mmt.mmtoolbackend.domain.repository.ConsultationRepository;
import ch.mmt.mmtoolbackend.domain.repository.UserRepository;

public class BaseIT {

	protected static final String BASE_MMT_API_PATH = "/api/v1/tool/";

	protected static final String BASE_ADMIN_API_PATH = "/api/v1/admin/";

	protected static final String TEST_PASSWORD = "password";

	protected static final String TEST_USER = "test@test.ch";

	protected static final String TEST_USER_FIRSTNAME = "Gigi";

	protected static final String TEST_USER_LASTNAME = "Dag";

	protected MockMvc mockMvc;

	@Autowired
	protected WebApplicationContext wac;

	@Autowired
	protected FilterChainProxy springSecurityFilterChain;

	@Value("${mmtool.security.oauth2.client.clientId}")
	private String clientId;

	@Autowired
	protected UserRepository userRepository;

	@Autowired
	protected ConsultationRepository consultationRepository;

	protected User testUser;

	protected Consultation testConsultation;

	@Before
	public void setup()
			throws ServiceException {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();
		testUser = createTestUser();
		testConsultation = createTestConsultation();
	}

	@After
	public void cleanup() throws ServiceException {
		userRepository.delete(testUser);
		consultationRepository.delete(testConsultation);
	}

	protected User createTestUser() throws ServiceException {
		return createUser(TEST_USER, TEST_PASSWORD, TEST_USER_FIRSTNAME, TEST_USER_LASTNAME, Role.ADMIN, Language.ITA, false);
	}

	protected User createRandomUser() throws ServiceException {
		Role role = Role.values()[RandomUtils.nextInt(0, 3)];
		return createUser(RandomStringUtils.randomAlphabetic(20), TEST_PASSWORD, RandomStringUtils.randomAlphabetic(10), RandomStringUtils.randomAlphabetic(10), role, Language.ITA,
				false);
	}

	protected User createRandomUser(Role role) throws ServiceException {
		return createUser(RandomStringUtils.randomAlphabetic(20), TEST_PASSWORD, RandomStringUtils.randomAlphabetic(10), RandomStringUtils.randomAlphabetic(10), role, Language.ITA,
				false);
	}

	protected User createRandomUser(Role role, Language language, Boolean disabled) throws ServiceException {
		return createUser(RandomStringUtils.randomAlphabetic(20), TEST_PASSWORD, RandomStringUtils.randomAlphabetic(10), RandomStringUtils.randomAlphabetic(10), role, language,
				disabled);
	}

	protected User createUser(String email, String password, String firstname, String lastname, Role role, Language language, Boolean disabled)
			throws ServiceException {
		User user = new User();
		user.setEmail(email);
		user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt(10)));
		user.setFirstname(firstname);
		user.setLastname(lastname);
		user.setRole(role);
		user.setLanguage(language);
		user.setDisabled(disabled);
		return userRepository.save(user);
	}

	protected String obtainAccessToken() throws Exception {
		return obtainAccessToken(testUser);
	}

	protected String obtainAccessToken(User user) throws Exception {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("grant_type", "password");
		params.add("client_id", clientId);
		params.add("username", user.getEmail());
		params.add("password", TEST_PASSWORD);

		ResultActions result = mockMvc.perform(post("/oauth/token")
				.params(params)
				.accept("application/json;charset=UTF-8"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"));

		String resultString = result.andReturn().getResponse().getContentAsString();

		JacksonJsonParser jsonParser = new JacksonJsonParser();
		return jsonParser.parseMap(resultString).get("access_token").toString();
	}

	protected Consultation createTestConsultation() throws ServiceException {
		return createConsultaiton(TEST_USER, TEST_USER_FIRSTNAME, TEST_USER_LASTNAME);
	}

	protected Consultation createConsultaiton(String email, String firstname, String lastname)
			throws ServiceException {
		Consultation consultation = new Consultation();
		consultation.setEmail(email);
		consultation.setFirstname(firstname);
		consultation.setLastname(lastname);
		consultation.setFingerprint1("A");
		consultation.setFingerprint2("W");
		consultation.setFingerprint3("W");
		consultation.setCatalyst(true);
		consultation.setCommunicator(true);
		consultation.setDefender(true);
		consultation.setDoubts(true);
		consultation.setEnterprising(true);
		consultation.setEnthusiast(true);
		consultation.setGetstuck(true);
		consultation.setGuiltfeelings(true);
		consultation.setHandyman(true);
		consultation.setHarmonizer(true);
		consultation.setHide(true);
		consultation.setInnovator(true);
		consultation.setLeader(true);
		consultation.setMediator(true);
		consultation.setMentor(true);
		consultation.setMotivator(true);
		consultation.setOpposition(true);
		consultation.setOrganized(true);
		consultation.setPassivity(true);
		consultation.setPerformer(true);
		consultation.setRetreat(true);
		consultation.setSubmit(true);
		consultation.setSupporter(true);
		consultation.setTaciturn(true);
		consultation.setTobear(true);
		return consultationRepository.save(consultation);
	}

	protected Consultation createRandomConsultation() {
		Consultation consultation = new Consultation();

		consultation.setEmail(RandomStringUtils.randomAlphabetic(20));
		consultation.setFirstname(RandomStringUtils.randomAlphabetic(20));
		consultation.setLastname(RandomStringUtils.randomAlphabetic(20));
		consultation.setFingerprint1("A");
		consultation.setFingerprint2("W");
		consultation.setFingerprint3("W");
		consultation.setCatalyst(true);
		consultation.setCommunicator(true);
		consultation.setDefender(true);
		consultation.setDoubts(true);
		consultation.setEnterprising(true);
		consultation.setEnthusiast(true);
		consultation.setGetstuck(true);
		consultation.setGuiltfeelings(true);
		consultation.setHandyman(true);
		consultation.setHarmonizer(true);
		consultation.setHide(true);
		consultation.setInnovator(true);
		consultation.setLeader(true);
		consultation.setMediator(true);
		consultation.setMentor(true);
		consultation.setMotivator(true);
		consultation.setOpposition(true);
		consultation.setOrganized(true);
		consultation.setPassivity(true);
		consultation.setPerformer(true);
		consultation.setRetreat(true);
		consultation.setSubmit(true);
		consultation.setSupporter(true);
		consultation.setTaciturn(true);
		consultation.setTobear(true);

		return consultationRepository.save(consultation);
	}

	protected String generateRandomEmail() {
		return RandomStringUtils.randomAlphabetic(16) + "@" + RandomStringUtils.randomAlphabetic(6) + ".com";

	}
}
