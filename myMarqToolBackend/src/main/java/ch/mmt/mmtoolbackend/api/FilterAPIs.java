package ch.mmt.mmtoolbackend.api;

public class FilterAPIs {

	private FilterAPIs() {
		// Avoid instantiations
	}

	public static final String RANGE_UNIT = "members";

	public static final String ACCEPT_RANGES_HEADER_NAME = "Accept-Ranges";

	public static final String LOCATION_HEADER_NAME = "Location";

	public static final String LINK_HEADER_NAME = "Link";

	public static final String RANGE_HEADER_NAME = "Range";

	public static final String RANGE_QUERY_NAME = "range";

	public static final String CONTENT_RANGE_HEADER_NAME = "Content-Range";

	public static final String FIRST_REL = "first";

	public static final String PREVIOUS_REL = "prev";

	public static final String SELF_REL = "self";

	public static final String NEXT_REL = "next";

	private static final String PAGE_RESPONSE = "<li>In <b>page request mode</b> the server answers with an "
			+ "<em>OK</em> status code (200) in case of success"
			+ "and returns initially the first page and the following «" + LINK_HEADER_NAME + "» response headers:<ul>"
			+ "<li>a «" + FIRST_REL + "» link referring to the first page itself" + "<li>a «" + SELF_REL
			+ "» link referring to the first page itself, too" + "<li>a «" + NEXT_REL
			+ "» link referring to the second page if the collection's members don't fit into one page,"
			+ "</ul>when one follows the «" + NEXT_REL + "» link the server returns the second page and the following "
			+ "«" + LINK_HEADER_NAME + "» " + "response headers:<ul>" + "<li>a «" + FIRST_REL
			+ "» link referring to the first page" + "<li>a «" + PREVIOUS_REL
			+ "» link referring to the first page, too" + "<li>a «" + SELF_REL
			+ "» link referring to the second page itself" + "<li>a «" + NEXT_REL
			+ "» link referring to the third page if the collection's members don't fit into two pages"
			+ "</ul>and so on</li>";

	public static final String RANGE_RESPONSE = "<li>In <b>range request mode</b> the server answers with a "
			+ "<em>Partial Content</em> status code (206) and returns the members specified by the " + "«"
			+ CONTENT_RANGE_HEADER_NAME + "» response header, e.g. «" + RANGE_UNIT
			+ "=100-199/4711» (the 100th to the 199th member of a total of 4711 members)."
			+ "<li/>Mutiple ranges such as «" + RANGE_UNIT
			+ "=0-0,-1» (the first and the last member for example) are not "
			+ "supported and lead to a <em>Range Not Satisfiable</em> status code (406).<br />"
			+ "In case of empty collection the server answers with HTTP <em>No Content</em> status code (204), and returns "
			+ "a «" + CONTENT_RANGE_HEADER_NAME + "» response header with the value «*/0»." + "</li>";

	private static final String EXTERNAL_REFERENCES = "External references:<ul>"
			+ "<li>The filter API range request specification is derived from "
			+ "<a href=\"https://tools.ietf.org/html/rfc7233\">RFC 7233 — Hypertext Transfer Protocol (HTTP/1.1): "
			+ "Range Requests</a> by using «" + RANGE_UNIT + "» range units instead of «bytes» range units.";

	public static final String RANGE_UNIT_EXAMPLES = "«" + RANGE_UNIT + "=100-199», " + "«" + RANGE_UNIT
			+ "=-100» (the last 100 members) or " + "«" + RANGE_UNIT
			+ "=1000-» (the 1000th member and all members following it).";

	public static final String RANGE_UNIT_EXAMPLES_AS_QUERY_PARAM = "«100-199», " + "«-100» (the last 100 members) or "
			+ "«1000-» (the 1000th member and all members following it).";

	public static final String GET_NOTE = "Filter selections may be retrieved in either  <b>page</b> or <b>range request mode</b>."
			+ "<p/>" + "To switch to <b>range request mode</b> the client must provide a «" + RANGE_HEADER_NAME
			+ "» request " + "header with a single «" + RANGE_UNIT + "» range, e.g. " + RANGE_UNIT_EXAMPLES + "<p/>"
			+ "The server answers as following:<ul>" + RANGE_RESPONSE + PAGE_RESPONSE + "</ul>" + "<p/>"
			+ EXTERNAL_REFERENCES;

	public static final String POST_NOTE = "Filter selections may be created in either <b>page</b> or <b>range request mode</b>."
			+ "<p/>" + "To switch to <b>range request mode</b> the client must provide a «" + RANGE_QUERY_NAME
			+ "» query " + "parameter with a single «" + RANGE_UNIT + "» range, e.g. " + RANGE_UNIT_EXAMPLES + "<p/>"
			+ "The server answers as following:<ul>" + RANGE_RESPONSE + PAGE_RESPONSE + "</ul>" + "<p/>"
			+ EXTERNAL_REFERENCES;

	private static final String LINK_HEADER_MESSAGE = "A «" + PREVIOUS_REL + "» «" + LINK_HEADER_NAME + "», a «"
			+ NEXT_REL + "» «" + LINK_HEADER_NAME
			+ "» or both are included if and only if the response body does not represent all members of the collection.";

	public static final String OK_MESSAGE = "An OK response status with «" + LINK_HEADER_NAME + "» response headers "
			+ "is returned in case of success if the client does not provide a «" + RANGE_HEADER_NAME
			+ "» request header. " + LINK_HEADER_MESSAGE;

	public static final String CREATED_MESSAGE = "A CREATED response status with a «" + LOCATION_HEADER_NAME
			+ "» response header " + "is returned in case of success. " + LINK_HEADER_MESSAGE;

	public static final String PREDICATE_BASED_EXAMPLE = "AVBYWV==.MCwCFH==";

	public static final String LOCATION_HEADER_DESCRIPTION = "After successful creation a collection filter "
			+ "its URL returned in the «" + LOCATION_HEADER_NAME + "» response header may be saved by the client "
			+ "and used later on to access the filtered collection again as it represents the whole selection and not "
			+ "a single page only like the «" + LINK_HEADER_NAME + "» headers.<p>" + "Shortened example:<ul>"
			+ "<li/>http://…/filter/" + PREDICATE_BASED_EXAMPLE + "</ul°";

	public static final String FILTER_ID_DESCRIPTION = "The filter id is part of the «" + LOCATION_HEADER_NAME + "» "
			+ "response header returned when a filter has successfully been created.";

	private static final String LINK_HEADER_DESCRIPTION = "<ul>" + "<li/>The «" + FIRST_REL
			+ "» link refers to the first page." + "<li/>The «" + PREVIOUS_REL + "» link refers to the actual page. "
			+ "It is omitted if actual page is the first one." + "<li/>The «" + SELF_REL
			+ "» link refers to the actual page." + "<li/>The «" + NEXT_REL + "» link refers to the actual page. "
			+ "It is omitted if actual page is the last one." + "<p> " + "Shortened example: «" + LINK_HEADER_NAME
			+ "<br>&lt;…/filter/VldYWVo=.XvwCFHi7&gt; rel=\"" + FIRST_REL + "\", "
			+ "<br>&lt;…/filter/VldYWVoa.gKgCFHi7&gt; rel=\"" + PREVIOUS_REL + "\", " + "<br>&lt;…/filter/"
			+ PREDICATE_BASED_EXAMPLE + "&gt; rel=\"" + SELF_REL + "\", "
			+ "<br>&lt;…/filter/QrWVoee=.HUuCFHi=&gt; rel=\"" + NEXT_REL + "\"»";

	public static final String GET_LINK_HEADER_DESCRIPTION = "The API's collections provide a «" + LINK_HEADER_NAME
			+ "» response "
			+ "header to page through its members unless the client made a range request by providing a " + "«"
			+ RANGE_HEADER_NAME + "» request header:" + LINK_HEADER_DESCRIPTION;

	public static final String POST_LINK_HEADER_DESCRIPTION = "The API's collections provide a «"
			+ LINK_HEADER_NAME + "» response "
			+ "header to page through its members unless the client made a range request by providing a " + "«"
			+ RANGE_QUERY_NAME + "» query parameter:" + LINK_HEADER_DESCRIPTION;

	public static final String ACCEPT_RANGES_HEADER_DESCRIPTION = "The API's collections accept " + "«"
			+ RANGE_UNIT + "» as range unit im order to switch from <em>paging mode</em> to "
			+ "<em>range request mode</em>";

	public static final String UNSATISFIABLE_RANGE_DESCRIPTION = "The response contains the total number of members, "
			+ "e.g. «" + RANGE_UNIT + "=*/4711» (unsatisfiable range request for a collection with 4711 members)";

	public static final String NO_CONTENT_RANGE_DESCRIPTION = "The response contains the total number of members, "
			+ "e.g. «" + RANGE_UNIT
			+ "=*/50» (e.g. if we are trying to retrieve elements 50 to 99 of a collection with 50 members)";

	public static final String CONTENT_RANGE_DESCRIPTION = "The response contains the  returned range as well as the "
			+ "total number of members, e.g. «" + RANGE_UNIT
			+ "=100-199/4711» (the 100th to the 199th member of a collection with total 4711 members)";

	public static final String REQUESTED_RANGE_NOT_SATISFIABLE_MESSAGE = "The status code <em>Range Not Satisfiable</em> "
			+ "is returned for example if the requested range starts after the last element or if multiple ranges are "
			+ "requested at once, e.g. «" + RANGE_UNIT + "=0-0,-1» (the first and the last member)";

	public static final String PARTIAL_CONTENT_MESSAGE_ON_HEADER = "The status code <em>Partial Content</em> "
			+ "is returned in case of success  if the client has included a «" + RANGE_HEADER_NAME
			+ "» request header.";

	public static final String PARTIAL_CONTENT_MESSAGE_ON_QUERY = "The status code <em>Partial Content</em> "
			+ "is returned in case of success  if the client has included a «" + RANGE_QUERY_NAME + "» request query.";

	public static final String NO_CONTENT_MESSAGE_ON_HEADER = "The status code <em>No Content</em> "
			+ "is returned in case of an empty result  if the client has included a «" + RANGE_HEADER_NAME
			+ "» request header.";

	public static final String NO_CONTENT_MESSAGE_ON_QUERY = "The status code <em>No Content</em> "
			+ "is returned in case of an empty result  if the client has included a «" + RANGE_QUERY_NAME
			+ "» request query.";

}