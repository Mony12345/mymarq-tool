package ch.mmt.mmtoolbackend.api.exception.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.NoArgsConstructor;

@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
@ApiModel(value = "WebApplicationException", description = "Entity accompanying an invalid status code")
public abstract class WebApplicationExceptionModel implements Serializable {

	private static final long serialVersionUID = -8489380607729891885L;

	public WebApplicationExceptionModel(String message) {
		this.message = message;
	}

	@ApiModelProperty(
			value = "Message accompanying a non-successful request",
			required = false,
			example = "Input String is not a valid xml")
	public String message;

	@ApiModelProperty(
			value = "Throwable exception",
			required = false)
	public Throwable cause;

}
