package ch.mmt.mmtoolbackend.api.model.patch;

import io.swagger.annotations.ApiModelProperty;

public abstract class ValueOperationModel extends OperationModel {

	@ApiModelProperty(
			value = "The operation object MUST contain a \"value\" member whose content "
					+ "specifies the value to be updated.",
			required = true,
			example = "[ \"foo\", \"bar\" ]")
	public Object value;

	protected ValueOperationModel(Kind add) {
		super(add);
	}

	@Override
	public String toString() {
		return super.toString() + "," + this.value;
	}
}
