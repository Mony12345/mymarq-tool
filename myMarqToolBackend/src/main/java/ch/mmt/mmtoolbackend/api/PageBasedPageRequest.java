package ch.mmt.mmtoolbackend.api;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public class PageBasedPageRequest extends PageRequest {
	private static final long serialVersionUID = 711205572564912303L;

	public static final int DEFAULT_PAGE_SIZE = 20;

	public PageBasedPageRequest() {
		this(0/* page */, DEFAULT_PAGE_SIZE/* size */);
	}

	/**
	 * @param page
	 * @param size
	 */
	public PageBasedPageRequest(int page, int size) {
		super(page, size);
	}

	/**
	 * @param page
	 * @param size
	 * @param sort
	 */
	public PageBasedPageRequest(int page, int size, Sort sort) {
		super(page, size, sort);
	}

	/**
	 * @param page
	 * @param size
	 * @param direction
	 * @param properties
	 */
	public PageBasedPageRequest(int page, int size, Direction direction, String... properties) {
		super(page, size, direction, properties);
	}

}
