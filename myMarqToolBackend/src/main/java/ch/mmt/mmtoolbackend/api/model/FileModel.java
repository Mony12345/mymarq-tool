package ch.mmt.mmtoolbackend.api.model;

import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import lombok.ToString;

@ApiModel(value = "FileReturnType", description = FileModel.DESCRIPTION)
@JsonTypeName("FileReturnType")
@ToString
public class FileModel {

	public static final String DESCRIPTION = "A file representation";

	@ApiModelProperty(value = "The filen name", required = false, example = "aaa.png.csv", accessMode = AccessMode.READ_ONLY)
	public String fileName;

	@ApiModelProperty(value = "The file content", required = false, accessMode = AccessMode.READ_ONLY)
	public byte[] fileContent;

	@ApiModelProperty(value = "The file content type", required = false, example = "text/csv", accessMode = AccessMode.READ_ONLY)
	public String fileContentType;
}
