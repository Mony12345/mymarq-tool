package ch.mmt.mmtoolbackend.api.search.model;

import com.fasterxml.jackson.annotation.JsonTypeName;

import ch.mmt.mmtoolbackend.datatypes.Language;
import ch.mmt.mmtoolbackend.datatypes.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;

@ApiModel(value = "UserSearch", description = UserSearchModel.DESCRIPTION)
@JsonTypeName("UserSearch")
@ToString
public class UserSearchModel {

	public static final String DESCRIPTION = "Users search param object. The search is performed using AND logic."
			+ "Fields that should not be considered for the search should <strong>be left away rather than</strong> left empty."
			+ "Wildcards are added automatically in front and to the end to all fields";

	@ApiModelProperty(required = false, value = "Find the users with the given email")
	public String email;

	@ApiModelProperty(required = false, value = "Find the users with the given firstname")
	public String firstname;

	@ApiModelProperty(required = false, value = "Find the users with the given lastname")
	public String lastname;

	@ApiModelProperty(required = false, value = "Find the users with the given language", example = "it")
	public Language language;

	@ApiModelProperty(required = false, value = "Find the users with the given role", example = "revisioner")
	public Role role;

	@ApiModelProperty(required = false, value = "Find all disabled, or enabled, users")
	public Boolean disabled;
}
