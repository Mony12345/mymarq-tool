package ch.mmt.mmtoolbackend.api.model.patch;

import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;

/**
 * The "test" operation tests that a value at the target location is equal
 * to a specified value.
 * <p>
 * The operation object MUST contain a "value" member that conveys the value
 * to be compared to the target location's value.
 * <p>
 * The target location MUST be equal to the "value" value for the operation
 * to be considered successful.
 * <p>
 * Here, "equal" means that the value at the target location and the value
 * conveyed by "value" are of the same JSON type, and that they are
 * considered equal by the following rules for that type:
 * <ul>
 * <li>strings: are considered equal if they contain the same number of
 * Unicode characters and their code points are byte-by-byte equal.
 * <li>numbers: are considered equal if their values are numerically equal.
 * <li>arrays: are considered equal if they contain the same number of
 * values, and if each value can be considered equal to the value at the
 * corresponding position in the other array, using this list of
 * type-specific rules.
 * <li>objects: are considered equal if they contain the same number of
 * members, and if each member can be considered equal to a member in the
 * other object, by comparing their keys (as strings) and their values
 * (using this list of type-specific rules).
 * <li>literals (false, true, and null): are considered equal if they are
 * the same.
 * <p>
 * Note that the comparison that is done is a logical comparison; e.g.,
 * whitespace between the member values of an array is not significant.
 * <p>
 * Also, note that ordering of the serialization of object members is not
 * significant.
 * <p>
 * For example: <code>
 * { "op": "test", "path": "/a/b/c", "value": "foo" }
 * </code>
 */
@JsonTypeName(TestModel.OPERATION)
@ApiModel(value = TestModel.OPERATION, description = "Test Operation", parent = OperationModel.class)
public class TestModel extends ValueOperationModel {

	public static final String OPERATION = "test";

	public TestModel() {
		super(Kind.test);
	}

	@Override
	public String toString() {
		return super.toString() + "," + this.value;
	}

}