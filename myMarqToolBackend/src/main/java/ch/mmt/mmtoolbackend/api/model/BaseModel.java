package ch.mmt.mmtoolbackend.api.model;

import java.time.Instant;
import java.util.UUID;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ch.mmt.mmtoolbackend.datatypes.serializer.InstantDeserializer;
import ch.mmt.mmtoolbackend.datatypes.serializer.InstantSerializer;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;

public class BaseModel {

	@ApiModelProperty(value = "The resource id, which uniquely identifies a resource", example = "5722d2ae-355f-11e7-a919-92ebcb67fe33", accessMode = AccessMode.READ_ONLY,
			required = false)
	public UUID uuid;

	@ApiModelProperty(value = "The resource last update date")
	@JsonSerialize(using = InstantSerializer.class)
	@JsonDeserialize(using = InstantDeserializer.class)
	public Instant lastUpdateAt;

	@ApiModelProperty(value = "The user who created the resource")
	public long createdBy;

	@ApiModelProperty(value = "The resource creation date")
	@JsonSerialize(using = InstantSerializer.class)
	@JsonDeserialize(using = InstantDeserializer.class)
	public Instant createdAt;
}
