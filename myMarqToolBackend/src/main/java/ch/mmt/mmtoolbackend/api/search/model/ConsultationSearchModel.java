package ch.mmt.mmtoolbackend.api.search.model;

import com.fasterxml.jackson.annotation.JsonTypeName;

import ch.mmt.mmtoolbackend.api.model.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;

@ApiModel(value = "ConsultationSearch", description = ConsultationSearchModel.DESCRIPTION)
@JsonTypeName("ConsultationSearch")
@ToString
public class ConsultationSearchModel extends BaseModel {

	public static final String DESCRIPTION = "Consultaion search param object. The search is performed using AND logic."
			+ "Fields that should not be considered for the search should <strong>be left away rather than</strong> left empty."
			+ "Wildcards are added automatically in front and to the end to all fields";

	@ApiModelProperty(required = false, value = "Find the consultaiton with the given firstname")
	public String firstname;

	@ApiModelProperty(required = false, value = "Find the consultation with the given lastname")
	public String lastname;

}
