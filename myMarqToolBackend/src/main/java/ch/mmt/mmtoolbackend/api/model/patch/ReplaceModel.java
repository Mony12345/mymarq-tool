package ch.mmt.mmtoolbackend.api.model.patch;

import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;

/**
 * The "replace" operation replaces the value at the target location with a
 * new value. The operation object MUST contain a "value" member whose
 * content specifies the replacement value.
 * <p>
 * The target location MUST exist for the operation to be successful.
 * <p>
 * For example: <code>
 * { "op": "replace", "path": "/a/b/c", "value": 42 }
 * </code> This operation is functionally identical to a "remove" operation
 * for a value, followed immediately by an "add" operation at the same
 * location with the replacement value.
 */
@JsonTypeName(ReplaceModel.OPERATION)
@ApiModel(value = ReplaceModel.OPERATION, description = "Replace Operation", parent = OperationModel.class)
public class ReplaceModel extends ValueOperationModel {

	public static final String OPERATION = "replace";

//    @ApiModelProperty(value = "The operation object MUST contain a \"value\" member"
//            + "whose content specifies the replacement value.", required = true, example = "[ \"foo\", \"bar\" ]")
//    public Object value;

	public ReplaceModel() {
		super(Kind.replace);
	}

}