package ch.mmt.mmtoolbackend.api.search.model;

import ch.mmt.mmtoolbackend.api.FilterAPIs;
import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import io.swagger.annotations.ApiModel;

@ApiModel(value = "RangeRequest", description = RangeRequestModel.DESCRIPTION)
public class RangeRequestModel {

	public static final String INFORMATION = "Defines, which range of the result set shall be returned. "
			+ "It contains " + "a single «" + FilterAPIs.RANGE_UNIT + "» range, e.g. " + FilterAPIs.RANGE_UNIT_EXAMPLES;

	public static final String DESCRIPTION = INFORMATION + "<p/><ol>" + "<li>The client provides a «"
			+ FilterAPIs.RANGE_HEADER_NAME + "» request header" + FilterAPIs.RANGE_RESPONSE + "</ol>";

	public static final String INFORMATION_AS_QUERY_PARAM = "Defines, which range of the result set shall be returned. "
			+ "It contains " + "a single range, e.g. " + FilterAPIs.RANGE_UNIT_EXAMPLES_AS_QUERY_PARAM;

	public static final String DESCRIPTION_AS_QUERY_PARAM = INFORMATION_AS_QUERY_PARAM + "<p/><ol>"
			+ "<li>The client provides a «" + FilterAPIs.RANGE_QUERY_NAME + "» query header" + FilterAPIs.RANGE_RESPONSE
			+ "</ol>";

	public static RangeRequestModel forRange(int from, int to) throws ServiceException {
		return new RangeRequestModel(from, to);
	}

	/**
	 * Constructor
	 *
	 * @param value
	 *            in the form: members=100-199, members=-100, members=1000-
	 * @throws Exception
	 *
	 * @exception IllegalArgumentException
	 */
	public RangeRequestModel(String value) throws Exception {
		boolean usingMembersPrefix = false;
		if (value != null) {
			String members = null;
			if (value.toLowerCase().startsWith(FilterAPIs.RANGE_UNIT.toLowerCase()) && value.contains("=")) {
				usingMembersPrefix = true;
				members = value.split("=")[1];

			} else {
				// may be a 'range=from-to' query parameter... let's see
				members = value;
			}

			String[] split = members.split("-");
			if (split.length == 1) {
				if (members.contains("-")) {
					// no 'to'...
					split = new String[] { split[0], "" };
				}
			}

			if (split.length == 2) {
				try {

					String n = split[0].trim();
					if (!n.isEmpty()) {
						this.from = Integer.valueOf(n);
					}

					n = split[1].trim();
					if (!n.isEmpty()) {
						this.to = Integer.valueOf(n);
					}

					validateRangeParameter(this);

					return;
				} catch (final NumberFormatException exception) {
					throw new ServiceException(
							"Range members must be numbers greater equal to '0', or empty.", this,
							RangeRequestModel.INFORMATION);
				}
			}
		}

		throw new ServiceException(
				(usingMembersPrefix ? "Range header value must start with 'members='. " : "")
						+ "Range members must be numbers greater equal to '0', or empty.",
				this,
				RangeRequestModel.INFORMATION);
	}

	private RangeRequestModel(int from, int to) throws ServiceException {
		this.from = from;
		this.to = to;
		validateRangeParameter(this);

	}

	private Integer from = null;

	private Integer to = null;

	public Integer from() {
		return this.from;
	}

	public Integer to() {
		return this.to;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(FilterAPIs.RANGE_UNIT);
		sb.append("=");

		if (this.from != null) {
			sb.append(this.from.intValue());
		}
		sb.append("-");
		if (this.to != null) {
			sb.append(this.to.intValue());
		}

		return sb.toString();
	}

	public String toContentRangeHeaderString(long totalSize) {
		return toString() + "/" + totalSize;
	}

	public static String noContentRangeHeaderString() {
		return "*/0";
	}

	public static void validateRangeParameter(RangeRequestModel rangeRequestModel) throws ServiceException {
		if (rangeRequestModel != null) {

			if ((rangeRequestModel.from() == null) && (rangeRequestModel.to() == null)) {
				throw new ServiceException(
						"'From' and 'to' cannot be both empty", rangeRequestModel,
						RangeRequestModel.INFORMATION);
			}

			if ((rangeRequestModel.from() != null) && (rangeRequestModel.from().intValue() < 0)) {
				throw new ServiceException(
						"'From' must be empty or greater than or equal '0'", rangeRequestModel,
						RangeRequestModel.INFORMATION);
			}

			if ((rangeRequestModel.to() != null) && (rangeRequestModel.to().intValue() < 0)) {
				throw new ServiceException(
						"'To' must be empty or greater than or equal '0'", rangeRequestModel,
						RangeRequestModel.INFORMATION);
			}

			if ((rangeRequestModel.to() != null) && (rangeRequestModel.from() != null)
					&& (rangeRequestModel.to().intValue() < rangeRequestModel.from().intValue())) {
				throw new ServiceException(
						"'To' must be greater than or equal to 'from'", rangeRequestModel,
						RangeRequestModel.INFORMATION);
			}

		}
	}
}
