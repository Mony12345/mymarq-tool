package ch.mmt.mmtoolbackend.api.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonTypeName;

import ch.mmt.mmtoolbackend.datatypes.Language;
import ch.mmt.mmtoolbackend.datatypes.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;

@ApiModel(value = "User", description = ConsultationModel.INFORMATION)
@JsonTypeName("User")
@ToString
public class UserModel extends BaseModel {

	public static final String INFORMATION = "This represents a user";

	@Email
	@ApiModelProperty(value = "The user's email", required = true)
	public String email;

	@ApiModelProperty(value = "The user's password", required = true)
	@NotNull
	public String password;

	@ApiModelProperty(value = "The user's firstname", required = false)
	public String firstname;

	@ApiModelProperty(value = "The user's lastname", required = false)
	public String lastname;

	@ApiModelProperty(value = "The user's language", required = true)
	@NotNull
	public Language language;

	@ApiModelProperty(value = "The user's role", required = false)
	public Role role;

	@ApiModelProperty(value = "Flag indicating if the user is enabled or not", required = false)
	public Boolean enabled;

}
