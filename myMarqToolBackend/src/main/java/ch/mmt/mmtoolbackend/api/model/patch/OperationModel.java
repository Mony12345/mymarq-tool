package ch.mmt.mmtoolbackend.api.model.patch;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Patch Operation according to RFC 5789
 * <p>
 * Operation objects MUST have exactly one "op" member, whose value indicates
 * the operation to perform. Its value MUST be one of "add", "remove",
 * "replace", "move", "copy", or "test"; other values are errors. The semantics
 * of each object is defined below.
 */
@ApiModel(
		value = "Operation",
		description = OperationModel.DESCRIPTION,
		subTypes = { AddModel.class,
				RemoveModel.class, ReplaceModel.class,
				TestModel.class },
		discriminator = OperationModel.DISCRIMINATOR)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = OperationModel.DISCRIMINATOR)
@JsonSubTypes({
		@JsonSubTypes.Type(value = AddModel.class, name = AddModel.OPERATION),
		@JsonSubTypes.Type(value = RemoveModel.class, name = RemoveModel.OPERATION),
		@JsonSubTypes.Type(value = ReplaceModel.class, name = ReplaceModel.OPERATION),
		@JsonSubTypes.Type(value = TestModel.class, name = TestModel.OPERATION) })

public abstract class OperationModel {

	public static final String INFORMATION = "Patch Operation according to RFC 5789.<p>" +
			" Operation objects MUST have exactly one \"op\" member, whose value indicates" +
			" the operation to perform. Its value MUST be one of \"add\", \"remove\"," +
			" \"replace\", \"move\", \"copy\", or \"test\"; other values are errors. The semantics" +
			" of each object is defined in the corresponding subclass.</p>";

	public static final String DESCRIPTION = INFORMATION
			+ "<br/>"
			+ "External References:<ul>"
			+ "<li/>RFC: <a href=\"https://tools.ietf.org/html/rfc5789\">RFC5789</a>"
			+ "</ul>";

	protected OperationModel(
			Kind op) {
		this.op = op;
	}

	public final static String DISCRIMINATOR = "op";
	@ApiModelProperty(
			value = "The operation discriminant",
			required = true,
			example = "add")
	public Kind op;

	@ApiModelProperty(
			value = "Operation objects MUST have exactly one \"path\" member. That member's value is a string "
					+ "containing a JSON-Pointer value according to RFC 6901 (https://tools.ietf.org/html/rfc6901) "
					+ "that references a location within the target document (the \"target location\") "
					+ "where the operation is performed",
			required = true,
			example = "/a/b/c")
	public String path;

	@Override
	public String toString() {
		return this.op + "," + this.path;
	}

	public static enum Kind {
		add,
		remove,
		replace,
		move,
		copy,
		test;
	}
}
