package ch.mmt.mmtoolbackend.api.model.patch;

import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;

/**
 * The "remove" operation removes the value at the target location.
 * <p>
 * The target location MUST exist for the operation to be successful.
 * <p>
 * For example: <code>
 * { "op": "remove", "path": "/a/b/c" }
 * </code> If removing an element from an array, any elements above the
 * specified index are shifted one position to the left.
 */
@ApiModel(value = RemoveModel.OPERATION, description = "Remove Operation", parent = OperationModel.class)
@JsonTypeName(RemoveModel.OPERATION)
public class RemoveModel extends OperationModel {

	public static final String OPERATION = "remove";

	public RemoveModel() {
		super(Kind.remove);
	}

}