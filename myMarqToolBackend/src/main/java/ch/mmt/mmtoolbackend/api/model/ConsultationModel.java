package ch.mmt.mmtoolbackend.api.model;

import java.util.Date;
import java.util.UUID;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;

@ApiModel(value = "Consultation", description = ConsultationModel.INFORMATION)
@JsonTypeName("Consultation")
@ToString
public class ConsultationModel extends BaseModel {

	public static final String INFORMATION = "This represents a consultation";

	@ApiModelProperty(value = "The person firstname", required = true)
	@NotNull
	public String firstname;

	@ApiModelProperty(value = "The person lastname", required = true)
	@NotNull
	public String lastname;

	@ApiModelProperty(value = "The person date of birth", required = false)
	public Date dateBirth;

	@ApiModelProperty(value = "The person gender", required = false)
	public String gender;

	@ApiModelProperty(value = "The person nationality", required = false)
	public String nationality;

	@ApiModelProperty(value = "The person civil status", required = false)
	public String civilStatus;

	@ApiModelProperty(value = "The person number of child", required = false)
	public int numberChild;

	@ApiModelProperty(value = "The person professional", required = false)
	public String professional;

	@ApiModelProperty(value = "The person education", required = false)
	public String education;

	@ApiModelProperty(value = "The person address street", required = false)
	public String street;

	@ApiModelProperty(value = "The person address street number", required = false)
	public String streetNumber;

	@ApiModelProperty(value = "The person address cap", required = false)
	public String cap;

	@ApiModelProperty(value = "The person address city", required = false)
	public String city;

	@ApiModelProperty(value = "The person address country", required = false)
	public String country;

	@Email
	@ApiModelProperty(value = "The person email", required = false)
	public String email;

	@ApiModelProperty(value = "The person phone number", required = false)
	public String phoneNumber;

	@ApiModelProperty(value = "The person fingerprint1", required = false)
	public String fingerprint1;
	@ApiModelProperty(value = "The person fingerprint2", required = false)
	public String fingerprint2;
	@ApiModelProperty(value = "The person fingerprint3", required = false)
	public String fingerprint3;
	@ApiModelProperty(value = "The person fingerprint4", required = false)
	public String fingerprint4;
	@ApiModelProperty(value = "The person fingerprint5", required = false)
	public String fingerprint5;
	@ApiModelProperty(value = "The person fingerprint6", required = false)
	public String fingerprint6;
	@ApiModelProperty(value = "The person fingerprint7", required = false)
	public String fingerprint7;
	@ApiModelProperty(value = "The person fingerprint8", required = false)
	public String fingerprint8;
	@ApiModelProperty(value = "The person fingerprint9", required = false)
	public String fingerprint9;
	@ApiModelProperty(value = "The person fingerprint10", required = false)
	public String fingerprint10;
	@ApiModelProperty(value = "The person handyman", required = false)
	public Boolean handyman;
	@ApiModelProperty(value = "The person leader", required = false)
	public Boolean leader;
	@ApiModelProperty(value = "The person organized", required = false)
	public Boolean organized;
	@ApiModelProperty(value = "The person performer", required = false)
	public Boolean performer;
	@ApiModelProperty(value = "The person communicator", required = false)
	public Boolean communicator;
	@ApiModelProperty(value = "The person getstuck", required = false)
	public Boolean getstuck;
	@ApiModelProperty(value = "The person doubts", required = false)
	public Boolean doubts;
	@ApiModelProperty(value = "The person opposition", required = false)
	public Boolean opposition;
	@ApiModelProperty(value = "The person passivity", required = false)
	public Boolean passivity;
	@ApiModelProperty(value = "The person taciturn", required = false)
	public Boolean taciturn;
	@ApiModelProperty(value = "The person supporter", required = false)
	public Boolean supporter;
	@ApiModelProperty(value = "The person motivator", required = false)
	public Boolean motivator;
	@ApiModelProperty(value = "The person mentor", required = false)
	public Boolean mentor;
	@ApiModelProperty(value = "The person innovator", required = false)
	public Boolean innovator;
	@ApiModelProperty(value = "The person catalyst", required = false)
	public Boolean catalyst;
	@ApiModelProperty(value = "The person retreat", required = false)
	public Boolean retreat;
	@ApiModelProperty(value = "The person tobear", required = false)
	public Boolean tobear;
	@ApiModelProperty(value = "The person guiltfeelings", required = false)
	public Boolean guiltfeelings;
	@ApiModelProperty(value = "The person hide", required = false)
	public Boolean hide;
	@ApiModelProperty(value = "The person submit", required = false)
	public Boolean submit;
	@ApiModelProperty(value = "The person harmonizer", required = false)
	public Boolean harmonizer;
	@ApiModelProperty(value = "The person enterprising", required = false)
	public Boolean enterprising;
	@ApiModelProperty(value = "The person mediator", required = false)
	public Boolean mediator;
	@ApiModelProperty(value = "The person enthusiast", required = false)
	public Boolean enthusiast;
	@ApiModelProperty(value = "The person defender", required = false)
	public Boolean defender;

	@ApiModelProperty(value = "The userUUID that create the consultation", required = false)
	public UUID userUUID;

	@ApiModelProperty(value = "The user firstname that create the consultation", required = false)
	public String userFirstName;
	@ApiModelProperty(value = "The user lastname that create the consultation", required = false)
	public String userLastName;

	public void anonimize(String fn, String ln) {
		this.firstname = fn;
		this.lastname = ln;
	}
}
