package ch.mmt.mmtoolbackend.api.exception;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import ch.mmt.mmtoolbackend.api.exception.model.ClientErrorModel;
import ch.mmt.mmtoolbackend.api.exception.model.ServerErrorModel;
import ch.mmt.mmtoolbackend.api.exception.model.WebApplicationExceptionModel;

public class WebApplicationErrorResponses {

	private WebApplicationErrorResponses() {
		// Avoid instantiation
	}

	public static ResponseEntity<String> newInternalException(
			String code,
			String msg) {
		String content = "{}";

		try {
			JSONObject obj = new JSONObject();
			obj.put("scope", "INTERNAL");
			obj.put("msg", msg);
			obj.put("code", code);
			content = obj.toString();
		} catch (JSONException je) {
		}

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(content);
	}

	public static ResponseEntity<ServerErrorModel> newInternalServerErrorException(
			Throwable cause) {
		ServerErrorModel error = newServerErrorException(
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
				ServerErrorModel.INTERNAL_SERVER_ERROR_MESSAGE,
				cause);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
	}

	public static ResponseEntity<ClientErrorModel> newBadRequestException(
			String message,
			Object invalidValue,
			String information) {
		ClientErrorModel error = newClientErrorModel(
				message,
				invalidValue,
				information);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
	}

	public static ResponseEntity<ClientErrorModel> newBadRequestException(
			String message,
			Object invalidValue,
			String information,
			Throwable cause) {
		ClientErrorModel error = newClientErrorModel(
				message,
				invalidValue,
				information,
				cause);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
	}

	public static ResponseEntity<ClientErrorModel> newBadRequestException(ServiceException se) {
		return WebApplicationErrorResponses.newBadRequestException(
				se.getMessage(), se.getInvalidValue(), se.getInformation(), se.getCauseMsg(), se.getCause());
	}

	public static ResponseEntity<ClientErrorModel> newBadRequestException(
			String message,
			Object invalidValue,
			String information,
			String causeMsg,
			Throwable t) {
		ClientErrorModel error = newClientErrorModel(
				message,
				invalidValue,
				information,
				causeMsg,
				t);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
	}

	public static ResponseEntity<ClientErrorModel> newBadRequestException(
			String message,
			Object invalidValue,
			String information,
			String cause) {
		ClientErrorModel error = newClientErrorModel(
				message,
				invalidValue,
				information,
				cause);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
	}

	public static ResponseEntity<ClientErrorModel> newRangeNotSatisfiableException(
			String message,
			Object invalidValue,
			String information) {
		ClientErrorModel error = newClientErrorModel(
				message,
				invalidValue,
				information);
		return ResponseEntity.status(HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE).body(error);
	}

	public static ResponseEntity<ClientErrorModel> newForbiddenException(
			String message) {
		ClientErrorModel error = newClientErrorModel(
				message,
				null,
				null);
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(error);
	}

	public static ResponseEntity<ClientErrorModel> newUnauthorizedException() {
		ClientErrorModel error = newClientErrorModel(
				"Authorization required",
				null,
				null);
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(error);
	}

	public static ResponseEntity<ClientErrorModel> newNotFoundException(
			String message) {
		ClientErrorModel error = newClientErrorModel(
				message,
				null,
				null);
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
	}

	public static ResponseEntity<ClientErrorModel> newNotFoundException(
			String message,
			final Object invalidValue,
			final String information) {
		ClientErrorModel error = newClientErrorModel(
				message,
				invalidValue,
				information);
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
	}

	public static ResponseEntity<ClientErrorModel> newConflictException(
			String message,
			Object invalidValue,
			String information) {
		ClientErrorModel error = newClientErrorModel(
				message,
				invalidValue,
				information);
		return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
	}

	public static ResponseEntity<ClientErrorModel> newConflictException(
			String message,
			Object invalidValue,
			String information,
			String cause) {
		ClientErrorModel error = newClientErrorModel(
				message,
				invalidValue,
				information,
				cause);
		return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
	}

	private static ServerErrorModel newServerErrorException(
			final int statusCode,
			final String message,
			final Throwable cause) {
		final ServerErrorModel serverError = new ServerErrorModel();
		serverError.message = message;
		serverError.cause = cause;

		return serverError;
	}

	private static ClientErrorModel newClientErrorModel(
			final String message,
			final Object invalidValue,
			final String information,
			final Throwable cause) {
		return newClientErrorModel(
				message,
				invalidValue,
				information,
				cause.getMessage(),
				cause);
	}

	private static ClientErrorModel newClientErrorModel(
			final String message,
			final Object invalidValue,
			final String information) {
		return newClientErrorModel(
				message,
				invalidValue,
				information,
				null,
				null);
	}

	private static ClientErrorModel newClientErrorModel(
			final String message,
			final Object invalidValue,
			final String information,
			final String reason) {
		return newClientErrorModel(
				message,
				invalidValue,
				information,
				reason,
				null);
	}

	private static ClientErrorModel newClientErrorModel(
			final String message,
			final Object invalidValue,
			final String information,
			final String reason,
			final Throwable cause) {
		final ClientErrorModel clientError = new ClientErrorModel();
		clientError.message = message;
		if (invalidValue != null) {
			clientError.invalidValue = invalidValue.toString();
		}
		clientError.information = information;
		clientError.reason = reason;
		clientError.cause = cause;

		return clientError;
	}

	public static ResponseEntity<? extends WebApplicationExceptionModel> convertServiceExceptionToResponse(
			ServiceException se) {
		HttpStatus httpStatus = se.getHttpStatus();
		if (httpStatus != null) {
			switch (httpStatus) {
			case BAD_REQUEST:
				return newBadRequestException(se.getMessage(), se.getInvalidValue(), se.getInformation());

			case CONFLICT:
				return newConflictException(se.getMessage(), se.getInvalidValue(), se.getInformation());

			case FORBIDDEN:
				return newForbiddenException(se.getMessage());

			case INTERNAL_SERVER_ERROR:
				return getInternalExceptionFromServiceException(se);

			case NOT_FOUND:
				return newNotFoundException(se.getMessage(), se.getInvalidValue(), se.getInformation());

			case REQUESTED_RANGE_NOT_SATISFIABLE:
				return newRangeNotSatisfiableException(se.getMessage(), se.getInvalidValue(), se.getInformation());

			case UNAUTHORIZED:
				return newUnauthorizedException();

			default:
				return newInternalServerErrorException(new Throwable(se.getMessage()));
			}
		}

		return getInternalExceptionFromServiceException(se);
	}

	private static ResponseEntity<ServerErrorModel> getInternalExceptionFromServiceException(ServiceException se) {
		if (se.getThrowable() != null) {
			return newInternalServerErrorException(se.getThrowable());
		} else {
			return newInternalServerErrorException(new Throwable(se.getCauseMsg()));
		}
	}

}
