package ch.mmt.mmtoolbackend.api.exception;

import org.springframework.http.HttpStatus;

public class ServiceException extends Exception {

	private static final long serialVersionUID = 4123553907679015973L;

	private final Object invalidValue;

	private final String information;

	private final String causeMsg;

	private final HttpStatus httpStatus;

	private final Throwable cause;

	public ServiceException(String message,
			Object invalidValue,
			String information,
			String causeMsg,
			HttpStatus httpStatus,
			Throwable t) {
		super(message, t);
		this.invalidValue = invalidValue;
		this.information = information;
		this.causeMsg = causeMsg;
		this.httpStatus = httpStatus;
		this.cause = t;
	}

	public ServiceException(String message) {
		this(message, null, null, null, null, null);
	}

	public ServiceException(String message, HttpStatus httpStatus) {
		this(message, null, null, null, httpStatus, null);
	}

	public ServiceException(String message, Object invalidValue, String information) {
		this(message, invalidValue, information, null, null, null);
	}

	public ServiceException(String message, Object invalidValue, String information, HttpStatus httpStatus) {
		this(message, invalidValue, information, null, httpStatus, null);
	}

	public ServiceException(String message, Object invalidValue, String information, String causeMsg) {
		this(message, invalidValue, information, causeMsg, null, null);
	}

	public ServiceException(String message, Object invalidValue, String information, String causeMsg,
			HttpStatus httpStatus) {
		this(message, invalidValue, information, causeMsg, httpStatus, null);
	}

	public ServiceException(String message, Object invalidValue, String information, Throwable t) {
		this(message, invalidValue, information, t.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, t);
	}

	public ServiceException(Throwable t) {
		this(null, t);
	}

	public ServiceException(String message, Throwable t) {
		this(message != null ? message : t.getMessage(), null, null, t.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
				t);
	}

	public Object getInvalidValue() {
		return invalidValue;
	}

	public String getInformation() {
		return information;
	}

	public String getCauseMsg() {
		return causeMsg;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public Throwable getThrowable() {
		return cause;
	}

}
