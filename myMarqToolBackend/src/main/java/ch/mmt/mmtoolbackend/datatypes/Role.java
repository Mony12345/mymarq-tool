package ch.mmt.mmtoolbackend.datatypes;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Role {
	COMPANY("company"), CONSULTANT("consultant"), ADMIN("admin");

	private String serializedValue;

	private Role(String serializedValue) {
		this.serializedValue = serializedValue;
	}

	@JsonValue
	@Override
	public String toString() {
		return serializedValue;
	}

	public static Role fromString(String value) {
		if (value == null) {
			return null;
		}
		for (Role type : values()) {
			if (type.serializedValue.equals(value)) {
				return type;
			}
		}

		return null;
	}

	public boolean isCompany() {
		return equals(COMPANY);
	}

	public boolean isConsultant() {
		return equals(CONSULTANT);
	}

	public boolean isAdmin() {
		return equals(ADMIN);
	}
}
