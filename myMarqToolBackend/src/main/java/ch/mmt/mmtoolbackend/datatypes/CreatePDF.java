package ch.mmt.mmtoolbackend.datatypes;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Paths;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import ch.mmt.mmtoolbackend.domain.model.Consultation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CreatePDF {

	private Font title = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	private Font small = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
	private Font smallwhite = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
	private Font smallsmall = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
	private String DIR_IMAGES = Paths.get("").toAbsolutePath().toString() + File.separator + "report" + File.separator;
	private String FINGER_IMAGES = Paths.get("").toAbsolutePath().toString() + File.separator + "fingerprints" + File.separator;
	private Consultation c;
	private ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

	public byte[] newConsultationPDF(Consultation c) {
		this.c = c;
		byte[] pdfBytes = null;
		try {
			Document document = new Document();
			//PdfWriter.getInstance(document, new FileOutputStream(DIR_IMAGES + "First.pdf"));
			PdfWriter.getInstance(document, byteArrayOutputStream); // Do this BEFORE document.open()

			document.open();
			addMetaData(document);
			addContent(document);
			document.close();

			pdfBytes = byteArrayOutputStream.toByteArray();
		} catch (Exception e) {
			log.error("Problem with the creation of PDF");
		}
		return pdfBytes;
	}

	private void addMetaData(Document document) {
		document.addTitle("Profile myMarq");
		document.addKeywords("MyMarq, Profile, Fingerprint");
		document.addAuthor("MyMarq");
		document.addCreator("MyMarq");
	}

	private void addContent(Document document) throws Exception, DocumentException {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(DIR_IMAGES + c.getUser().getLanguage().toString().toLowerCase() + ".json"));
		JSONObject text = (JSONObject) obj;

		HashMap<String, String> fingerMap = new HashMap<>();
		fingerMap.put("P", "5");
		fingerMap.put("W", "5");
		fingerMap.put("C", "4");
		fingerMap.put("L", "3");
		fingerMap.put("LT", "3+2");
		fingerMap.put("T", "2");
		fingerMap.put("LA", "3+1");
		fingerMap.put("A", "1");

		PdfPTable table = new PdfPTable(11);
		if (c.getUser().getLanguage().toString().toLowerCase().equals("de")) {
			table.setTotalWidth(555);
		} else {
			table.setTotalWidth(525);
		}
		table.setWidths(new float[] { 15, 12, 3, 12, 3, 12, 3, 12, 3, 12, 3 });
		table.setLockedWidth(true);

		//row empty
		PdfPCell cell = new PdfPCell(new Phrase("", title));
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setFixedHeight(20);
		cell.setColspan(11);
		table.addCell(cell);

		//row 0
		Image img1 = Image.getInstance(DIR_IMAGES + "logo.png");
		cell = new PdfPCell(img1);
		cell.setFixedHeight(75);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(11);
		table.addCell(cell);

		//row empty
		cell = new PdfPCell(new Phrase("", title));
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setFixedHeight(15);
		cell.setColspan(11);
		table.addCell(cell);

		//row 1
		cell = new PdfPCell(new Phrase((String) text.get("character_title"), title));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(11);
		table.addCell(cell);
		//row empty
		cell = new PdfPCell(new Phrase("", title));
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setFixedHeight(5);
		cell.setColspan(11);
		table.addCell(cell);

		//row 2
		cell = new PdfPCell(new Phrase((String) text.get("pattern"), small));
		cell.setFixedHeight(70);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		String[] images = { "laccio.png", "vortice.png", "composite.png", "abete.png", "arco.png" };
		for (int i = 0; i < 5; i++) {
			img1 = Image.getInstance(DIR_IMAGES + images[i]);
			float prop = 65 / img1.getHeight();
			img1.scaleAbsoluteHeight(65);
			img1.scaleAbsoluteWidth(prop * img1.getWidth());
			cell = new PdfPCell(img1);
			cell.setFixedHeight(70);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setColspan(2);
			table.addCell(cell);
		}

		//row 3
		cell = new PdfPCell(new Phrase((String) text.get("name"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("loop"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("whorl"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("composite"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("tree"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("arch"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);

		//row 4
		cell = new PdfPCell(new Phrase((String) text.get("quantity"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("6+", small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("2+", small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("1+", small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("0.5+", small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("0.5+", small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setColspan(2);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);

		//row 5
		cell = new PdfPCell(new Phrase((String) text.get("character"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(90);
		cell.setRowspan(2);
		table.addCell(cell);

		String[] images2 = { "armonizzatore.png", "intraprendente.png", "mediatore.png", "entusiasta.png", "difensore.png" };
		for (int i = 0; i < 5; i++) {
			img1 = Image.getInstance(DIR_IMAGES + images2[i]);
			float prop = 65 / img1.getHeight();
			img1.scaleAbsoluteHeight(65);
			img1.scaleAbsoluteWidth(prop * img1.getWidth());
			cell = new PdfPCell(img1);
			cell.setFixedHeight(70);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setColspan(2);
			table.addCell(cell);
		}

		//row 6
		//first cell no need because row 4 has rowspan
		cell = new PdfPCell(new Phrase((String) text.get("harmonizer"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(20);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getHarmonizer() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);

		cell = new PdfPCell(new Phrase((String) text.get("enterprising"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getEnterprising() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("mediator"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getMediator() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("enthusiast"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getEnthusiast() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("defender"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getDefender() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);

		//row empty
		cell = new PdfPCell(new Phrase("", title));
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setFixedHeight(25);
		cell.setColspan(11);
		table.addCell(cell);

		//row 7
		cell = new PdfPCell(new Phrase((String) text.get("potential_challenge"), title));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(11);
		table.addCell(cell);

		//row empty
		cell = new PdfPCell(new Phrase("", title));
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setFixedHeight(5);
		cell.setColspan(11);
		table.addCell(cell);

		//row 8
		cell = new PdfPCell(new Phrase((String) text.get("quotient"), smallsmall));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("whorl") + " 5", smallsmall));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("composite") + " 4", smallsmall));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("loop") + " 3", smallsmall));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("tree") + " 2", smallsmall));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("arch") + " 1", smallsmall));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);

		//row empty
		cell = new PdfPCell(new Phrase("", title));
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setFixedHeight(10);
		cell.setColspan(11);
		table.addCell(cell);

		//row 9
		cell = new PdfPCell(new Phrase((String) text.get("hand_r"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("1 - " + (String) text.get("thumb"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("2 - " + (String) text.get("index"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("3 - " + (String) text.get("middle"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("4 - " + (String) text.get("ring"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("5 - " + (String) text.get("little"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);

		//row 10
		cell = new PdfPCell(new Phrase((String) text.get("pattern"), small));
		cell.setFixedHeight(100);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);

		for (int i = 1; i <= 5; i++) {
			String fingerprintPath = FINGER_IMAGES + c.getUuid() + File.separator + i + ".png";
			cell = new PdfPCell(new Phrase("", small));
			if (new File(fingerprintPath).exists()) {
				img1 = Image.getInstance(fingerprintPath);
				float prop = 95 / img1.getHeight();
				img1.scaleAbsoluteHeight(95);
				img1.scaleAbsoluteWidth(prop * img1.getWidth());
				cell = new PdfPCell(img1);
			}
			cell.setFixedHeight(100);
			if (i == 1 || i == 2 || i == 4) {
				cell.setBorderWidth(2);
			}
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setColspan(2);
			table.addCell(cell);
		}

		//row 10
		cell = new PdfPCell(new Phrase((String) text.get("quotient"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);

		cell = new PdfPCell(new Phrase(fingerMap.get(c.getFingerprint1()), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase(fingerMap.get(c.getFingerprint2()), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase(fingerMap.get(c.getFingerprint3()), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase(fingerMap.get(c.getFingerprint4()), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase(fingerMap.get(c.getFingerprint5()), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);

		//row 11
		cell = new PdfPCell(new Phrase((String) text.get("potential"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBackgroundColor(WebColors.getRGBColor("#76D6FF"));
		cell.setFixedHeight(20);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("handyman"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getHandyman() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("leader"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getLeader() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("organized"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getOrganized() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("performer"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getPerformer() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("communicator"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getCommunicator() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);

		//row 12
		cell = new PdfPCell(new Phrase((String) text.get("challenge"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBackgroundColor(WebColors.getRGBColor("#76D6FF"));
		cell.setFixedHeight(20);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("stuck"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getGetstuck() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("doubts"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getDoubts() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("opposition"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getOpposition() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("passivity"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getPassivity() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("taciturn"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getTaciturn() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);

		//row empty
		cell = new PdfPCell(new Phrase("", title));
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setFixedHeight(20);
		cell.setColspan(11);
		table.addCell(cell);

		//row 13
		cell = new PdfPCell(new Phrase((String) text.get("hand_l"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("6 - " + (String) text.get("thumb"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("7 - " + (String) text.get("index"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("8 - " + (String) text.get("middle"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("9 - " + (String) text.get("ring"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("10 - " + (String) text.get("little"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);

		//row 14
		cell = new PdfPCell(new Phrase((String) text.get("pattern"), small));
		cell.setFixedHeight(100);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);

		for (int i = 6; i <= 10; i++) {
			String fingerprintPath = FINGER_IMAGES + c.getUuid() + File.separator + i + ".png";
			cell = new PdfPCell(new Phrase("", small));
			if (new File(fingerprintPath).exists()) {
				img1 = Image.getInstance(fingerprintPath);
				float prop = 95 / img1.getHeight();
				img1.scaleAbsoluteHeight(95);
				img1.scaleAbsoluteWidth(prop * img1.getWidth());
				cell = new PdfPCell(img1);
			}
			cell.setFixedHeight(100);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			if (i == 8 || i == 10) {
				cell.setBorderWidth(2);
			}
			cell.setColspan(2);
			table.addCell(cell);
		}

		//row 15
		cell = new PdfPCell(new Phrase((String) text.get("quotient"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);

		cell = new PdfPCell(new Phrase(fingerMap.get(c.getFingerprint6()), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase(fingerMap.get(c.getFingerprint7()), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase(fingerMap.get(c.getFingerprint8()), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase(fingerMap.get(c.getFingerprint9()), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase(fingerMap.get(c.getFingerprint10()), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setColspan(2);
		table.addCell(cell);

		//row 11
		cell = new PdfPCell(new Phrase((String) text.get("potential"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBackgroundColor(WebColors.getRGBColor("#76D6FF"));
		cell.setFixedHeight(20);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("supporter"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getSupporter() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("motivator"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getMotivator() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("mentor"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getMentor() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("innovator"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getInnovator() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("catalyst"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getCatalyst() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);

		//row 12
		cell = new PdfPCell(new Phrase((String) text.get("challenge"), smallwhite));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBackgroundColor(WebColors.getRGBColor("#76D6FF"));
		cell.setFixedHeight(20);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("retreat"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getRetreat() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("tobear"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getTobear() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("guiltfeelings"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getGuiltfeelings() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("hide"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getHide() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase((String) text.get("submit"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
		img1 = Image.getInstance(DIR_IMAGES + (c.getSubmit() ? "check1.png" : "check0.png"));
		cell = new PdfPCell(img1);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setFixedHeight(5);
		table.addCell(cell);

		//row 13
		cell = new PdfPCell(new Phrase((String) text.get("footer"), small));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(11);
		table.addCell(cell);

		document.add(table);
	}

}
