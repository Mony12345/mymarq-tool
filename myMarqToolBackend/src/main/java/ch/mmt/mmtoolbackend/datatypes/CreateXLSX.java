package ch.mmt.mmtoolbackend.datatypes;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import ch.mmt.mmtoolbackend.domain.model.Consultation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CreateXLSX {

	public byte[] newConsultationsXLSX(List<Consultation> consultations) {
		byte[] xlsxBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();

			XSSFSheet sheet = workbook.createSheet("Profiles");

			XSSFCreationHelper createHelper = workbook.getCreationHelper();

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 14);

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

			Font handFont = workbook.createFont();
			handFont.setBold(true);
			handFont.setColor(IndexedColors.RED.getIndex());
			handFont.setFontHeightInPoints((short) 14);

			CellStyle handCellStyle = workbook.createCellStyle();
			handCellStyle.setFont(handFont);
			handCellStyle.setFillBackgroundColor(IndexedColors.BLUE_GREY.getIndex());
			handCellStyle.setAlignment(HorizontalAlignment.CENTER);

			Font boldFont = workbook.createFont();
			boldFont.setBold(true);
			boldFont.setFontHeightInPoints((short) 12);

			CellStyle boldCellStyle = workbook.createCellStyle();
			boldCellStyle.setFont(boldFont);
			boldCellStyle.setAlignment(HorizontalAlignment.CENTER);

			Font normalFont = workbook.createFont();
			normalFont.setFontHeightInPoints((short) 12);

			CellStyle normalCellStyle = workbook.createCellStyle();
			normalCellStyle.setFont(normalFont);
			normalCellStyle.setAlignment(HorizontalAlignment.CENTER);

			int r = 0;

			// row 0
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 15));
			XSSFRow headerRow = sheet.createRow(r);
			XSSFCell cell = headerRow.createCell(1);
			cell.setCellValue("MYMARQ GLOBAL SPREADSHEET");
			cell.setCellStyle(headerCellStyle);
			r++;

			// row 1
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 2, 6));
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 7, 11));
			XSSFRow row = sheet.createRow(r);
			cell = row.createCell(2);
			cell.setCellValue("R");
			cell.setCellStyle(handCellStyle);
			cell = row.createCell(7);
			cell.setCellValue("L");
			cell.setCellStyle(handCellStyle);
			r++;

			// row 2
			String[] headerTable = { "CMC", "1 - Thumb", "2 - Index", "3 - Middle", "4 - Ring",
					"5 - Little", "6 - Thumb", "7 - Index", "8 - Middle", "9 - Ring", "10 - Little",
					"Character", "Potential", "Challenges", "DoBirth", "Gender", "Nationality" };
			row = sheet.createRow(r);
			for (int i = 1; i <= 17; i++) {
				cell = row.createCell(i);
				cell.setCellValue(headerTable[i - 1]);
				cell.setCellStyle(boldCellStyle);
			}
			r++;

			String oldYear = "";

			for (Consultation c : consultations) {
				row = sheet.createRow(r);
				cell = row.createCell(0);
				String year = new SimpleDateFormat("yyyy").format(Date.from(c.getCreatedAt()));
				if (!oldYear.equals(year)) {
					cell.setCellValue(year);
					oldYear = year;
				} else {
					cell.setCellValue("");
				}
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(1);
				cell.setCellValue((c.getUser() != null)
						? c.getUser().getFirstname().substring(0, 1) + ". " + c.getUser().getLastname().substring(0, 1) + ". " + c.getUuid().toString().substring(0, 8)
						: "-");
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(2);
				cell.setCellValue((c.getFingerprint1() != null && c.getFingerprint1().equals("P")) ? "W" : c.getFingerprint1());
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(3);
				cell.setCellValue((c.getFingerprint2() != null && c.getFingerprint2().equals("P")) ? "W" : c.getFingerprint2());
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(4);
				cell.setCellValue((c.getFingerprint3() != null && c.getFingerprint3().equals("P")) ? "W" : c.getFingerprint3());
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(5);
				cell.setCellValue((c.getFingerprint4() != null && c.getFingerprint4().equals("P")) ? "W" : c.getFingerprint4());
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(6);
				cell.setCellValue((c.getFingerprint5() != null && c.getFingerprint5().equals("P")) ? "W" : c.getFingerprint5());
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(7);
				cell.setCellValue((c.getFingerprint6() != null && c.getFingerprint6().equals("P")) ? "W" : c.getFingerprint6());
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(8);
				cell.setCellValue((c.getFingerprint7() != null && c.getFingerprint7().equals("P")) ? "W" : c.getFingerprint7());
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(9);
				cell.setCellValue((c.getFingerprint8() != null && c.getFingerprint8().equals("P")) ? "W" : c.getFingerprint8());
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(10);
				cell.setCellValue((c.getFingerprint9() != null && c.getFingerprint9().equals("P")) ? "W" : c.getFingerprint9());
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(11);
				cell.setCellValue((c.getFingerprint10() != null && c.getFingerprint10().equals("P")) ? "W" : c.getFingerprint10());
				cell.setCellStyle(normalCellStyle);

				String charact = "";
				charact = (c.getHarmonizer() ? "P+" : "") + (c.getEnterprising() ? "A+" : "") + (c.getMediator() ? "M+" : "")
						+ (c.getEnthusiast() ? "E+" : "") + (c.getDefender() ? "D+" : "");
				if (charact.length() > 0) {
					charact = charact.substring(0, charact.length() - 1);
				}
				cell = row.createCell(12);
				cell.setCellValue(charact);
				cell.setCellStyle(normalCellStyle);

				String potential = "";
				potential = (c.getHandyman() ? "1+" : "") + (c.getLeader() ? "2+" : "") + (c.getOrganized() ? "3+" : "")
						+ (c.getPerformer() ? "4+" : "") + (c.getCommunicator() ? "5+" : "") + (c.getSupporter() ? "6+" : "")
						+ (c.getMotivator() ? "7+" : "") + (c.getMentor() ? "8+" : "") + (c.getInnovator() ? "9+" : "")
						+ (c.getCatalyst() ? "10+" : "");
				if (potential.length() > 0) {
					potential = potential.substring(0, potential.length() - 1);
				}
				cell = row.createCell(13);
				cell.setCellValue(potential);
				cell.setCellStyle(normalCellStyle);

				String challenge = "";
				challenge = (c.getGetstuck() ? "1+" : "") + (c.getDoubts() ? "2+" : "") + (c.getOpposition() ? "3+" : "")
						+ (c.getPassivity() ? "4+" : "") + (c.getTaciturn() ? "5+" : "") + (c.getRetreat() ? "6+" : "")
						+ (c.getTobear() ? "7+" : "") + (c.getGuiltfeelings() ? "8+" : "") + (c.getHide() ? "9+" : "")
						+ (c.getSubmit() ? "10+" : "");
				if (challenge.length() > 0) {
					challenge = challenge.substring(0, challenge.length() - 1);
				}
				cell = row.createCell(14);
				cell.setCellValue(challenge);
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(15);
				cell.setCellValue(c.getDateBirth() == null ? "?" : new SimpleDateFormat("dd.MM.yyyy").format(c.getDateBirth()));
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(16);
				cell.setCellValue(c.getGender() == null ? "?" : c.getGender());
				cell.setCellStyle(normalCellStyle);

				cell = row.createCell(17);
				cell.setCellValue(c.getCountry() == null ? "?" : c.getCountry());
				cell.setCellStyle(normalCellStyle);
				r++;
			}

			for (int i = 0; i < 16; i++) {
				sheet.autoSizeColumn(i);
			}

			ByteArrayOutputStream fileOut = new ByteArrayOutputStream();
			//FileOutputStream fileOut = new FileOutputStream("test.xlsx");
			workbook.write(fileOut);
			fileOut.flush();
			//fileOut.close();
			// Closing the workbook
			workbook.close();
			xlsxBytes = fileOut.toByteArray();
		} catch (Exception e) {
			log.error("Error during the creation of XLSX");
		}

		return xlsxBytes;
	}
}
