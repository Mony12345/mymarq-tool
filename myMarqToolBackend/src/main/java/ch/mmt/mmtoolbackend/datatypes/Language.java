package ch.mmt.mmtoolbackend.datatypes;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Language {
	DEU("de"), ITA("it"), ENG("en");

	private String serializedValue;

	private Language(String serializedValue) {
		this.serializedValue = serializedValue;
	}

	@JsonValue
	@Override
	public String toString() {
		return serializedValue;
	}

	public static Language fromString(String value) {
		if (value == null) {
			return null;
		}
		for (Language type : values()) {
			if (type.serializedValue.equals(value)) {
				return type;
			}
		}

		return null;
	}
}

