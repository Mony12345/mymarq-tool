package ch.mmt.mmtoolbackend.domain.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Entity(name = "consultation")
@Table(name = "consultation")
@NamedQueries({
		@NamedQuery(query = "SELECT c FROM consultation c " + Consultation.FIND_REVISION_WHERE_CLAUSE
				+ "ORDER BY c.lastUpdateAt DESC", name = "Consultation.findFilteredOrderByLastUpdateAtDesc"),
		@NamedQuery(query = "SELECT c FROM consultation c ORDER BY c.createdAt", name = "Consultation.findOrderByCreatedAt"),
		@NamedQuery(query = "SELECT COUNT(c) FROM consultation c "
				+ Consultation.FIND_REVISION_WHERE_CLAUSE, name = "Consultation.countFiltered")

})

public class Consultation extends BaseEntity {

	public static final String FIND_REVISION_WHERE_CLAUSE = "WHERE (:firstname IS NULL OR lower(COALESCE(c.firstname, '')) LIKE lower(CAST(:firstname AS string))) "
			+ "AND  (:lastname IS NULL OR lower(COALESCE(c.lastname, '')) LIKE lower(CAST(:lastname AS string))) "
			+ "AND (:userUUID IS NULL OR c.user.uuid = :userUUID) ";

	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (handyman == null) {
			handyman = Boolean.FALSE;
		}
		if (leader == null) {
			leader = Boolean.FALSE;
		}
		if (organized == null) {
			organized = Boolean.FALSE;
		}
		if (performer == null) {
			performer = Boolean.FALSE;
		}
		if (communicator == null) {
			communicator = Boolean.FALSE;
		}
		if (getstuck == null) {
			getstuck = Boolean.FALSE;
		}
		if (doubts == null) {
			doubts = Boolean.FALSE;
		}
		if (opposition == null) {
			opposition = Boolean.FALSE;
		}
		if (passivity == null) {
			passivity = Boolean.FALSE;
		}
		if (taciturn == null) {
			taciturn = Boolean.FALSE;
		}
		if (supporter == null) {
			supporter = Boolean.FALSE;
		}
		if (motivator == null) {
			motivator = Boolean.FALSE;
		}
		if (mentor == null) {
			mentor = Boolean.FALSE;
		}
		if (innovator == null) {
			innovator = Boolean.FALSE;
		}
		if (catalyst == null) {
			catalyst = Boolean.FALSE;
		}
		if (retreat == null) {
			retreat = Boolean.FALSE;
		}
		if (tobear == null) {
			tobear = Boolean.FALSE;
		}
		if (guiltfeelings == null) {
			guiltfeelings = Boolean.FALSE;
		}
		if (hide == null) {
			hide = Boolean.FALSE;
		}
		if (submit == null) {
			submit = Boolean.FALSE;
		}
		if (harmonizer == null) {
			harmonizer = Boolean.FALSE;
		}
		if (enterprising == null) {
			enterprising = Boolean.FALSE;
		}
		if (mediator == null) {
			mediator = Boolean.FALSE;
		}
		if (enthusiast == null) {
			enthusiast = Boolean.FALSE;
		}
		if (defender == null) {
			defender = Boolean.FALSE;
		}

	}

	private String firstname;

	private String lastname;

	private String email;

	private String street;

	private String streetNumber;

	private String cap;

	private String city;

	private String country;

	private String phoneNumber;

	private Date dateBirth;
	private String gender;
	private String nationality;
	private String civilStatus;
	private int numberChild;
	private String professional;
	private String education;
	private String fingerprint1;
	private String fingerprint2;
	private String fingerprint3;
	private String fingerprint4;
	private String fingerprint5;
	private String fingerprint6;
	private String fingerprint7;
	private String fingerprint8;
	private String fingerprint9;
	private String fingerprint10;

	private Boolean handyman;
	private Boolean leader;
	private Boolean organized;
	private Boolean performer;
	private Boolean communicator;
	private Boolean getstuck;
	private Boolean doubts;
	private Boolean opposition;
	private Boolean passivity;
	private Boolean taciturn;
	private Boolean supporter;
	private Boolean motivator;
	private Boolean mentor;
	private Boolean innovator;
	private Boolean catalyst;
	private Boolean retreat;
	private Boolean tobear;
	private Boolean guiltfeelings;
	private Boolean hide;
	private Boolean submit;
	private Boolean harmonizer;
	private Boolean enterprising;
	private Boolean mediator;
	private Boolean enthusiast;
	private Boolean defender;

	@OneToOne
	@JoinColumn(name = "user_id", nullable = false)
	@EqualsAndHashCode.Exclude
	@JsonIgnoreProperties("consultations")
	private User user;

}
