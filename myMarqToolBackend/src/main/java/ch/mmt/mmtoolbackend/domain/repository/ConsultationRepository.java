package ch.mmt.mmtoolbackend.domain.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ch.mmt.mmtoolbackend.domain.model.Consultation;

@Repository
public interface ConsultationRepository extends JpaRepository<Consultation, Long> {

	Consultation findByUuid(UUID uuid);

	@Query(name = "Consultation.findFilteredOrderByLastUpdateAtDesc")
	List<Consultation> findFilteredOrderByLastUpdateAtDesc(
			@Param("firstname") String firstname,
			@Param("lastname") String lastname,
			@Param("userUUID") UUID userUUID,
			Pageable pageable);

	@Query(name = "Consultation.findOrderByCreatedAt")
	List<Consultation> findOrderByCreatedAt();

	@Query(name = "Consultation.countFiltered")
	Long countFiltered(
			@Param("firstname") String firstname,
			@Param("lastname") String lastname,
			@Param("userUUID") UUID userUUID);

}
