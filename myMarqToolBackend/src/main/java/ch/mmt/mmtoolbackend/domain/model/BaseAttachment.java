package ch.mmt.mmtoolbackend.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity(name = "attachment")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class BaseAttachment extends BaseEntity {

	@Column(columnDefinition = "LONGBLOB")
	private byte[] attachment;

//	private long resourceId;

}
