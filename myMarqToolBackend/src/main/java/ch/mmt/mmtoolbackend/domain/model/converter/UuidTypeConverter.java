package ch.mmt.mmtoolbackend.domain.model.converter;

import java.util.UUID;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class UuidTypeConverter implements AttributeConverter<UUID, String> {
	@Override
	public String convertToDatabaseColumn(UUID uuid) {
		return uuid == null ? null : uuid.toString();
	}

	@Override
	public UUID convertToEntityAttribute(String value) {
		return value == null || "".equals(value) ? null : UUID.fromString(value);
	}
}
