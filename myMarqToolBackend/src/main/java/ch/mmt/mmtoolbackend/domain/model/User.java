package ch.mmt.mmtoolbackend.domain.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ch.mmt.mmtoolbackend.datatypes.Language;
import ch.mmt.mmtoolbackend.datatypes.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(exclude = { "password" })
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Entity(name = "user")
@Table(name = "user")
@NamedQueries({
		@NamedQuery(query = "SELECT u FROM user u " + User.FIND_USER_WHERE_CLAUSE
				+ "ORDER BY u.email ASC", name = "User.findFilteredOrderByEmailAsc"),
		@NamedQuery(query = "SELECT COUNT(u) FROM user u "
				+ User.FIND_USER_WHERE_CLAUSE, name = "User.countFiltered"),

})

public class User extends BaseEntity {

	public static final String FIND_USER_WHERE_CLAUSE = "WHERE (:email IS NULL OR lower(COALESCE(u.email, '')) LIKE lower(CAST(:email AS string))) "
			+ "AND (:firstname IS NULL OR lower(COALESCE(u.firstname, '')) LIKE lower(CAST(:firstname AS string))) "
			+ "AND (:lastname IS NULL OR lower(COALESCE(u.lastname, '')) LIKE lower(CAST(:lastname AS string))) "
			+ "AND (:language IS NULL OR u.language = :language) "
			+ "AND (:role IS NULL OR u.role = :role) "
			+ "AND (:disabled IS NULL OR u.disabled = :disabled) ";

	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (disabled == null) {
			disabled = Boolean.FALSE;
		}
	}

	private String email;

	private String password;

	private String firstname;

	private String lastname;

	@Enumerated(EnumType.STRING)
	private Language language;

	private Boolean disabled;

	@Enumerated(EnumType.STRING)
	private Role role;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	@EqualsAndHashCode.Exclude
	@JsonIgnoreProperties("user")
	protected Set<Consultation> consultations;

}
