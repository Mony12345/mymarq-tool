package ch.mmt.mmtoolbackend.domain.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ch.mmt.mmtoolbackend.datatypes.Language;
import ch.mmt.mmtoolbackend.datatypes.Role;
import ch.mmt.mmtoolbackend.domain.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmailAndDisabledIsFalse(String email);

	Boolean existsByEmailAndDisabledIsFalse(String email);

	User findByUuid(UUID uuid);

	@Query(name = "User.findFilteredOrderByEmailAsc")
	List<User> findFilteredOrderByNameAsc(
			@Param("email") String email,
			@Param("firstname") String firstname,
			@Param("lastname") String lastname,
			@Param("language") Language language,
			@Param("role") Role role,
			@Param("disabled") Boolean disabled,
			Pageable pageable);

	@Query(name = "User.countFiltered")
	Long countFiltered(
			@Param("email") String email,
			@Param("firstname") String firstname,
			@Param("lastname") String lastname,
			@Param("language") Language language,
			@Param("role") Role role,
			@Param("disabled") Boolean disabled);

}
