package ch.mmt.mmtoolbackend.domain.model;

import java.time.Instant;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import ch.mmt.mmtoolbackend.domain.model.converter.UuidTypeConverter;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public class BaseEntity {

	@PrePersist
	public void prePersist() {
		if (createdAt == null) {
			createdAt = Instant.now();
		}
		if (lastUpdateAt == null) {
			lastUpdateAt = Instant.now();
		}
		if (uuid == null) {
			uuid = UUID.randomUUID();
		}
	}

	@PreUpdate
	public void preUpdate() {
		lastUpdateAt = Instant.now();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	@Column
	@Convert(converter = UuidTypeConverter.class)
	private UUID uuid;

	@Column
	protected Instant lastUpdateAt;

	@Column
	protected Instant createdAt;
}
