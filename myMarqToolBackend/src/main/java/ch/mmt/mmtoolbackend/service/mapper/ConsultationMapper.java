package ch.mmt.mmtoolbackend.service.mapper;

import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.api.model.ConsultationModel;
import ch.mmt.mmtoolbackend.domain.model.Consultation;
import ch.mmt.mmtoolbackend.service.validator.ConsultationValidator;

public class ConsultationMapper {

	public static ConsultationModel toConsultationModel(Consultation consultation) {
		ConsultationModel model = new ConsultationModel();
		model.uuid = consultation.getUuid();
		model.firstname = consultation.getFirstname();
		model.lastname = consultation.getLastname();
		model.createdAt = consultation.getCreatedAt();
		model.lastUpdateAt = consultation.getLastUpdateAt();

		model.email = consultation.getEmail();
		model.street = consultation.getStreet();
		model.streetNumber = consultation.getStreetNumber();
		model.cap = consultation.getCap();
		model.city = consultation.getCity();
		model.country = consultation.getCountry();
		model.phoneNumber = consultation.getPhoneNumber();
		model.dateBirth = consultation.getDateBirth();
		model.gender = consultation.getGender();
		model.nationality = consultation.getNationality();
		model.civilStatus = consultation.getCivilStatus();
		model.numberChild = consultation.getNumberChild();
		model.professional = consultation.getProfessional();
		model.education = consultation.getEducation();
		model.fingerprint1 = consultation.getFingerprint1();
		model.fingerprint2 = consultation.getFingerprint2();
		model.fingerprint3 = consultation.getFingerprint3();
		model.fingerprint4 = consultation.getFingerprint4();
		model.fingerprint5 = consultation.getFingerprint5();
		model.fingerprint6 = consultation.getFingerprint6();
		model.fingerprint7 = consultation.getFingerprint7();
		model.fingerprint8 = consultation.getFingerprint8();
		model.fingerprint9 = consultation.getFingerprint9();
		model.fingerprint10 = consultation.getFingerprint10();
		model.handyman = consultation.getHandyman();
		model.leader = consultation.getLeader();
		model.organized = consultation.getOrganized();
		model.performer = consultation.getPerformer();
		model.communicator = consultation.getCommunicator();
		model.getstuck = consultation.getGetstuck();
		model.doubts = consultation.getDoubts();
		model.opposition = consultation.getOpposition();
		model.passivity = consultation.getPassivity();
		model.taciturn = consultation.getTaciturn();
		model.supporter = consultation.getSupporter();
		model.motivator = consultation.getMotivator();
		model.mentor = consultation.getMentor();
		model.innovator = consultation.getInnovator();
		model.catalyst = consultation.getCatalyst();
		model.retreat = consultation.getRetreat();
		model.tobear = consultation.getTobear();
		model.guiltfeelings = consultation.getGuiltfeelings();
		model.hide = consultation.getHide();
		model.submit = consultation.getSubmit();
		model.harmonizer = consultation.getHarmonizer();
		model.enterprising = consultation.getEnterprising();
		model.mediator = consultation.getMediator();
		model.enthusiast = consultation.getEnthusiast();
		model.defender = consultation.getDefender();
		if (consultation.getUser() != null) {
			model.userUUID = consultation.getUser().getUuid();
			model.userFirstName = consultation.getUser().getFirstname();
			model.userLastName = consultation.getUser().getLastname();
		}
		return model;
	}

	public static Consultation toConsultation(ConsultationModel consultationModel) {
		Consultation consultation = new Consultation();
		consultation.setFirstname(consultationModel.firstname);
		consultation.setLastname(consultationModel.lastname);

		consultation.setEmail(consultationModel.email);
		consultation.setStreet(consultationModel.street);
		consultation.setStreetNumber(consultationModel.streetNumber);
		consultation.setCap(consultationModel.cap);
		consultation.setCity(consultationModel.city);
		consultation.setCountry(consultationModel.country);
		consultation.setPhoneNumber(consultationModel.phoneNumber);
		consultation.setDateBirth(consultationModel.dateBirth);
		consultation.setGender(consultationModel.gender);
		consultation.setNationality(consultationModel.nationality);
		consultation.setCivilStatus(consultationModel.civilStatus);
		consultation.setNumberChild(consultationModel.numberChild);
		consultation.setProfessional(consultationModel.professional);
		consultation.setEducation(consultationModel.education);
		consultation.setFingerprint1(consultationModel.fingerprint1);
		consultation.setFingerprint2(consultationModel.fingerprint2);
		consultation.setFingerprint3(consultationModel.fingerprint3);
		consultation.setFingerprint4(consultationModel.fingerprint4);
		consultation.setFingerprint5(consultationModel.fingerprint5);
		consultation.setFingerprint6(consultationModel.fingerprint6);
		consultation.setFingerprint7(consultationModel.fingerprint7);
		consultation.setFingerprint8(consultationModel.fingerprint8);
		consultation.setFingerprint9(consultationModel.fingerprint9);
		consultation.setFingerprint10(consultationModel.fingerprint10);
		consultation.setHandyman(consultationModel.handyman);
		consultation.setLeader(consultationModel.leader);
		consultation.setOrganized(consultationModel.organized);
		consultation.setPerformer(consultationModel.performer);
		consultation.setCommunicator(consultationModel.communicator);
		consultation.setGetstuck(consultationModel.getstuck);
		consultation.setDoubts(consultationModel.doubts);
		consultation.setOpposition(consultationModel.opposition);
		consultation.setPassivity(consultationModel.passivity);
		consultation.setTaciturn(consultationModel.taciturn);
		consultation.setSupporter(consultationModel.supporter);
		consultation.setMotivator(consultationModel.motivator);
		consultation.setMentor(consultationModel.mentor);
		consultation.setInnovator(consultationModel.innovator);
		consultation.setCatalyst(consultationModel.catalyst);
		consultation.setRetreat(consultationModel.retreat);
		consultation.setTobear(consultationModel.tobear);
		consultation.setGuiltfeelings(consultationModel.guiltfeelings);
		consultation.setHide(consultationModel.hide);
		consultation.setSubmit(consultationModel.submit);
		consultation.setHarmonizer(consultationModel.harmonizer);
		consultation.setEnterprising(consultationModel.enterprising);
		consultation.setMediator(consultationModel.mediator);
		consultation.setEnthusiast(consultationModel.enthusiast);
		consultation.setDefender(consultationModel.defender);

		return consultation;
	}

	public static Consultation replaceConsultationData(
			Consultation consultation,
			ConsultationModel newConsultationModel) throws ServiceException {
		ConsultationValidator.validateConsultationData(newConsultationModel, false);

		consultation.setFirstname(newConsultationModel.firstname);
		consultation.setLastname(newConsultationModel.lastname);

		consultation.setEmail(newConsultationModel.email);
		consultation.setStreet(newConsultationModel.street);
		consultation.setStreetNumber(newConsultationModel.streetNumber);
		consultation.setCap(newConsultationModel.cap);
		consultation.setCity(newConsultationModel.city);
		consultation.setCountry(newConsultationModel.country);
		consultation.setPhoneNumber(newConsultationModel.phoneNumber);
		consultation.setDateBirth(newConsultationModel.dateBirth);
		consultation.setGender(newConsultationModel.gender);
		consultation.setNationality(newConsultationModel.nationality);
		consultation.setCivilStatus(newConsultationModel.civilStatus);
		consultation.setNumberChild(newConsultationModel.numberChild);
		consultation.setProfessional(newConsultationModel.professional);
		consultation.setEducation(newConsultationModel.education);
		consultation.setFingerprint1(newConsultationModel.fingerprint1);
		consultation.setFingerprint2(newConsultationModel.fingerprint2);
		consultation.setFingerprint3(newConsultationModel.fingerprint3);
		consultation.setFingerprint4(newConsultationModel.fingerprint4);
		consultation.setFingerprint5(newConsultationModel.fingerprint5);
		consultation.setFingerprint6(newConsultationModel.fingerprint6);
		consultation.setFingerprint7(newConsultationModel.fingerprint7);
		consultation.setFingerprint8(newConsultationModel.fingerprint8);
		consultation.setFingerprint9(newConsultationModel.fingerprint9);
		consultation.setFingerprint10(newConsultationModel.fingerprint10);
		consultation.setHandyman(newConsultationModel.handyman);
		consultation.setLeader(newConsultationModel.leader);
		consultation.setOrganized(newConsultationModel.organized);
		consultation.setPerformer(newConsultationModel.performer);
		consultation.setCommunicator(newConsultationModel.communicator);
		consultation.setGetstuck(newConsultationModel.getstuck);
		consultation.setDoubts(newConsultationModel.doubts);
		consultation.setOpposition(newConsultationModel.opposition);
		consultation.setPassivity(newConsultationModel.passivity);
		consultation.setTaciturn(newConsultationModel.taciturn);
		consultation.setSupporter(newConsultationModel.supporter);
		consultation.setMotivator(newConsultationModel.motivator);
		consultation.setMentor(newConsultationModel.mentor);
		consultation.setInnovator(newConsultationModel.innovator);
		consultation.setCatalyst(newConsultationModel.catalyst);
		consultation.setRetreat(newConsultationModel.retreat);
		consultation.setTobear(newConsultationModel.tobear);
		consultation.setGuiltfeelings(newConsultationModel.guiltfeelings);
		consultation.setHide(newConsultationModel.hide);
		consultation.setSubmit(newConsultationModel.submit);
		consultation.setHarmonizer(newConsultationModel.harmonizer);
		consultation.setEnterprising(newConsultationModel.enterprising);
		consultation.setMediator(newConsultationModel.mediator);
		consultation.setEnthusiast(newConsultationModel.enthusiast);
		consultation.setDefender(newConsultationModel.defender);

		return consultation;
	}

}
