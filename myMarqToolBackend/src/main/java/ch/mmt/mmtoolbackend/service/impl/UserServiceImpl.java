package ch.mmt.mmtoolbackend.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.api.model.UserModel;
import ch.mmt.mmtoolbackend.api.model.patch.OperationModel;
import ch.mmt.mmtoolbackend.api.model.patch.ReplaceModel;
import ch.mmt.mmtoolbackend.api.search.model.RangeRequestModel;
import ch.mmt.mmtoolbackend.api.search.model.UserSearchModel;
import ch.mmt.mmtoolbackend.domain.model.User;
import ch.mmt.mmtoolbackend.domain.repository.UserRepository;
import ch.mmt.mmtoolbackend.service.mapper.UserMapper;
import ch.mmt.mmtoolbackend.service.validator.UserValidator;
import ch.mmt.mmtoolbackend.utils.FilterUtil;
import ch.mmt.mmtoolbackend.utils.PaginationUtil;
import ch.mmt.mmtoolbackend.utils.PatchUtil;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class UserServiceImpl {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder userPasswordEncoder;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	public User getEnableUserByEmail(String email) {
		return userRepository.findByEmailAndDisabledIsFalse(email);
	}

	public UserModel createUser(UserModel newUserModel) throws ServiceException {
		UserValidator.validateUserData(newUserModel, true);

		if (userRepository.existsByEmailAndDisabledIsFalse(newUserModel.email)) {
			throw new ServiceException("User already exists", newUserModel.email, "Please provide a new email", HttpStatus.BAD_REQUEST);
		}
		User user = UserMapper.toUser(newUserModel, userPasswordEncoder);

		user = userRepository.save(user);

		return UserMapper.toUserModel(user);
	}

	public UserModel getUserByUuid(UUID userUUID) throws ServiceException {
		User user = userRepository.findByUuid(userUUID);
		if (user == null) {
			throw new ServiceException("User not found", userUUID, "Please provide a valid and existing user uuid", HttpStatus.NOT_FOUND);
		}

		return UserMapper.toUserModel(user);
	}

	public void deleteUserByUUID(UUID userUUID, User authenticatedUser) throws ServiceException {
		User user = userRepository.findByUuid(userUUID);
		if (user == null) {
			throw new ServiceException("User not found", userUUID, "Please provide a valid and existing user uuid", HttpStatus.NOT_FOUND);
		}

		if (user.getId().equals(authenticatedUser.getId())) {
			throw new ServiceException("Cannot delete authenticated user", userUUID, "Please don't try to delete yourself", HttpStatus.FORBIDDEN);
		}

		userRepository.delete(user);
	}

	public Pair<List<UserModel>, Long> searchUsers(UserSearchModel searchModel, RangeRequestModel range) {
		Pageable pageable = null;
		Long count = 0l;

		if (range != null) {
			pageable = PaginationUtil.toPageable(range);
		}

		FilterUtil.computeFilter(searchModel);

		count = userRepository.countFiltered(searchModel.email, searchModel.firstname, searchModel.lastname, searchModel.language, searchModel.role,
				searchModel.disabled);

		List<User> users = userRepository.findFilteredOrderByNameAsc(searchModel.email, searchModel.firstname, searchModel.lastname, searchModel.language, searchModel.role,
				searchModel.disabled, pageable);

		return Pair.of(users.stream()
				.map(UserMapper::toUserModel)
				.collect(Collectors.toList()), count);
	}

	public UserModel applyPatchToUser(UUID userUUID, List<OperationModel> patch, boolean isAdmin) throws ServiceException, IOException {
		UserModel oldUserModel = null;
		UserModel newUserModel = null;

		// Validation
		List<OperationModel> invalidPatchOps = isAdmin ? UserValidator.validateUpdateUserPatchAdminOperations(patch) : UserValidator.validateUpdateUserPatchOperations(patch);
		if (!invalidPatchOps.isEmpty()) {
			log.warn("Invalid patch operations received (or patches' paths not supported)");
			throw new ServiceException("Invalid patch operations received (or patches' paths not supported)", patch, OperationModel.INFORMATION, HttpStatus.BAD_REQUEST);
		}

		User user = userRepository.findByUuid(userUUID);
		if (user == null) {
			throw new ServiceException("User not found", userUUID, "Please provide a valid and existing user uuid", HttpStatus.NOT_FOUND);
		}

		patch.stream().forEach(o -> {
			if (o instanceof ReplaceModel) {
				ReplaceModel replaceModel = (ReplaceModel) o;
				if (replaceModel.value.equals("")) {
					replaceModel.op = OperationModel.Kind.remove;
				}
			}
		});

		oldUserModel = UserMapper.toUserModel(user);
		newUserModel = PatchUtil.patchModel(oldUserModel, UserModel.class, patch);

		if (newUserModel != oldUserModel) {
			UserMapper.replaceUserData(user, newUserModel, userPasswordEncoder);

			user = userRepository.save(user);

			return UserMapper.toUserModel(user);
		} else {
			log.warn("No update needed!");
		}

		return newUserModel;
	}
}
