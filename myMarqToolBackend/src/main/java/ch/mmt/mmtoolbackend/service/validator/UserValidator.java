package ch.mmt.mmtoolbackend.service.validator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.api.model.UserModel;
import ch.mmt.mmtoolbackend.api.model.patch.OperationModel;
import ch.mmt.mmtoolbackend.utils.OperationModelUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserValidator {

	public static List<OperationModel> validateUpdateUserPatchOperations(List<OperationModel> patch) {
		List<OperationModel> invalidOps = new ArrayList<>();

		if ((patch == null) || patch.isEmpty()) {
			return invalidOps;
		}

		for (OperationModel operation : patch) {
			// test operation is ok
			if (OperationModelUtil.isTest(operation)) {
				continue;
			}
			// if path not valid
			if (!operation.path.equals("/firstname")
					&& !operation.path.equals("/lastname")
					&& !operation.path.equals("/password")
					&& !operation.path.equals("/language")
					&& !operation.path.equals("/enabled")
					&& !operation.path.equals("/email")) {
				invalidOps.add(operation);
			}

			// remove operation permitted only for nullable properties
			if ((operation.path.equals("/language") || operation.path.equals("/enabled")) && OperationModelUtil.isRemove(operation)) {
				invalidOps.add(operation);
			}
		}

		return invalidOps;
	}

	public static List<OperationModel> validateUpdateUserPatchAdminOperations(List<OperationModel> patch) {
		List<OperationModel> invalidOps = new ArrayList<>();

		if ((patch == null) || patch.isEmpty()) {
			return invalidOps;
		}

		for (OperationModel operation : patch) {
			// test operation is ok
			if (OperationModelUtil.isTest(operation)) {
				continue;
			}
			// if path not valid
			if (!operation.path.equals("/firstname")
					&& !operation.path.equals("/lastname")
					&& !operation.path.equals("/language")
					&& !operation.path.equals("/role")
					&& !operation.path.equals("/enabled")
					&& !operation.path.equals("/password")
					&& !operation.path.equals("/email")) {
				invalidOps.add(operation);
			}

			// remove operation permitted only for nullable properties
			if ((operation.path.equals("/language") || operation.path.equals("/role")
					|| operation.path.equals("/enabled")) && OperationModelUtil.isRemove(operation)) {
				invalidOps.add(operation);
			}
		}

		return invalidOps;
	}

	public static void validateUserData(UserModel user, boolean newUser) throws ServiceException {
		if ((user.email == null) || user.email.isEmpty()) {
			log.warn("User email cannot be null");
			throw new ServiceException("User email cannot be null", user.email, "new user data should be provide a user email", HttpStatus.BAD_REQUEST);
		}
		if (newUser && ((user.password == null) || user.password.isEmpty())) {
			log.warn("User password cannot be null");
			throw new ServiceException("User password cannot be null", user.password, "new user data should be provide a user password", HttpStatus.BAD_REQUEST);
		}
		if ((user.language == null)) {
			log.warn("User language cannot be null");
			throw new ServiceException("User language cannot be null", user.language, "new user data should be provide a user language", HttpStatus.BAD_REQUEST);
		}
		if (user.role == null) {
			log.warn("User role cannot be null");
			throw new ServiceException("User role cannot be null", user.role, "new user data should be provide a user role",
					HttpStatus.BAD_REQUEST);
		}
	}
}
