package ch.mmt.mmtoolbackend.service.validator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.api.model.ConsultationModel;
import ch.mmt.mmtoolbackend.api.model.patch.OperationModel;
import ch.mmt.mmtoolbackend.utils.OperationModelUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConsultationValidator {

	public static List<OperationModel> validateUpdateConsultationPatchOperations(List<OperationModel> patch) {
		List<OperationModel> invalidOps = new ArrayList<>();

		if ((patch == null) || patch.isEmpty()) {
			return invalidOps;
		}

		for (OperationModel operation : patch) {
			// test operation is ok
			if (OperationModelUtil.isTest(operation)) {
				continue;
			}
			// copy and move are not used for now
			if (OperationModelUtil.isCopy(operation) || OperationModelUtil.isMove(operation)) {
				invalidOps.add(operation);
				continue;
			}

			// if path not valid
			if (!operation.path.equals("/firstname")
					&& !operation.path.equals("/lastname")
					&& !operation.path.equals("/email")
					&& !operation.path.equals("/street")
					&& !operation.path.equals("/streetNumber")
					&& !operation.path.equals("/cap")
					&& !operation.path.equals("/city")
					&& !operation.path.equals("/country")
					&& !operation.path.equals("/phoneNumber")
					&& !operation.path.equals("/dateBirth")
					&& !operation.path.equals("/gender")
					&& !operation.path.equals("/nationality")
					&& !operation.path.equals("/civilStatus")
					&& !operation.path.equals("/numberChild")
					&& !operation.path.equals("/professional")
					&& !operation.path.equals("/education")
					&& !operation.path.equals("/fingerprint1")
					&& !operation.path.equals("/fingerprint2")
					&& !operation.path.equals("/fingerprint3")
					&& !operation.path.equals("/fingerprint4")
					&& !operation.path.equals("/fingerprint5")
					&& !operation.path.equals("/fingerprint6")
					&& !operation.path.equals("/fingerprint7")
					&& !operation.path.equals("/fingerprint8")
					&& !operation.path.equals("/fingerprint9")
					&& !operation.path.equals("/fingerprint10")
					&& !operation.path.equals("/handyman")
					&& !operation.path.equals("/leader")
					&& !operation.path.equals("/organized")
					&& !operation.path.equals("/performer")
					&& !operation.path.equals("/communicator")
					&& !operation.path.equals("/getstuck")
					&& !operation.path.equals("/doubts")
					&& !operation.path.equals("/opposition")
					&& !operation.path.equals("/passivity")
					&& !operation.path.equals("/taciturn")
					&& !operation.path.equals("/supporter")
					&& !operation.path.equals("/motivator")
					&& !operation.path.equals("/mentor")
					&& !operation.path.equals("/innovator")
					&& !operation.path.equals("/catalyst")
					&& !operation.path.equals("/retreat")
					&& !operation.path.equals("/tobear")
					&& !operation.path.equals("/guiltfeelings")
					&& !operation.path.equals("/hide")
					&& !operation.path.equals("/submit")
					&& !operation.path.equals("/harmonizer")
					&& !operation.path.equals("/enterprising")
					&& !operation.path.equals("/mediator")
					&& !operation.path.equals("/enthusiast")
					&& !operation.path.equals("/defender")) {
				invalidOps.add(operation);
			}
		}

		return invalidOps;

	}

	public static void validateConsultationData(ConsultationModel consultation, boolean newConsultation) throws ServiceException {
		if ((consultation.firstname == null) || consultation.firstname.isEmpty()) {
			log.warn("Consultation name cannot be null");
			throw new ServiceException("Consultation firstname cannot be null", consultation.firstname, "new consultation data should be provide a consultation firstname",
					HttpStatus.BAD_REQUEST);
		}

		if ((consultation.lastname == null) || consultation.lastname.isEmpty()) {
			log.warn("Consultation name cannot be null");
			throw new ServiceException("Consultation lastname cannot be null", consultation.lastname, "new consultation data should be provide a consultation lastname",
					HttpStatus.BAD_REQUEST);
		}

	}
}
