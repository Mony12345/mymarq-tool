package ch.mmt.mmtoolbackend.service.mapper;

import org.springframework.security.crypto.password.PasswordEncoder;

import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.api.model.UserModel;
import ch.mmt.mmtoolbackend.domain.model.User;
import ch.mmt.mmtoolbackend.service.validator.UserValidator;

public class UserMapper {

	public static UserModel toUserModel(User user) {
		UserModel model = new UserModel();
		model.uuid = user.getUuid();
		model.email = user.getEmail();
		model.firstname = user.getFirstname();
		model.lastname = user.getLastname();
		model.language = user.getLanguage();
		model.role = user.getRole();
		model.enabled = !user.getDisabled();
		model.createdAt = user.getCreatedAt();
		model.lastUpdateAt = user.getLastUpdateAt();

		return model;
	}

	public static User toUser(UserModel model, PasswordEncoder passwordEncoder) throws ServiceException {
		User user = new User();
		user.setEmail(model.email);
		user.setPassword(passwordEncoder.encode(model.password));
		user.setFirstname(model.firstname);
		user.setLastname(model.lastname);
		user.setLanguage(model.language);
		user.setRole(model.role);
		user.setDisabled(Boolean.FALSE.equals(model.enabled));

		return user;
	}

	public static User replaceUserData(User user, UserModel newUserModel, PasswordEncoder passwordEncoder) throws ServiceException {
		UserValidator.validateUserData(newUserModel, false);

		user.setEmail(newUserModel.email);
		user.setFirstname(newUserModel.firstname);
		user.setLastname(newUserModel.lastname);
		user.setLanguage(newUserModel.language);
		user.setRole(newUserModel.role);
		user.setDisabled(Boolean.FALSE.equals(newUserModel.enabled));

		if (newUserModel.password != null) {
			user.setPassword(passwordEncoder.encode(newUserModel.password));
		}

		return user;
	}
}
