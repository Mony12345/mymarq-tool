package ch.mmt.mmtoolbackend.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.api.model.ConsultationModel;
import ch.mmt.mmtoolbackend.api.model.FileModel;
import ch.mmt.mmtoolbackend.api.model.patch.OperationModel;
import ch.mmt.mmtoolbackend.api.model.patch.ReplaceModel;
import ch.mmt.mmtoolbackend.api.search.model.ConsultationSearchModel;
import ch.mmt.mmtoolbackend.api.search.model.RangeRequestModel;
import ch.mmt.mmtoolbackend.datatypes.CreatePDF;
import ch.mmt.mmtoolbackend.datatypes.CreateXLSX;
import ch.mmt.mmtoolbackend.datatypes.Role;
import ch.mmt.mmtoolbackend.domain.model.Consultation;
import ch.mmt.mmtoolbackend.domain.model.User;
import ch.mmt.mmtoolbackend.domain.repository.ConsultationRepository;
import ch.mmt.mmtoolbackend.domain.repository.UserRepository;
import ch.mmt.mmtoolbackend.service.mapper.ConsultationMapper;
import ch.mmt.mmtoolbackend.service.mapper.FileMapper;
import ch.mmt.mmtoolbackend.service.validator.ConsultationValidator;
import ch.mmt.mmtoolbackend.utils.FilterUtil;
import ch.mmt.mmtoolbackend.utils.PaginationUtil;
import ch.mmt.mmtoolbackend.utils.PatchUtil;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class ConsultationServiceImpl {

	@Autowired
	private ConsultationRepository consultationRepository;

	@Autowired
	private UserRepository userRepository;

	@Value("${mmtool.python.model.url}")
	private String pythonServer;

	private static final String PROFILE_FORMULAR_FILE_NAME_PATTERN = "profile.%s.%s.%s.pdf";

	private static final String PROFILES_FORMULAR_FILE_NAME_PATTERN = "profiles.%s.xlsx";

	public ConsultationModel createConsultationForUser(User user, ConsultationModel newConsultationModel) throws ServiceException {
		ConsultationValidator.validateConsultationData(newConsultationModel, true);
		Consultation consultation = ConsultationMapper.toConsultation(newConsultationModel);
		consultation.setUser(user);
		consultation = consultationRepository.save(consultation);
		return ConsultationMapper.toConsultationModel(consultation);
	}

	public ConsultationModel getConsultationByUUID(UUID consultationUUID) throws ServiceException {
		Consultation consultation = consultationRepository.findByUuid(consultationUUID);
		if (consultation == null) {
			throw new ServiceException("Consultation not found", consultationUUID, "Please provide a valid and existing consultation uuid", HttpStatus.NOT_FOUND);
		}
		return ConsultationMapper.toConsultationModel(consultation);
	}

	public void deleteConsultationByUUID(UUID consultationUUID) throws ServiceException {
		Consultation consultation = consultationRepository.findByUuid(consultationUUID);

		if (consultation == null) {
			throw new ServiceException("Consultation not found", consultationUUID, "Please provide a valid and existing consultation uuid", HttpStatus.NOT_FOUND);
		}

		consultationRepository.delete(consultation);
	}

	public Pair<List<ConsultationModel>, Long> searchConsultations(User user, ConsultationSearchModel searchModel, RangeRequestModel range) throws Exception {
		Pageable pageable = null;
		Long count = 0l;
		List<Consultation> consultations = null;

		if (range != null) {
			pageable = PaginationUtil.toPageable(range);
		}

		FilterUtil.computeFilter(searchModel);

		if (user.getRole().equals(Role.ADMIN)) {
			count = consultationRepository.countFiltered(searchModel.firstname, searchModel.lastname, null);
			consultations = consultationRepository.findFilteredOrderByLastUpdateAtDesc(searchModel.firstname, searchModel.lastname,
					null, pageable);

		} else {
			count = consultationRepository.countFiltered(searchModel.firstname, searchModel.lastname, user.getUuid());
			consultations = consultationRepository.findFilteredOrderByLastUpdateAtDesc(searchModel.firstname, searchModel.lastname,
					user.getUuid(), pageable);
		}

		return Pair.of(consultations.stream()
				.map(ConsultationMapper::toConsultationModel)
				.peek(c -> {
					if (!user.getUuid().equals(c.userUUID)) {
						c.anonimize(c.userFirstName, c.userLastName);
					}
				})
				.collect(Collectors.toList()), count);
	}

	public ConsultationModel applyPatchToConsultation(UUID consultationUUID, List<OperationModel> patch) throws ServiceException, IOException, Exception {
		ConsultationModel oldConsultationModel = null;
		ConsultationModel newConsultationModel = null;

		// Validation
		List<OperationModel> invalidPatchOps = ConsultationValidator.validateUpdateConsultationPatchOperations(patch);
		if (!invalidPatchOps.isEmpty()) {
			log.warn("Invalid patch operations received (or patches' paths not supported)");
			throw new ServiceException("Invalid patch operations received (or patches' paths not supported)", patch, OperationModel.INFORMATION, HttpStatus.BAD_REQUEST);
		}

		Consultation consultation = consultationRepository.findByUuid(consultationUUID);
		if (consultation == null) {
			throw new ServiceException("Consultation not found", consultationUUID, "Please provide a valid and existing consultation uuid", HttpStatus.NOT_FOUND);
		}

		// process "replace" with empty value as "remove"
		patch.stream().forEach(o -> {
			if (o instanceof ReplaceModel) {
				ReplaceModel replaceModel = (ReplaceModel) o;
				if (replaceModel.value.equals("")) {
					replaceModel.op = OperationModel.Kind.remove;
				}
			}
		});

		// patchModel returns old object if there are no changes.
		oldConsultationModel = ConsultationMapper.toConsultationModel(consultation);
		newConsultationModel = PatchUtil.patchModel(oldConsultationModel, ConsultationModel.class, patch);
		if (newConsultationModel != oldConsultationModel) {

			ConsultationMapper.replaceConsultationData(consultation, newConsultationModel);
			consultation = updateProfile(consultation);
			consultation = consultationRepository.save(consultation);

			// send images to BE model for improve models
			patch.stream().forEach(o -> {
				if (o instanceof ReplaceModel) {
					ReplaceModel replaceModel = (ReplaceModel) o;
					if (replaceModel.path.toString().toLowerCase().contains("fingerprint")) {
						try {
							sendImageForImprovements(consultationUUID, replaceModel.path, replaceModel.value.toString());
						} catch (Exception e) {
							log.error("Problem with patch");
						}
					}
				}
			});

			return ConsultationMapper.toConsultationModel(consultation);
		} else {
			log.warn("No update needed!");
		}

		return newConsultationModel;
	}

	public ConsultationModel uploadImage(UUID consultationUUID, MultipartFile[] file, String img, String fingerNo) throws ServiceException {
		Consultation consultation = consultationRepository.findByUuid(consultationUUID);
		if (consultation == null) {
			throw new ServiceException("Consultation not found", consultationUUID, "Please provide a valid and existing consultation uuid", HttpStatus.NOT_FOUND);
		}

		try {

			Path currentRelativePath = Paths.get("");
			String dir = currentRelativePath.toAbsolutePath().toString() + File.separator + "fingerprints" + File.separator + consultationUUID;
			new File(dir).mkdirs();

			byte[] buffer = null;
			if (img == null || img.isEmpty()) {
				InputStream initialStream = file[0].getInputStream();
				buffer = new byte[initialStream.available()];
				initialStream.read(buffer);
			} else {
				buffer = Base64.decodeBase64(img);
			}

			String imagePath = dir + File.separator + fingerNo + ".png";
			File targetFile = new File(imagePath);
			OutputStream outStream = new FileOutputStream(targetFile);
			outStream.write(buffer);
			outStream.close();

			String fpType = callFingerPrintModel(imagePath);

			switch (fingerNo) {
			case "1":
				consultation.setFingerprint1(fpType);
				break;
			case "2":
				consultation.setFingerprint2(fpType);
				break;
			case "3":
				consultation.setFingerprint3(fpType);
				break;
			case "4":
				consultation.setFingerprint4(fpType);
				break;
			case "5":
				consultation.setFingerprint5(fpType);
				break;
			case "6":
				consultation.setFingerprint6(fpType);
				break;
			case "7":
				consultation.setFingerprint7(fpType);
				break;
			case "8":
				consultation.setFingerprint8(fpType);
				break;
			case "9":
				consultation.setFingerprint9(fpType);
				break;
			case "10":
				consultation.setFingerprint10(fpType);
				break;
			default:
				break;
			}

			consultation = updateProfile(consultation);

			consultationRepository.save(consultation);
			return ConsultationMapper.toConsultationModel(consultation);

		} catch (IOException e) {
			log.error("Problem during the upload of image");
			return null;
		}
	}

	public ConsultationModel uploadImage2(UUID consultationUUID, MultipartFile[] file, String fingerNo) throws ServiceException {
		Consultation consultation = consultationRepository.findByUuid(consultationUUID);
		if (consultation == null) {
			throw new ServiceException("Consultation not found", consultationUUID, "Please provide a valid and existing consultation uuid", HttpStatus.NOT_FOUND);
		}

		try {
			InputStream initialStream = file[0].getInputStream();

			Path currentRelativePath = Paths.get("");
			String dir = currentRelativePath.toAbsolutePath().toString() + File.separator + "fingerprints" + File.separator + consultationUUID;
			new File(dir).mkdirs();

			byte[] buffer = new byte[initialStream.available()];
			initialStream.read(buffer);
			String imagePath = dir + File.separator + fingerNo + ".png";
			File targetFile = new File(imagePath);
			OutputStream outStream = new FileOutputStream(targetFile);
			outStream.write(buffer);
			outStream.close();

			String fpType = callFingerPrintModel(imagePath);

			switch (fingerNo) {
			case "1":
				consultation.setFingerprint1(fpType);
				break;
			case "2":
				consultation.setFingerprint2(fpType);
				break;
			case "3":
				consultation.setFingerprint3(fpType);
				break;
			case "4":
				consultation.setFingerprint4(fpType);
				break;
			case "5":
				consultation.setFingerprint5(fpType);
				break;
			case "6":
				consultation.setFingerprint6(fpType);
				break;
			case "7":
				consultation.setFingerprint7(fpType);
				break;
			case "8":
				consultation.setFingerprint8(fpType);
				break;
			case "9":
				consultation.setFingerprint9(fpType);
				break;
			case "10":
				consultation.setFingerprint10(fpType);
				break;
			default:
				break;
			}

			consultation = updateProfile(consultation);

			consultationRepository.save(consultation);
			return ConsultationMapper.toConsultationModel(consultation);

		} catch (IOException e) {
			log.error("Problem with uploadImage");
			return null;
		}
	}

	//TODO call the model 
	public String callFingerPrintModel(String imagePath) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "multipart/form-data");
		headers.add("MimeType", "multipart/form-data");

		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		File file = new File(imagePath);
		Resource resource = new FileSystemResource(file);
		body.add("image", resource);

		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
		String serverUrl = pythonServer + "/image";
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.postForEntity(serverUrl, requestEntity, String.class);
		System.out.println("Response: " + response.getBody());
		log.info("Response:" + response.getBody());
		JSONObject resp = new JSONObject(response.getBody().toString());
		String pattern = (String) resp.get("classification");
		log.info("Classificazione: " + pattern);
		return pattern;
	}

	public Consultation updateProfile(Consultation c) {
		//definition of character
		double harm = 0;
		double intr = 0;
		double medi = 0;
		double entu = 0;
		double dife = 0;
		int ltNumber = 0;
		int laNumber = 0;
		int tNumber = 0;
		int aNumber = 0;

		List<String> list = Arrays.asList(
				c.getFingerprint1(),
				c.getFingerprint2(),
				c.getFingerprint3(),
				c.getFingerprint4(),
				c.getFingerprint5(),
				c.getFingerprint6(),
				c.getFingerprint7(),
				c.getFingerprint8(),
				c.getFingerprint9(),
				c.getFingerprint10());

		for (String word : list) {
			switch ((word != null) ? word : " ") {
			case "L":
				harm += 1;
				break;
			case "W":
				intr += 1;
				break;
			case "C":
				medi += 1;
				break;
			case "T":
				entu += 1;
				tNumber += 1;
				break;
			case "A":
				dife += 1;
				aNumber += 1;
				break;
			case "P":
				intr += 1;
				break;
			case "LT":
				harm += 0.5;
				entu += 0.5;
				ltNumber += 1;
				break;
			case "LA":
				harm += 0.5;
				dife += 0.5;
				laNumber += 1;
				break;
			default:
				break;
			}
		}

		c.setHarmonizer(harm >= 6);
		c.setEnterprising(intr >= 2);
		c.setMediator(medi >= 1);
		c.setEnthusiast(entu >= 0.5);
		c.setDefender(dife >= 0.5);

		//definition of potential

		if (intr > 0) {
			c = setPotential(c, "P", "W", intr);
		} else if (medi > 0) {
			c = setPotential(c, "C", "-", medi);
		} else if (harm > 0) {
			c = setPotential(c, "L", "-", harm);
		} else if (ltNumber > 0) {
			c = setPotential(c, "LT", "-", ltNumber);
		} else if ((tNumber + laNumber) > 0) {
			c = setPotential(c, "T", "LA", tNumber + laNumber);
		} else {
			c = setPotential(c, "A", "-", aNumber);
		}

		//definition of challange
		if (aNumber > 0) {
			c = setChallange(c, "A", "-", aNumber);
		} else if ((tNumber + laNumber) > 0) {
			c = setChallange(c, "T", "LA", tNumber + laNumber);
		} else if (ltNumber > 0) {
			c = setChallange(c, "LT", "-", ltNumber);
		} else if (harm > 0) {
			c = setChallange(c, "L", "-", harm);
		} else if (medi > 0) {
			c = setChallange(c, "C", "-", medi);
		} else {
			c = setChallange(c, "P", "W", intr);
		}

		return c;
	}

	public Consultation setPotential(Consultation c, String s1, String s2, double noSimbol) {
		if (noSimbol == 10) {
			c.setHandyman(false);
			c.setLeader(false);
			c.setOrganized(false);
			c.setPerformer(false);
			c.setCommunicator(false);
			c.setSupporter(false);
			c.setMotivator(false);
			c.setMentor(false);
			c.setInnovator(false);
			c.setCatalyst(false);

		} else {
			c.setHandyman((s1.equals(c.getFingerprint1()) || s2.equals(c.getFingerprint1())));
			c.setLeader((s1.equals(c.getFingerprint2()) || s2.equals(c.getFingerprint2())));
			c.setOrganized((s1.equals(c.getFingerprint3()) || s2.equals(c.getFingerprint3())));
			c.setPerformer((s1.equals(c.getFingerprint4()) || s2.equals(c.getFingerprint4())));
			c.setCommunicator((s1.equals(c.getFingerprint5()) || s2.equals(c.getFingerprint5())));
			c.setSupporter((s1.equals(c.getFingerprint6()) || s2.equals(c.getFingerprint6())));
			c.setMotivator((s1.equals(c.getFingerprint7()) || s2.equals(c.getFingerprint7())));
			c.setMentor((s1.equals(c.getFingerprint8()) || s2.equals(c.getFingerprint8())));
			c.setInnovator((s1.equals(c.getFingerprint9()) || s2.equals(c.getFingerprint9())));
			c.setCatalyst((s1.equals(c.getFingerprint10()) || s2.equals(c.getFingerprint10())));
		}
		return c;
	}

	public Consultation setChallange(Consultation c, String s1, String s2, double noSimbol) {
		//TODO riguardare tutto secondo le regole speciali 

		if (noSimbol == 10) {

			c.setGetstuck(false);
			c.setDoubts(false);
			c.setOpposition(false);
			c.setPassivity(false);
			c.setTaciturn(false);
			c.setRetreat(false);
			c.setTobear(false);
			c.setGuiltfeelings(false);
			c.setHide(false);
			c.setSubmit(false);

		} else {
			c.setGetstuck((s1.equals(c.getFingerprint1()) || s2.equals(c.getFingerprint1())));
			c.setDoubts((s1.equals(c.getFingerprint2()) || s2.equals(c.getFingerprint2())));
			c.setOpposition((s1.equals(c.getFingerprint3()) || s2.equals(c.getFingerprint3())) && (!(s1.equals(c.getFingerprint8()) || s2.equals(c.getFingerprint8()))));
			c.setPassivity((s1.equals(c.getFingerprint4()) || s2.equals(c.getFingerprint4())));
			c.setTaciturn((s1.equals(c.getFingerprint5()) || s2.equals(c.getFingerprint5())) && (!(s1.equals(c.getFingerprint10()) || s2.equals(c.getFingerprint10()))));

			c.setRetreat((s1.equals(c.getFingerprint6()) || s2.equals(c.getFingerprint6())) && (!(s1.equals(c.getFingerprint1()) || s2.equals(c.getFingerprint1()))));
			c.setTobear((s1.equals(c.getFingerprint7()) || s2.equals(c.getFingerprint7())) && (!(s1.equals(c.getFingerprint2()) || s2.equals(c.getFingerprint2()))));
			c.setGuiltfeelings((s1.equals(c.getFingerprint8()) || s2.equals(c.getFingerprint8())));
			c.setHide((s1.equals(c.getFingerprint9()) || s2.equals(c.getFingerprint9())) && (!(s1.equals(c.getFingerprint4()) || s2.equals(c.getFingerprint4()))));
			c.setSubmit((s1.equals(c.getFingerprint10()) || s2.equals(c.getFingerprint10())));
		}

		return c;
	}

	public FileModel getImage(UUID consultationUUID, String fingerNo) throws ServiceException {

		try {

			Path currentRelativePath = Paths.get("");
			String dir = currentRelativePath.toAbsolutePath().toString() + File.separator + "fingerprints";
			String imagePath = dir + File.separator + consultationUUID + File.separator + fingerNo + ".png";

			File f = new File(imagePath);
			if (!f.exists()) {
				imagePath = dir + File.separator + "finger.png";
			}

			File image = new File(imagePath);
			URLConnection connection = image.toURL().openConnection();

			return FileMapper.toFileModel(connection.getContentType(), image.getName(), Files.readAllBytes(image.toPath()));

		} catch (Exception e) {
			log.error("Problem during getImage");
			return null;
		}
	}

	public FileModel exportConsultation(UUID consultationUUID) throws ServiceException {

		try {
			Consultation consultation = consultationRepository.findByUuid(consultationUUID);
			if (consultation == null) {
				throw new ServiceException("Consultation not found", consultationUUID, "Please provide a valid and existing consultation uuid", HttpStatus.NOT_FOUND);
			}

			try {
				CreatePDF pdf = new CreatePDF();
				FileModel file = new FileModel();
				file.fileContentType = MediaType.APPLICATION_PDF.toString();
				file.fileContent = pdf.newConsultationPDF(consultation);
				file.fileName = String.format(PROFILE_FORMULAR_FILE_NAME_PATTERN, consultation.getFirstname().replace(" ", "_"),
						consultation.getLastname().replace(" ", "_"),
						OffsetDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss")));

				return file;
			} catch (Exception e) {
				throw new ServiceException("Error generating pdf report", e);
			}

		} catch (Exception e) {
			log.error("Problem with the generation of PDF");
			return null;
		}
	}

	public FileModel exportConsultations() throws ServiceException {

		try {

			List<Consultation> consultations = consultationRepository.findOrderByCreatedAt();

			try {
				CreateXLSX xlsx = new CreateXLSX();

				FileModel file = new FileModel();
				file.fileContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
				file.fileContent = xlsx.newConsultationsXLSX(consultations);
				file.fileName = String.format(PROFILES_FORMULAR_FILE_NAME_PATTERN, OffsetDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss")));

				return file;
			} catch (Exception e) {
				throw new ServiceException("Error generating xlsx report", e);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in xlsx");
			return null;
		}
	}

	public void sendImageForImprovements(UUID consultationUUID, String path, String value) {

		Path currentRelativePath = Paths.get("");
		String dir = currentRelativePath.toAbsolutePath().toString() + File.separator + "fingerprints" + File.separator + consultationUUID;

		String imagePath = dir + File.separator + path.replace("/fingerprint", "") + ".png";

		File file = new File(imagePath);
		if (file.exists()) {
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Type", "multipart/form-data");
			headers.add("MimeType", "multipart/form-data");

			MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

			Resource resource = new FileSystemResource(file);
			body.add("image", resource);

			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
			String serverUrl = pythonServer + "/improve/" + value;
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.postForEntity(serverUrl, requestEntity, String.class);
			System.out.println("Response: " + response.getBody());

		}
	}
}
