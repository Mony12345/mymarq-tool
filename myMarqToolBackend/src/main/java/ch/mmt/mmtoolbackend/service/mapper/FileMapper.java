package ch.mmt.mmtoolbackend.service.mapper;

import ch.mmt.mmtoolbackend.api.model.FileModel;

public class FileMapper {

	public static FileModel toFileModel(String contentType, String fileName, byte[] data) {
		FileModel model = new FileModel();
		model.fileContent = data;
		model.fileContentType = contentType;
		model.fileName = fileName;

		return model;
	}
}
