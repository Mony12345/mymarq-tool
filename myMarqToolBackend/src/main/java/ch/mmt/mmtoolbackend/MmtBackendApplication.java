package ch.mmt.mmtoolbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = { "ch.mmt" })
@EnableJpaRepositories(basePackages = { "ch.mmt.mmtoolbackend.domain.repository" })
@EntityScan(basePackages = { "ch.mmt" })
@EnableAspectJAutoProxy
public class MmtBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MmtBackendApplication.class, args);
	}
}
