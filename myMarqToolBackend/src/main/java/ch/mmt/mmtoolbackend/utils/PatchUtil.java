package ch.mmt.mmtoolbackend.utils;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.flipkart.zjsonpatch.JsonDiff;
import com.flipkart.zjsonpatch.JsonPatch;

import ch.mmt.mmtoolbackend.api.model.patch.OperationModel;

public class PatchUtil {

	/**
	 * Applies the set of patch operations to the given model and returns the patched model.
	 *
	 * @param originalModel
	 * @param clazz
	 * @param patch
	 * @return patched model
	 * @throws JsonProcessingException
	 * @throws IOException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 */
	public static <T> T patchModel(
			T originalModel,
			Class<? extends T> clazz,
			List<OperationModel> patch)
			throws IOException {
		final ObjectMapper mapper = new ObjectMapper();
		final String sourceJson = mapper.writeValueAsString(originalModel);
		final JsonNode sourceNode = mapper.readTree(sourceJson);
		final JsonNode patchNode = mapper.readTree(mapper.writeValueAsString(patch));
		final JsonNode targetNode = JsonPatch.apply(patchNode, sourceNode);
		final ArrayNode diff = (ArrayNode) JsonDiff.asJson(sourceNode, targetNode);
		if (diff.size() != 0) {
			final T patchedModel = mapper.readValue(mapper.writeValueAsString(mapper.treeToValue(targetNode, Object.class)), clazz);
			return patchedModel;
		}
		return originalModel;
	}
}
