package ch.mmt.mmtoolbackend.utils;

import ch.mmt.mmtoolbackend.api.model.patch.OperationModel;
import ch.mmt.mmtoolbackend.api.model.patch.OperationModel.Kind;

public class OperationModelUtil {

	public static boolean isAdd(
			OperationModel operation) {
		return (operation != null) && OperationModel.Kind.add.equals(operation.op);
	}

	public static boolean isRemove(
			OperationModel operation) {
		return (operation != null) && OperationModel.Kind.remove.equals(operation.op);
	}

	public static boolean isReplace(
			OperationModel operation) {
		return (operation != null) && OperationModel.Kind.replace.equals(operation.op);
	}

	public static boolean isMove(
			OperationModel operation) {
		return (operation != null) && OperationModel.Kind.move.equals(operation.op);
	}

	public static boolean isCopy(
			OperationModel operation) {
		return (operation != null) && OperationModel.Kind.copy.equals(operation.op);
	}

	public static boolean isTest(
			OperationModel operation) {
		return (operation != null) && OperationModel.Kind.test.equals(operation.op);
	}
}
