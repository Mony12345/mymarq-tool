package ch.mmt.mmtoolbackend.utils;

import org.springframework.data.domain.Pageable;

import ch.mmt.mmtoolbackend.api.OffsetBasedPageRequest;
import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.api.search.model.RangeRequestModel;

public class PaginationUtil {

	/**
	 * Converts a range request (e.g.: 0-19) to a page query.
	 * 
	 * @param rangeRequestModel
	 * @param totalEntries
	 * @return
	 */
	public static Pageable toPageable(RangeRequestModel rangeRequestModel) {

		Pageable pageable = null;
		if (rangeRequestModel != null) {
			int startIndex = Integer.MIN_VALUE;
			int endIndex = Integer.MAX_VALUE;

			if (rangeRequestModel.from() != null) {
				startIndex = rangeRequestModel.from().intValue();
			}
			if (rangeRequestModel.to() != null) {
				endIndex = rangeRequestModel.to().intValue();
			}

			int size = endIndex - startIndex + 1;

			pageable = new OffsetBasedPageRequest(startIndex, size);
		} // else, return all, we cannot find out which page it is...
		return pageable;
	}

	public static RangeRequestModel responseRangeRequestFor(OffsetBasedPageRequest p, int totalSize)
			throws ServiceException {
		int m = (int) Math.min(p.getOffset() + p.getPageSize() - 1, totalSize);
		if (m < p.getOffset()) {
			m = (int) p.getOffset();
		}
		return RangeRequestModel
				.forRange((int) p.getOffset(), m);
	}
}
