package ch.mmt.mmtoolbackend.utils;

import java.lang.reflect.Field;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FilterUtil {

	private FilterUtil() {
	}

	public static void computeFilter(Object model) {
		if (model != null) {
			Field[] fields = model.getClass().getFields();
			for (Field f : fields) {
				try {
					if (f.getType().equals(String.class)) {
						if (!f.getName().equals("DESCRIPTION") && !f.getName().equals("DISCRIMINATOR")) {
							f.set(model, checkIfPercentagePresent((String) f.get(model)));
						}
					}
				} catch (Exception e) {
					log.warn(
							"Could not extend model field: {} of model class: {}, with percentages to search in LIKE mode...",
							f.getName(), model.getClass().getName());
				}
			}
		}
	}

	public static String checkIfPercentagePresent(String searchAttribute) {
		String percentageSearchAttribute = null;
		if (!StringUtils.isEmpty(searchAttribute)) {
			if (!searchAttribute.startsWith("%")) {
				searchAttribute = "%" + searchAttribute;
			}
			if (!searchAttribute.endsWith("%")) {
				searchAttribute += "%";
			}
			percentageSearchAttribute = searchAttribute;
		}
		return percentageSearchAttribute;
	}

	public static Pair<Instant, Instant> computeFiltersForDateAtStartAndEndOfDay(
			LocalDate startLocalDate,
			LocalDate endLocalDate) {
		Instant start = startLocalDate != null
				? OffsetDateTime.of(startLocalDate, LocalTime.MIN,
						ZoneOffset.UTC).toInstant()
				: null;

		Instant end = endLocalDate != null
				? OffsetDateTime.of(endLocalDate, LocalTime.MAX,
						ZoneOffset.UTC).toInstant()
				: null;

		return Pair.of(start, end);
	}
}
