package ch.mmt.mmtoolbackend.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Aspect
@Slf4j
public class LogRestControllerMethods {

	@Before("execution(* ch.mmt.mmtoolbackend.controller.*.*RestController.*(..))")
	public void logExecutionInput(JoinPoint joinPoint) {
		log.trace("REST method <{}> called with input parameters <{}>...", joinPoint.getSignature(), joinPoint.getArgs());
	}
}
