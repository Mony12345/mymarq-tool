package ch.mmt.mmtoolbackend.aop;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import ch.mmt.mmtoolbackend.api.model.BaseModel;
import ch.mmt.mmtoolbackend.domain.model.BaseEntity;
import lombok.extern.slf4j.Slf4j;

@Component
@Aspect
@Slf4j
public class LogServicesMethods {

	@Before("execution(public * ch.mmt.mmtoolbackend.service.impl.*.*(..))")
	public void logExecutionInput(JoinPoint joinPoint) {
		log.debug("Executing <{}> with input parameters <{}>...", joinPoint.getSignature(), joinPoint.getArgs());
	}

	@AfterReturning(pointcut = "execution(public * ch.mmt.mmtoolbackend.service.impl.*.*(..))", returning = "result")
	public void logExecutionOutput(JoinPoint joinPoint, Object result) {
		if (result instanceof List) {
			List<?> list = (List<?>) result;
			log.debug("Found <{}> results..", list.size());
		}
		if (result instanceof Pair) {
			Pair<?, ?> pair = (Pair<?, ?>) result;

			if (pair.getLeft() instanceof List && pair.getRight() instanceof Long) {
				List<?> list = (List<?>) pair.getLeft();
				log.info("Found a total of {} results!", pair.getRight());
				log.info("Fetched a subset of {} results ", list.size());
			}
		}
		if (result instanceof BaseEntity) {
			BaseEntity entity = (BaseEntity) result;
			log.debug("Found entity with uuid <{}>", entity.getUuid());
		}
		if (result instanceof BaseModel) {
			BaseModel model = (BaseModel) result;
			log.debug("Found entity with uuid <{}>", model.uuid);
		}
	}
}
