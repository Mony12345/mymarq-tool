package ch.mmt.mmtoolbackend.authentication;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ch.mmt.mmtoolbackend.domain.model.User;
import ch.mmt.mmtoolbackend.service.impl.UserServiceImpl;

@Service(value = "authenticationUserService")
public class AuthenticationUserServiceImpl implements UserDetailsService {

	@Autowired
	private UserServiceImpl userController;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = null;
		user = userController.getEnableUserByEmail(username);
		if (user == null) {
			throw new UsernameNotFoundException("User <" + username + "> not found!");
		}

		return new AuthenticatedUser(username, user.getPassword(), getAuthorityForUser(user), user.getLanguage(), user.getUuid());
	}

	private List<SimpleGrantedAuthority> getAuthorityForUser(User user) {
		return Arrays.asList(new SimpleGrantedAuthority(user.getRole().name()));
	}
}
