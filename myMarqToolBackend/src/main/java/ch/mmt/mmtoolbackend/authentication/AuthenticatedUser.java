package ch.mmt.mmtoolbackend.authentication;

import java.util.Collection;
import java.util.UUID;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import ch.mmt.mmtoolbackend.datatypes.Language;

public class AuthenticatedUser extends User {

	private static final long serialVersionUID = 1284011858046176550L;

	protected Language language;
	protected UUID userUUID;

	public AuthenticatedUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
	}

	public AuthenticatedUser(String username, String password, Collection<? extends GrantedAuthority> authorities, Language language, UUID userUUID) {
		super(username, password, authorities);
		this.language = language;
		this.userUUID = userUUID;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public UUID getUserUUID() {
		return userUUID;
	}

	public void setUserUUID(UUID userUUID) {
		this.userUUID = userUUID;
	}
}
