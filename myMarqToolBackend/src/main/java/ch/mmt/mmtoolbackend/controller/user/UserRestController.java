package ch.mmt.mmtoolbackend.controller.user;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.api.exception.WebApplicationErrorResponses;
import ch.mmt.mmtoolbackend.api.exception.model.ClientErrorModel;
import ch.mmt.mmtoolbackend.api.exception.model.ServerErrorModel;
import ch.mmt.mmtoolbackend.api.model.UserModel;
import ch.mmt.mmtoolbackend.api.model.patch.OperationModel;
import ch.mmt.mmtoolbackend.controller.BaseRestController;
import ch.mmt.mmtoolbackend.service.impl.UserServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = { BaseRestController.BASE_REST_PATH + "/user" })
public class UserRestController extends BaseRestController {

	@Autowired
	private UserServiceImpl userService;

	@GetMapping(path = "/{userUUID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "User detail retrieval", httpMethod = "GET", notes = "Get all user details by his uuid")
	@ApiResponses({
			@ApiResponse(code = HttpServletResponse.SC_OK, message = "Success", response = UserModel.class),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Not Found", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> getUser(
			@ApiParam(hidden = true) @RequestHeader("Authorization") String encodedToken,
			@ApiParam(value = "The user's uuid", required = true) @NotNull @PathVariable UUID userUUID) {
		try {
			if (!getUser(encodedToken).getUuid().equals(userUUID)) {
				return WebApplicationErrorResponses.newForbiddenException("Cannot update another user");
			}
			UserModel resp = userService.getUserByUuid(userUUID);
			return ResponseEntity.ok(resp);
		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}

	@PatchMapping(path = "/{userUUID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "User Update", httpMethod = "PATCH", notes = "Updates the user information.<br />"
			+ "<p>Valid / supported PATCH operations based on patch path: </p>"
			+ "<ul>"
			+ "<li> \"/password/...\": add,replace,test</li>"
			+ "<li> \"/firstname\": add,replace,remove,test</li>"
			+ "<li> \"/lastname/...\": add,replace,remove,test</li>"
			+ "<li> \"/language\": add,replace,test</li>"
			+ "<li> \"/enabled\": add,replace,test</li>"
			+ "</ul>")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, message = "Success", response = UserModel.class),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Not Found", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Bad Request", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> updateUser(
			@ApiParam(hidden = true) @RequestHeader("Authorization") String encodedToken,
			@ApiParam(value = "The user uuid", required = true) @NotNull @PathVariable UUID userUUID,
			@NotNull @RequestBody List<OperationModel> patch) {
		try {
			if (!getUser(encodedToken).getUuid().equals(userUUID)) {
				return WebApplicationErrorResponses.newForbiddenException("Cannot update another user");
			}

			UserModel resp = userService.applyPatchToUser(userUUID, patch, false);
			return ResponseEntity.ok(resp);
		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}
}