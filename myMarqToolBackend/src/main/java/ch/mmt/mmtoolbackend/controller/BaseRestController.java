package ch.mmt.mmtoolbackend.controller;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.List;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ch.mmt.mmtoolbackend.api.FilterAPIs;
import ch.mmt.mmtoolbackend.api.OffsetBasedPageRequest;
import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.api.exception.WebApplicationErrorResponses;
import ch.mmt.mmtoolbackend.api.search.model.RangeRequestModel;
import ch.mmt.mmtoolbackend.domain.model.User;
import ch.mmt.mmtoolbackend.domain.repository.UserRepository;
import ch.mmt.mmtoolbackend.utils.PaginationUtil;

public class BaseRestController {

	@Autowired
	protected UserRepository userRepository;

	public static final String BASE_REST_PATH = "/api/v1/tool";

	public static final String BASE_ADMIN_REST_PATH = "/api/v1/admin";

	public <T> ResponseEntity<?> generateRangeResponse(Pair<List<T>, Long> results, RangeRequestModel range) throws ServiceException {
		int totalSize = results.getRight().intValue();
		if (range != null) {
			OffsetBasedPageRequest p = (OffsetBasedPageRequest) PaginationUtil.toPageable(range);
			if (totalSize > 0) {
				if (results.getLeft().size() < totalSize) {
					return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT)
							.header(FilterAPIs.CONTENT_RANGE_HEADER_NAME, PaginationUtil
									.responseRangeRequestFor(p, totalSize).toContentRangeHeaderString(totalSize))
							.body(results.getLeft());
				} else {
					return ResponseEntity.ok()
							.header(FilterAPIs.CONTENT_RANGE_HEADER_NAME, PaginationUtil
									.responseRangeRequestFor(p, totalSize).toContentRangeHeaderString(totalSize))
							.body(results.getLeft());
				}
			} else {
				return ResponseEntity.noContent().header(FilterAPIs.CONTENT_RANGE_HEADER_NAME,
						RangeRequestModel.noContentRangeHeaderString()).build();
			}
		} else {
			if (results.getLeft().isEmpty()) {
				return ResponseEntity.noContent().build();
			} else {
				return ResponseEntity.ok(results.getLeft());
			}
		}
	}

	public ResponseEntity<?> handleServiceException(ServiceException e) {
		if (e.getHttpStatus() != null && e.getHttpStatus().equals(HttpStatus.NOT_FOUND)) {
			return WebApplicationErrorResponses.newNotFoundException(e.getMessage(), e.getInvalidValue(), e.getInformation());
		}
		if (e.getHttpStatus() != null && e.getHttpStatus().equals(HttpStatus.BAD_REQUEST)) {
			return WebApplicationErrorResponses.newBadRequestException(e.getMessage(), e.getInvalidValue(), e.getInformation());
		}
		if (e.getHttpStatus() != null && e.getHttpStatus().equals(HttpStatus.CONFLICT)) {
			return WebApplicationErrorResponses.newConflictException(e.getMessage(), e.getInvalidValue(), e.getInformation());
		}
		if (e.getHttpStatus() != null && e.getHttpStatus().equals(HttpStatus.FORBIDDEN)) {
			return WebApplicationErrorResponses.newForbiddenException(e.getMessage());
		}
		if (e.getHttpStatus() != null && e.getHttpStatus().equals(HttpStatus.UNAUTHORIZED)) {
			return WebApplicationErrorResponses.newUnauthorizedException();
		}
		if (e.getHttpStatus() != null && e.getHttpStatus().equals(HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE)) {
			return WebApplicationErrorResponses.newRangeNotSatisfiableException(e.getMessage(), e.getInvalidValue(), e.getInformation());
		}

		return WebApplicationErrorResponses.newInternalServerErrorException(e);
	}

	public String getUsernameInToken(String encodedToken) throws UnsupportedEncodingException {
		String jsonString = decodeToken(encodedToken);
		JSONObject token = new JSONObject(jsonString);

		return token.getString("user_name");
	}

	public User getUser(String encodedToken) throws UnsupportedEncodingException {
		String jsonString = decodeToken(encodedToken);
		JSONObject token = new JSONObject(jsonString);

		UUID userUUID = UUID.fromString(token.getString("userUUID"));

		return userRepository.findByUuid(userUUID);
	}

	public String decodeToken(String encodedToken) throws UnsupportedEncodingException {
		String[] pieces = encodedToken.split("\\.");
		String b64payload = pieces[1];
		return new String(Base64.decodeBase64(b64payload), "UTF-8");
	}

	public URI getNewResourceLocation(UUID uuid) {
		return ServletUriComponentsBuilder.fromCurrentRequestUri().path(uuid.toString()).build().toUri();
	}
}
