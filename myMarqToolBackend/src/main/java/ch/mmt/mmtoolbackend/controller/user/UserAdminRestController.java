package ch.mmt.mmtoolbackend.controller.user;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.mmt.mmtoolbackend.api.FilterAPIs;
import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.api.exception.WebApplicationErrorResponses;
import ch.mmt.mmtoolbackend.api.exception.model.ClientErrorModel;
import ch.mmt.mmtoolbackend.api.exception.model.ServerErrorModel;
import ch.mmt.mmtoolbackend.api.model.UserModel;
import ch.mmt.mmtoolbackend.api.model.patch.OperationModel;
import ch.mmt.mmtoolbackend.api.search.model.RangeRequestModel;
import ch.mmt.mmtoolbackend.api.search.model.UserSearchModel;
import ch.mmt.mmtoolbackend.controller.BaseRestController;
import ch.mmt.mmtoolbackend.service.impl.UserServiceImpl;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@RestController
@RequestMapping(value = { BaseRestController.BASE_ADMIN_REST_PATH + "/user" })
public class UserAdminRestController extends BaseRestController {

	@Autowired
	private UserServiceImpl userService;

	@PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Add a new user", httpMethod = "POST", notes = "Add a new user")
	@ApiResponses(value = { @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "Created", responseHeaders = {
			@ResponseHeader(response = String.class, description = "The newly created user resource locator, e.g "
					+ "http://localhost:8080/api/v1/admin/user/c0ab35ed-1581-4017-9268-8b7d094e7a93", name = "Location") }),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Bad Request", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> createUser(@ApiParam(value = UserModel.INFORMATION, required = true) @Valid @RequestBody UserModel userModel) {
		try {
			UserModel resp = userService.createUser(userModel);

			return ResponseEntity.created(getNewResourceLocation(resp.uuid)).body(resp);
		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}

	@GetMapping(path = "/{userUUID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "User detail retrieval", httpMethod = "GET", notes = "Get all user details by his uuid")
	@ApiResponses({
			@ApiResponse(code = HttpServletResponse.SC_OK, message = "Success", response = UserModel.class),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Not Found", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> getUser(
			@ApiParam(value = "The user's uuid", required = true) @NotNull @PathVariable UUID userUUID) {
		try {
			UserModel resp = userService.getUserByUuid(userUUID);
			return ResponseEntity.ok(resp);
		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}

	@DeleteMapping(path = "/{userUUID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Delete user", httpMethod = "DELETE", notes = "Delete the user with the provided uuid")
	@ApiResponses({
			@ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "Success"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Not Found", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> deleteUser(@ApiParam(hidden = true) @RequestHeader("Authorization") String encodedToken,
			@ApiParam(value = "The user's uuid", required = true) @NotNull @PathVariable UUID userUUID) {
		try {
			userService.deleteUserByUUID(userUUID, getUser(encodedToken));

			return ResponseEntity.noContent().build();
		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}

	@PatchMapping(path = "/{userUUID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "User Update", httpMethod = "PATCH", notes = "Updates the user information.<br />"
			+ "<p>Valid / supported PATCH operations based on patch path: </p>"
			+ "<ul>"
			+ "<li> \"/firstname\": add,replace,remove,test</li>"
			+ "<li> \"/lastname/...\": add,replace,remove,test</li>"
			+ "<li> \"/language\": add,replace,test</li>"
			+ "<li> \"/role\": add,replace,test</li>"
			+ "<li> \"/enabled\": add,replace,test</li>"
			+ "</ul>")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, message = "Success", response = UserModel.class),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Not Found", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Bad Request", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> updateUser(
			@ApiParam(hidden = true) @RequestHeader("Authorization") String encodedToken,
			@ApiParam(value = "The user uuid", required = true) @NotNull @PathVariable UUID userUUID,
			@NotNull @RequestBody List<OperationModel> patch) {
		try {
			UserModel resp = userService.applyPatchToUser(userUUID, patch, true);
			return ResponseEntity.ok(resp);
		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}

	@PostMapping(path = "/search")
	@ResponseBody
	@ApiOperation(value = "Search users", httpMethod = "POST", notes = "Retrieve the users")
	@ApiResponses({
			@ApiResponse(code = HttpServletResponse.SC_OK, message = "Success", responseHeaders = {
					@ResponseHeader(response = String.class, description = FilterAPIs.ACCEPT_RANGES_HEADER_DESCRIPTION, name = FilterAPIs.ACCEPT_RANGES_HEADER_NAME) },
					response = UserModel.class, responseContainer = "List"),
			@ApiResponse(code = HttpServletResponse.SC_PARTIAL_CONTENT, message = FilterAPIs.PARTIAL_CONTENT_MESSAGE_ON_HEADER, responseHeaders = {
					@ResponseHeader(response = String.class, description = FilterAPIs.CONTENT_RANGE_DESCRIPTION, name = FilterAPIs.CONTENT_RANGE_HEADER_NAME) },
					response = UserModel.class, responseContainer = "List"),
			@ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = FilterAPIs.NO_CONTENT_MESSAGE_ON_HEADER, responseHeaders = {
					@ResponseHeader(response = String.class, description = FilterAPIs.NO_CONTENT_RANGE_DESCRIPTION, name = FilterAPIs.CONTENT_RANGE_HEADER_NAME)
			}),
			@ApiResponse(code = HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE, message = FilterAPIs.REQUESTED_RANGE_NOT_SATISFIABLE_MESSAGE, responseHeaders = {
					@ResponseHeader(response = String.class, description = FilterAPIs.UNSATISFIABLE_RANGE_DESCRIPTION, name = FilterAPIs.CONTENT_RANGE_HEADER_NAME)
			}, response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	@ApiImplicitParams({
			@ApiImplicitParam(name = FilterAPIs.RANGE_HEADER_NAME, value = RangeRequestModel.DESCRIPTION, paramType = "header", example = "members=0-10")
	})
	public ResponseEntity<?> searchUsers(
			@RequestHeader(name = FilterAPIs.RANGE_HEADER_NAME, required = false) RangeRequestModel range,
			@ApiParam(value = UserSearchModel.DESCRIPTION, required = true) @NotNull @RequestBody UserSearchModel userSearchModel) {
		try {
			Pair<List<UserModel>, Long> results = userService.searchUsers(userSearchModel, range);

			return generateRangeResponse(results, range);
		} catch (ServiceException e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}
}