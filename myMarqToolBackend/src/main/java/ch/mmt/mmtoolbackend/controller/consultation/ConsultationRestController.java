package ch.mmt.mmtoolbackend.controller.consultation;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ch.mmt.mmtoolbackend.api.FilterAPIs;
import ch.mmt.mmtoolbackend.api.exception.ServiceException;
import ch.mmt.mmtoolbackend.api.exception.WebApplicationErrorResponses;
import ch.mmt.mmtoolbackend.api.exception.model.ClientErrorModel;
import ch.mmt.mmtoolbackend.api.exception.model.ServerErrorModel;
import ch.mmt.mmtoolbackend.api.model.ConsultationModel;
import ch.mmt.mmtoolbackend.api.model.FileModel;
import ch.mmt.mmtoolbackend.api.model.patch.OperationModel;
import ch.mmt.mmtoolbackend.api.search.model.ConsultationSearchModel;
import ch.mmt.mmtoolbackend.api.search.model.RangeRequestModel;
import ch.mmt.mmtoolbackend.controller.BaseRestController;
import ch.mmt.mmtoolbackend.service.impl.ConsultationServiceImpl;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@RestController
@RequestMapping(value = { BaseRestController.BASE_REST_PATH + "/consultation" })
public class ConsultationRestController extends BaseRestController {

	@Autowired
	private ConsultationServiceImpl consultationService;

	@PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Add a new Consultation", httpMethod = "POST", notes = "Add a new Consultation")
	@ApiResponses(value = { @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "Created", responseHeaders = {
			@ResponseHeader(response = String.class, description = "The newly created consultation resource locator, e.g "
					+ "http://localhost:8080/api/v1/consultation/c0ab35ed-1581-4017-9268-8b7d094e7a93", name = "Location") }),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Bad Request", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> createConsultation(@ApiParam(hidden = true) @RequestHeader("Authorization") String encodedToken,
			@ApiParam(value = ConsultationModel.INFORMATION, required = true) @RequestBody ConsultationModel consultationModel) {
		try {
			ConsultationModel resp = consultationService.createConsultationForUser(getUser(encodedToken), consultationModel);

			return ResponseEntity.created(getNewResourceLocation(resp.uuid)).body(resp);
		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}

	@GetMapping(path = "/{consultationUUID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Consultation detail retrieval", httpMethod = "GET", notes = "Get all consultation details by his uuid")
	@ApiResponses({
			@ApiResponse(code = HttpServletResponse.SC_OK, message = "Success", response = ConsultationModel.class),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Not Found", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> getConsultation(
			@ApiParam(value = "The consultation's uuid", required = true) @NotNull @PathVariable UUID consultationUUID) {
		try {
			ConsultationModel resp = consultationService.getConsultationByUUID(consultationUUID);
			return ResponseEntity.ok(resp);
		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}

	@DeleteMapping(path = "/{consultationUUID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Delete consultation", httpMethod = "DELETE", notes = "Delete the consultation with the provided uuid")
	@ApiResponses({
			@ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "Success"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Not Found", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> deleteConsultation(
			@ApiParam(value = "The consultation's uuid", required = true) @NotNull @PathVariable UUID consultationUUID) {
		try {
			consultationService.deleteConsultationByUUID(consultationUUID);

			return ResponseEntity.noContent().build();
		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}

	@PatchMapping(path = "/{consultationUUID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Consultation Update", httpMethod = "PATCH", notes = "Updates the consultation information.<br />"
			+ "<p>Valid / supported PATCH operations based on patch path: </p>"
			+ "<ul>"
			+ "<li> \"/firstname\": add,replace,test</li>"
			+ "<li> \"/lastname\": add,replace,test</li>"
			+ "<li> \"/email\": add,replace,remove,test</li>"
			+ "<li> \"/street\": add,replace,remove,test</li>"
			+ "<li> \"/streetNumber/...\": add,replace,remove,test</li>"
			+ "<li> \"/cap\": add,replace,remove,test</li>"
			+ "<li> \"/city\": add,replace,remove,test</li>"
			+ "<li> \"/country\": add,replace,remove,test</li>"
			+ "<li> \"/phoneNumber\": add,replace,remove,test</li>"
			+ "<li> \"/consultationType\": add,replace,test</li>"
			+ "<li> \"/enabled\": add,replace,test</li>"
			+ "<li> \"/dateBirth\": add,replace,test</li>"
			+ "<li> \"/gender\": add,replace,test</li>"
			+ "<li> \"/nationality\": add,replace,test</li>"
			+ "<li> \"/civilStatus\": add,replace,test</li>"
			+ "<li> \"/numberChild\": add,replace,test</li>"
			+ "<li> \"/professional\": add,replace,test</li>"
			+ "<li> \"/education\": add,replace,test</li>"
			+ "<li> \"/fingerprint1\": add,replace,test</li>"
			+ "<li> \"/fingerprint2\": add,replace,test</li>"
			+ "<li> \"/fingerprint3\": add,replace,test</li>"
			+ "<li> \"/fingerprint4\": add,replace,test</li>"
			+ "<li> \"/fingerprint5\": add,replace,test</li>"
			+ "<li> \"/fingerprint6\": add,replace,test</li>"
			+ "<li> \"/fingerprint7\": add,replace,test</li>"
			+ "<li> \"/fingerprint8\": add,replace,test</li>"
			+ "<li> \"/fingerprint9\": add,replace,test</li>"
			+ "<li> \"/fingerprint10\": add,replace,test</li>"
			+ "</ul>")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, message = "Success", response = ConsultationModel.class),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Not Found", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Bad Request", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> updateConsultation(
			@ApiParam(hidden = true) @RequestHeader("Authorization") String encodedToken,
			@ApiParam(value = "The consultation's uuid", required = true) @NotNull @PathVariable UUID consultationUUID,
			@NotNull @RequestBody List<OperationModel> patch) {
		try {
			ConsultationModel resp = consultationService.applyPatchToConsultation(consultationUUID, patch);
			return ResponseEntity.ok(resp);
		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}

	@PostMapping(path = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Authenticated user's revision retrieval", httpMethod = "POST", notes = "Search revision for the authenticated user")
	@ApiResponses({
			@ApiResponse(code = HttpServletResponse.SC_OK, message = "Success", responseHeaders = {
					@ResponseHeader(response = String.class, description = FilterAPIs.ACCEPT_RANGES_HEADER_DESCRIPTION, name = FilterAPIs.ACCEPT_RANGES_HEADER_NAME) },
					response = ConsultationModel.class, responseContainer = "List"),
			@ApiResponse(code = HttpServletResponse.SC_PARTIAL_CONTENT, message = FilterAPIs.PARTIAL_CONTENT_MESSAGE_ON_HEADER, responseHeaders = {
					@ResponseHeader(response = String.class, description = FilterAPIs.CONTENT_RANGE_DESCRIPTION, name = FilterAPIs.CONTENT_RANGE_HEADER_NAME) },
					response = ConsultationModel.class, responseContainer = "List"),
			@ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = FilterAPIs.NO_CONTENT_MESSAGE_ON_HEADER, responseHeaders = {
					@ResponseHeader(response = String.class, description = FilterAPIs.NO_CONTENT_RANGE_DESCRIPTION, name = FilterAPIs.CONTENT_RANGE_HEADER_NAME)
			}),
			@ApiResponse(code = HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE, message = FilterAPIs.REQUESTED_RANGE_NOT_SATISFIABLE_MESSAGE, responseHeaders = {
					@ResponseHeader(response = String.class, description = FilterAPIs.UNSATISFIABLE_RANGE_DESCRIPTION, name = FilterAPIs.CONTENT_RANGE_HEADER_NAME)
			}, response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	@ApiImplicitParams({
			@ApiImplicitParam(name = FilterAPIs.RANGE_HEADER_NAME, value = RangeRequestModel.DESCRIPTION, paramType = "header", example = "members=0-10")
	})
	public ResponseEntity<?> searchConsultations(@ApiParam(hidden = true) @RequestHeader("Authorization") String encodedToken,
			@ApiParam(value = ConsultationSearchModel.DESCRIPTION, required = true) @NotNull @RequestBody ConsultationSearchModel consultationSearchModel,
			@RequestHeader(name = FilterAPIs.RANGE_HEADER_NAME, required = false) RangeRequestModel range) {
		try {
			Pair<List<ConsultationModel>, Long> results = consultationService.searchConsultations(getUser(encodedToken), consultationSearchModel, range);

			return generateRangeResponse(results, range);
		} catch (ServiceException e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}

	@PostMapping(path = "/{consultationUUID}/{fingerNo}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Upload Image consultation", httpMethod = "POST", consumes = "multipart/form-data",
			notes = "Upload the consultation fingerprint with the provided uuid and finger number")
	@ApiResponses({
			@ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "Success"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Not Found", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> uploadImageConsultation(
			@ApiParam(value = "The consultation's uuid", required = true) @NotNull @PathVariable UUID consultationUUID,
			@ApiParam(value = "The consultation's file", required = false) @ModelAttribute MultipartFile[] file,
			@ApiParam(value = "The consultation's fingerprint image", required = false) @RequestBody(required = false) String img,
			@ApiParam(value = "The consultation's finger number", required = true) @NotNull @PathVariable String fingerNo) {
		try {

			ConsultationModel resp = consultationService.uploadImage(consultationUUID, file, img, fingerNo);
			return ResponseEntity.ok(resp);

		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}

	@GetMapping(path = "/{consultationUUID}/{fingerNo}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Consultation detail retrieval", httpMethod = "GET", notes = "Get image  by his uuid and number of finger")
	@ApiResponses({
			@ApiResponse(code = HttpServletResponse.SC_OK, message = "Success", response = ConsultationModel.class),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Not Found", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> getImage(
			@ApiParam(value = "The consultation's uuid", required = true) @NotNull @PathVariable UUID consultationUUID,
			@ApiParam(value = "The consultation's finger number", required = true) @NotNull @PathVariable String fingerNo) {
		try {
			FileModel fileModel = consultationService.getImage(consultationUUID, fingerNo);
			if (fileModel == null) {
				return ResponseEntity.noContent().build();
			}

			HttpHeaders respHeaders = new HttpHeaders();
			respHeaders.set("Access-Control-Expose-Headers", "Content-Disposition");
			respHeaders.setContentLength(fileModel.fileContent.length);
			respHeaders.setContentType(MediaType.valueOf(fileModel.fileContentType));
			respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			respHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileModel.fileName);

			return new ResponseEntity<>(fileModel.fileContent, respHeaders, HttpStatus.OK);
		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}

	@GetMapping(path = "/{consultationUUID}/export", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Export Consultation report pdf", httpMethod = "GET", notes = "Get report by his uuid")
	@ApiResponses({
			@ApiResponse(code = HttpServletResponse.SC_OK, message = "Success", response = ConsultationModel.class),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Not Found", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> exportConsultation(
			@ApiParam(value = "The consultation's uuid", required = true) @NotNull @PathVariable UUID consultationUUID) {
		try {
			FileModel fileModel = consultationService.exportConsultation(consultationUUID);
			if (fileModel == null) {
				return ResponseEntity.noContent().build();
			}

			HttpHeaders respHeaders = new HttpHeaders();
			respHeaders.set("Access-Control-Expose-Headers", "Content-Disposition");
			respHeaders.setContentLength(fileModel.fileContent.length);
			respHeaders.setContentType(MediaType.APPLICATION_PDF);
			respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			respHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileModel.fileName);

			return new ResponseEntity<>(fileModel.fileContent, respHeaders, HttpStatus.OK);
		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}

	@GetMapping(path = "/exportList", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Consultations report detail", httpMethod = "GET", notes = "Get report of all consltations")
	@ApiResponses({
			@ApiResponse(code = HttpServletResponse.SC_OK, message = "Success", response = ConsultationModel.class),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Not Found", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "Permission denied", response = ClientErrorModel.class),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error", response = ServerErrorModel.class)
	})
	public ResponseEntity<?> exportConsultations() {
		try {
			FileModel fileModel = consultationService.exportConsultations();
			if (fileModel == null) {
				return ResponseEntity.noContent().build();
			}

			HttpHeaders respHeaders = new HttpHeaders();
			respHeaders.set("Access-Control-Expose-Headers", "Content-Disposition");
			respHeaders.setContentLength(fileModel.fileContent.length);
			respHeaders.setContentType(MediaType.valueOf(fileModel.fileContentType));
			respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			respHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileModel.fileName);

			return new ResponseEntity<>(fileModel.fileContent, respHeaders, HttpStatus.OK);
		} catch (ServiceException se) {
			return handleServiceException(se);
		} catch (Exception e) {
			return WebApplicationErrorResponses.newInternalServerErrorException(e);
		}
	}
}
