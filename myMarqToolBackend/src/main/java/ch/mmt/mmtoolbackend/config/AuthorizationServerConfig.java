package ch.mmt.mmtoolbackend.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import ch.mmt.mmtoolbackend.authentication.AuthenticatedUser;
import ch.mmt.mmtoolbackend.authentication.AuthenticationUserServiceImpl;
import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableAuthorizationServer
@Slf4j
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	private static final String REFRESH_TOKEN = "refresh_token";

	private static final String GRANT_TYPE_PASSWORD = "password";

	@Value("${mmtool.security.oauth2.client.clientId}")
	private String clientId;

	@Value("${mmtool.security.oauth2.resource.id}")
	private String resourceId;

	@Value("${mmtool.security.oauth2.jwt.signingkey}")
	private String jwtSigningKey;

	@Value("${mmtool.security.oauth2.jwt.validity}")
	private int jwtValidity;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private PasswordEncoder userPasswordEncoder;

	@Autowired
	private AuthenticationUserServiceImpl authenticationUserService;

	@Bean
	@Primary
	public DefaultTokenServices tokenServices() {
		DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
		defaultTokenServices.setTokenStore(tokenStore());
		defaultTokenServices.setSupportRefreshToken(true);
		defaultTokenServices.setTokenEnhancer(accessTokenConverter());
		defaultTokenServices.setAccessTokenValiditySeconds(jwtValidity);

		return defaultTokenServices;
	}

	@Bean
	public WebResponseExceptionTranslator<OAuth2Exception> loggingExceptionTranslator() {
		return new DefaultWebResponseExceptionTranslator() {
			@Override
			public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
				log.error("Authentication Exception: ", e);

				// Carry on handling the exception
				ResponseEntity<OAuth2Exception> responseEntity = super.translate(e);
				HttpHeaders headers = new HttpHeaders();
				headers.setAll(responseEntity.getHeaders().toSingleValueMap());
				OAuth2Exception excBody = responseEntity.getBody();
				return new ResponseEntity<>(excBody, headers, responseEntity.getStatusCode());
			}
		};
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints
				.authenticationManager(this.authenticationManager)
				.tokenServices(tokenServices())
				.tokenStore(tokenStore())
				.accessTokenConverter(accessTokenConverter()).exceptionTranslator(loggingExceptionTranslator());
		;
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer configurer) throws Exception {
		configurer
				.inMemory()
				.withClient(clientId)
				.secret("")
				.authorizedGrantTypes(GRANT_TYPE_PASSWORD, REFRESH_TOKEN)
				.scopes("admin")
				.accessTokenValiditySeconds(jwtValidity);
	}

	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}

	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter() {
			@Override
			public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
				if (authentication.getOAuth2Request().getGrantType() != null && authentication.getOAuth2Request().getGrantType().equalsIgnoreCase(GRANT_TYPE_PASSWORD)) {
					addAdditionalInfo((DefaultOAuth2AccessToken) accessToken, (AuthenticatedUser) authentication.getPrincipal());
				}
				if (authentication.getOAuth2Request().getRefreshTokenRequest() != null
						&& authentication.getOAuth2Request().getRefreshTokenRequest().getGrantType().equalsIgnoreCase(REFRESH_TOKEN)) {
					AuthenticatedUser user = (AuthenticatedUser) authenticationUserService.loadUserByUsername((String) authentication.getUserAuthentication().getPrincipal());
					addAdditionalInfo((DefaultOAuth2AccessToken) accessToken, user);
				}
				accessToken = super.enhance(accessToken, authentication);
				((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(new HashMap<String, Object>());

				return accessToken;
			}
		};
		converter.setSigningKey(jwtSigningKey);

		return converter;
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.allowFormAuthenticationForClients()
				.passwordEncoder(userPasswordEncoder);
	}

	private void addAdditionalInfo(DefaultOAuth2AccessToken accessToken, AuthenticatedUser user) {
		final Map<String, Object> additionalInfo = new HashMap<>();
		additionalInfo.put("userUUID", user.getUserUUID().toString());
		additionalInfo.put("language", user.getLanguage().toString());
		accessToken.setAdditionalInformation(additionalInfo);
	}
}
