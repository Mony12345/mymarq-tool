package ch.mmt.mmtoolbackend.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

import ch.mmt.mmtoolbackend.controller.BaseRestController;
import ch.mmt.mmtoolbackend.datatypes.Role;

@Configuration
@EnableResourceServer
public class ResourceConfig extends ResourceServerConfigurerAdapter {

	private static final String[] AUTH_WHITELIST = {
			// -- swagger ui
			"/v2/api-docs/**",
			"/api-docs/**",
			"/redoc/**",
			"/swagger-resources",
			"/configuration/security",
			"/swagger-ui.html",
			"/webjars/**",
			"/swagger-resources/configuration/ui",
			"/swagger-resources/configuration/security"
	};

	@Value("${mmtool.security.oauth2.resource.id}")
	private String resourceId;

	// The DefaultTokenServices bean provided at the AuthorizationConfig
	@Autowired
	private DefaultTokenServices tokenServices;

	// The TokenStore bean provided at the AuthorizationConfig
	@Autowired
	private TokenStore tokenStore;

	// To allow the rResourceServerConfigurerAdapter to understand the token,
	// it must share the same characteristics with AuthorizationServerConfigurerAdapter.
	// So, we must wire it up the beans in the ResourceServerSecurityConfigurer.
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.resourceId(resourceId).tokenServices(tokenServices).tokenStore(tokenStore);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
				.authorizeRequests().antMatchers(AUTH_WHITELIST).permitAll()
				.antMatchers(BaseRestController.BASE_REST_PATH + "/**")
				.access("hasAuthority('" + Role.ADMIN.name() + "') || hasAuthority('" + Role.CONSULTANT.name() + "') || hasAuthority('" + Role.COMPANY.name() + "')")
				.antMatchers(BaseRestController.BASE_ADMIN_REST_PATH + "/**")
				.access("hasAuthority('" + Role.ADMIN.name() + "')")
				.anyRequest().authenticated();
	}
}
