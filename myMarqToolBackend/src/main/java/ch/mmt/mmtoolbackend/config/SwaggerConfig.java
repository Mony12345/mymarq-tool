package ch.mmt.mmtoolbackend.config;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.ResourceOwnerPasswordCredentialsGrant;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Value("${build.version}")
	private String softwareVersion;

	@Value("${mmtool.security.oauth2.client.clientId}")
	private String clientId;

	@Value("${local.server.port}")
	private int port;

	@Bean
	public Docket api() throws UnknownHostException {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("ch.mmt.mmtoolbackend.controller"))
				.paths(PathSelectors.regex(".*/api/v1.*"))
				.build()
				.apiInfo(apiInfo())
				.useDefaultResponseMessages(false)
				.securitySchemes(Arrays.asList(securityScheme()))
				.securityContexts(Arrays.asList(securityContext()));
	}

	@Bean
	public springfox.documentation.swagger.web.SecurityConfiguration security() {
		return SecurityConfigurationBuilder.builder()
				.useBasicAuthenticationWithAccessCodeGrant(false)
				.clientId(clientId)
				.clientSecret("")
				.build();
	}

	private SecurityScheme securityScheme() throws UnknownHostException {
		GrantType grantType = new ResourceOwnerPasswordCredentialsGrant("http://" + InetAddress.getLoopbackAddress().getHostAddress() + ":" + port + "/oauth/token");

		SecurityScheme oauth = new OAuthBuilder().name("oauth2")
				.grantTypes(Arrays.asList(grantType))
				.scopes(Arrays.asList(scopes()))
				.build();

		return oauth;
	}

	private AuthorizationScope[] scopes() {
		AuthorizationScope[] scopes = { new AuthorizationScope("admin", "API admin access") };
		return scopes;
	}

	private SecurityContext securityContext() {
		return SecurityContext.builder()
				.securityReferences(
						Arrays.asList(new SecurityReference("oauth2", scopes())))
				.forPaths(PathSelectors.regex(".*/api/v1.*"))
				.build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("MM-Tool Back-End")
				.description("MM-Tool Back-End exposes all needed functionality")
				.version(softwareVersion).build();
	}
}
