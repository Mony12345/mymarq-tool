#!/bin/sh

# Load Secrets as Env Vars
source /env_secrets_expand.sh

# Start the SunPas Server
exec java -Djava.security.egd=file:/dev/./urandom -jar /app.jar