# fp-python-webservice

## commands

Create the environment with conda:

```shell script
conda create --name fp python=3.7
```

Install required packages using pip:

```shell script
pip install -r requirements.txt
```
