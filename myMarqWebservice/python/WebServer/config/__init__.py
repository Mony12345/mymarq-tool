import logging
import logging.config
import os
import keras

import tensorflow
from tensorflow.python.framework import ops
import yaml

from flask_apscheduler import APScheduler

dir_path = os.path.dirname(os.path.realpath(__file__))


def setup_logging():
    log_dir = os.path.join('logs')
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    f_yaml = os.path.join(dir_path, 'logging.yaml')
    if os.path.exists(f_yaml):
        with open(f_yaml, 'rt') as f:
            try:
                log_conf = yaml.safe_load(f.read())
                logging.config.dictConfig(log_conf)
            except Exception as e:
                logging.basicConfig(level=logging.INFO)
                logging.exception(e)
                logging.error('Error in loggin configuration, using default configs')


class SetupConfig(object):
    ENV = 'setup'
    MODELS_PATH = os.path.join('model', 'predict_pattern.model')
    JOBS = [{
        'id': 'job_train',
        'func': 'WebServer.jobs.training:train',
        'max_instances': 1,
        'args': None,
        'trigger': 'cron',
        'hour': '1',
        'minute': '0',
        'second': '0'
    }]


class DevelopmentConfig(SetupConfig):
    ENV = 'dev'


class ProductionConfig(SetupConfig):
    ENV = 'prod'


config = {
    'setup': SetupConfig(),
    'dev': DevelopmentConfig(),
    'prod': ProductionConfig(),
}


def configure_app(app):
    setup_logging()

    flask_env = os.getenv('FLASK_ENV', 'prod')
    cfg_class = config[flask_env]

    app.config.from_object(cfg_class)

    # setup paths
    app.path_model = cfg_class.MODELS_PATH

    # create directories
    if not os.path.exists(app.path_model):
        os.makedirs(app.path_model)

    logging.info(f'Configuration: {flask_env}')
    logging.info(f'model path: {app.path_model}')

    # load model
    app.model = keras.models.load_model(app.path_model, compile=False)
    app.model._make_predict_function()
    app.graph = ops.get_default_graph()

    scheduler = APScheduler()
    scheduler.init_app(app)

    if os.environ.get('scheduler_lock') != '1':
        os.environ['scheduler_lock'] = os.environ.get('scheduler_lock', '1')
        app.apscheduler.start()
        logging.info('Scheduler started')

    logging.info('Configuration: DONE')


if __name__ == '__main__':
    pass
