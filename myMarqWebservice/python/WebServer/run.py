import os

from werkzeug.serving import run_simple

from WebServer.wsgi import application

if __name__ == '__main__':
    run_simple(
        hostname=os.getenv('FLASK_HOST', '0.0.0.0'),
        port=int(os.getenv('FLASK_PORT', 5000)),
        application=application,
        use_reloader=True
    )
