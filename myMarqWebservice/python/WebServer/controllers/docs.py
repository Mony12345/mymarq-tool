import json
import logging
import logging.config
import os

from flask import Flask
from flask import send_from_directory, Response

from WebServer.config import configure_app


def init():
    app = Flask(
        __name__,
        root_path=os.path.abspath('WebServer'),
        static_url_path='',
        static_folder=os.path.abspath('WebServer/docs/html')
    )

    configure_app(app)

    @app.route('/', methods=['GET'])
    def index():
        return Response(json.dumps({
            'status': 'ok',
            'data': '🐳'
        }), mimetype='application/json'), 200

    @app.route('/html', methods=['GET'])
    def docs():
        logging.info('Documentation page')
        return send_from_directory(os.path.join(os.getcwd(), 'WebServer/docs/html'), 'index.html')

    return app
