import json
import logging
import os
import time
from uuid import uuid4

import PIL.ImageOps
import keras
import numpy as np
import skimage
from PIL import Image
from flask import Flask, Response, request
from keras import backend as K
from tensorflow.python.framework import ops

from WebServer.config import configure_app


def init():
    app = Flask(
        __name__,
        root_path=os.getcwd()
    )

    app.path_model = ''
    patterns = ['A', 'LA', 'C', 'L', 'P', 'T', 'LT', 'W']
    configure_app(app)

    @app.route('/', methods=['GET'])
    def index():
        return Response(json.dumps({'model': app.path_model}), mimetype='application/json'), 200

    @app.route('/reload', methods=['GET'])
    def reloadModel():

        K.clear_session()

        with app.graph.as_default():
            newModel = keras.models.load_model(app.path_model, compile=False)
            newModel._make_predict_function()

            app.model = newModel

        logging.info('Models is reloaded')
        return Response(json.dumps({'model': app.path_model}), mimetype='application/json'), 200

    @app.route('/image', methods=['POST'])
    def analyze():
        content_length = request.content_length
        content_type = request.content_type
        mimetype = request.mimetype

        logging.info(f'image Content-Length: {content_length}, Content-Type: {content_type}, MimeType: {mimetype}')

        try:
            image = request.files['image']

            logging.info(f'image Filename: {image.filename}, Name: {image.name}')
            imageid = time.strftime("%Y%m%d%H%M%S")
            imageName = imageid + '_' + image.filename
            image_path = os.path.join(app.root_path, 'images', 'temp', imageName)
            image.save(image_path)
            img = preprocessingImage(image_path)

            # perform analysis on image
            prediction = process_image(app.model, app.graph, img)
            logging.info(f'Prediction: {prediction}')

            os.remove(image_path)

        except Exception as e:
            os.remove(image_path)
            logging.exception(e)
            logging.error('Something failed with this image!')
            return Response(json.dumps({
                'status': 'error',
                'exception': str(e)
            })), 500

        return Response(json.dumps({
            'status': 'ok',
            'pos': str(prediction[0]),
            'classification': patterns[prediction[0]]
        })), 200

    @app.route('/improve/<value>', methods=['POST'])
    def improve(value: str):

        content_length = request.content_length
        content_type = request.content_type
        mimetype = request.mimetype

        logging.info(
            f'image Content-Length: {content_length}, Content-Type: {content_type}, MimeType: {mimetype}, value: {value}')

        try:
            image = request.files['image']

            logging.info(f'image Filename: {image.filename}, Name: {image.name}')
            imageid = str(uuid4())[:8]
            imagetime = time.strftime("%Y%m%d%H%M%S")

            imageName = f'{patterns.index(value) + 1}_{imagetime}_{imageid}.png'
            image_path = os.path.join(app.root_path, 'images', 'train', imageName)
            image.save(image_path)
            img = preprocessingImage(image_path)
            img.save(image_path)
        except Exception as e:
            logging.exception(e)
            logging.error('Something failed with this image!')
            return Response(json.dumps({
                'status': 'error',
                'exception': str(e)
            })), 500

        return Response(json.dumps({
            'status': 'ok'
        })), 200

    return app


def preprocessingImage(image_path):
    img = Image.open(image_path)
    width, height = img.size

    color = img.getpixel((0, 0))
    print(color)
    small_img = img.resize((int(width / 5), int(height / 5)), Image.ANTIALIAS)
    color = small_img.getpixel((0, 0))

    try:
        if color < 128:
            img = PIL.ImageOps.invert(img)
    except Exception as e:
        if color[0] < 128:
            img = PIL.ImageOps.invert(img)

    new_h = height
    new_w = width
    if height / width >= 1.5:
        new_w = int(height * 2 / 3)
        new_h = height
    else:
        new_w = width
        new_h = int(width * 1.5)

    new_img = Image.new('RGBA', (new_w, new_h), 'white')
    offset = ((new_w - width) // 2, (new_h - height) // 2)
    new_img.paste(img, offset)
    new_img = new_img.resize((160, 240), Image.ANTIALIAS)

    return new_img


def process_image(model, graph, image):
    image = np.array(image)
    image = skimage.img_as_ubyte(image)
    image = image[:, :, [2, 1, 0]]

    X = []
    X.append(image)
    X = np.array(X).astype('float32')

    print('image size:', X.shape)
    with graph.as_default():
        pattern = model.predict_classes(X)

    return pattern
