#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
# Take image and resize to a specified size, after applying data augmentation
from keras.utils import to_categorical
from skimage import img_as_ubyte
from skimage.transform import SimilarityTransform, warp


def transform_complex(im, sz):
    if np.random.rand() < 0.5:
        im = np.fliplr(im)
    tl = np.array(im.shape[0:2]) / 2
    tf2 = SimilarityTransform(translation=-tl)
    tf3 = SimilarityTransform(rotation=np.deg2rad(np.random.uniform(-45, 45)))
    tf5 = SimilarityTransform(translation=tl)
    imr = warp(im, (tf2 + (tf3 + tf5)).inverse, output_shape=(sz[0], sz[1]), mode="edge")
    imr = imr * np.random.uniform(0.95, 1.05, size=(1, 1, 3))
    imr = np.clip(imr, 0, 1)
    return img_as_ubyte(imr)


def sample(df, sz):
    finger = df.sample(n=1)
    im = finger["image"].iloc[0]
    im = transform_complex(im, sz)
    l = finger["pattern"].iloc[0]
    return im, l


def mkbatch(df, N, sz):
    X = []
    y = []

    df_list = [df[df['pattern'] == i] for i in range(1, 9)]

    for df_p in df_list:
        try:
            for i in range(int(N / 8)):
                im, l = sample(df_p, sz)
                X.append(im)
                y.append(l-1)
        except Exception as e:
            pass

    if len(X) < N:
        for i in range(0,  N - len(X)):
            im, l = sample(df, sz)
            X.append(im)
            y.append(l-1)

    X = np.array(X).astype('float32')
    y = to_categorical(np.array(y), 8)
    return X, y


def mygenerator(df, batch_size, sz):
    while True:
        X, y = mkbatch(df, batch_size, sz)
        yield X, y


