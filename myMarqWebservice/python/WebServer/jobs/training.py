import datetime
import os
from os import listdir
from os.path import isfile

import keras
import numpy as np
import pandas as pd
import requests
import skimage
import tqdm
from keras import backend as K
from skimage import img_as_ubyte
from skimage.transform import resize
from sklearn.metrics import accuracy_score
from tensorflow.python.framework import ops

from .functions import mygenerator

sz = (240, 160)


def load_finger():
    images = [f for f in listdir(os.path.join('images', 'train')) if isfile(os.path.join('images', 'train', f))]
    dataset = []
    for image in tqdm.tqdm(images):
        # skip files that start with .
        if image.startswith("."):
            continue

        fileName = image.split("_")
        label_pattern = int(fileName[0])

        try:
            im = skimage.io.imread(os.path.join('images', 'train', image))
        except (OSError, ValueError) as e:
            print("ignoring due to exception: ", str(e))
            continue

        # just convert the image to ubytes
        im = skimage.transform.resize(im, sz)
        im = skimage.img_as_ubyte(im)
        im = im[:, :, [2, 1, 0]]
        dataset.append(dict(file=image, pattern=label_pattern, image=im))
    data = pd.DataFrame(dataset)
    return data


def train():
    try:
        print('training start in folder', os.getcwd())
        train_set = load_finger()
        if len(train_set.index) >= 20:
            df_val = pd.read_pickle(os.path.join("images", "validation", "data_val.pkl"))
            val_X = []
            for _, img in df_val['image'].iteritems():
                img = resize(img, sz)
                img = img_as_ubyte(img)
                img = img[:, :, [2, 1, 0]]
                val_X.append(img)

            val_X = np.array(val_X)
            val_y = np.array(df_val['pattern'] - 1)

            K.clear_session()

            graph = ops.get_default_graph()
            with graph.as_default():
                model = keras.models.load_model(os.path.join('model', 'predict_pattern.model'), compile=False)
                model._make_predict_function()
                predictions = model.predict_classes(val_X)

                acctual_accuracy = accuracy_score(val_y, predictions)
                print('acctual_accuracy: ', acctual_accuracy)

                batch_size = 50
                epoch = 1
                if len(train_set.index) > 50:
                    batch_size = 100
                    epoch = 2

                model.compile(loss="categorical_crossentropy",
                              optimizer="adam",
                              metrics=['accuracy'])

                history = model.fit_generator(mygenerator(train_set, batch_size, sz),
                                              steps_per_epoch=5,
                                              epochs=epoch,
                                              verbose=1)

                predictions = model.predict_classes(val_X)
                new_accuracy = accuracy_score(val_y, predictions)
                print('new_accuracy: ', new_accuracy)

                if (new_accuracy > acctual_accuracy):
                    now = datetime.datetime.today().strftime('%Y%m%d-%H%M%S')
                    os.rename(os.path.join('model', 'predict_pattern.model'),
                              os.path.join('model', f'predict_pattern_{now}.model'))

                    keras.models.save_model(model, os.path.join('model', 'predict_pattern.model'))

                    hostname = os.getenv('FLASK_HOST', 'localhost')
                    port = int(os.getenv('FLASK_PORT', 5000))
                    requests.get(f"http://{hostname}:{port}/reload")
        else:
            print('Limit of images not reached (20)')
    except Exception as e:
        print(e)

    print('job end')
