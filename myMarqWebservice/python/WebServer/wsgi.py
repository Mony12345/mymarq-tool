from werkzeug.middleware.dispatcher import DispatcherMiddleware

from WebServer.controllers.main import init as main_app
from WebServer.controllers.docs import init as docs_app

application = DispatcherMiddleware(main_app(), {
    '/docs': docs_app(),
})

if __name__ == '__main__':
    pass
