#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import skimage
import numpy as np

# Take image and resize to a specified size, after applying data augmentation
from keras.utils import to_categorical


def transform_complex(im, sz):
    if np.random.rand() < 0.5:
        im = np.fliplr(im)
    tl = np.array(im.shape[0:2]) / 2
    tf2 = skimage.transform.SimilarityTransform(translation=-tl)
    # wery sybtle rotations
    tf3 = skimage.transform.SimilarityTransform(rotation=np.deg2rad(np.random.uniform(-2, 2)))
    tf5 = skimage.transform.SimilarityTransform(translation=tl)
    imr = skimage.transform.warp(im, (tf2 + (tf3 + tf5)).inverse, output_shape=(sz[0], sz[1]), mode="edge")
    imr = imr * np.random.uniform(0.95, 1.05, size=(1, 1, 3))
    imr = np.clip(imr, 0, 1)
    return skimage.img_as_ubyte(imr)


def sample(df, sz):
    finger = df.sample(n=1)
    im = finger["image"].iloc[0]
    im = transform_complex(im, sz)
    l = finger["pattern"].iloc[0]
    return im, l


def transform_all(df, sz):
    X = []
    y = []
    for i in range(0, df.shape[0]):
        im = df.image.iloc[i]
        im = transform_complex(im, sz)
        l = df["pattern"].iloc[i]

        X.append(im)
        y.append(l)
    X = np.array(X).astype('float32')
    y = np.array(y)
    return X, y


def mkbatch(df, N, sz):
    X = []
    y = []

    df_list = [df[df['pattern'] == i] for i in range(1, 9)]

    for df in df_list:
        for i in range(int(N / 8)):
            im, l = sample(df, sz)
            X.append(im)
            y.append(l - 1)

    X = np.array(X).astype('float32')
    y = to_categorical(np.array(y))
    return X, y


def mygenerator(df, batch_size, sz):
    while True:
        X, y = mkbatch(df, batch_size, sz)
        yield (X, y)


def sampleOnePattern(df, sz, pattern):
    finger = df.sample(n=1)
    im = finger["image"].iloc[0]
    im = transform_complex(im, sz)
    l = finger["pattern"].iloc[0]
    return im, l



def mkbatchOnePattern(df, N, sz, pattern):
    X = []
    y = []

    df_list = [df[df['pattern'] == i] for i in range(1, 9)]

    for df in df_list:
        for i in range(int(N / 8)):
            im, l = sampleOnePattern(df, sz,pattern)
            X.append(im)
            if l == pattern:
                y.append(1)
            else:
                y.append(0)

    X = np.array(X).astype('float32')
    y = to_categorical(np.array(y))
    return X, y


def mygeneratorOnePattern(df, batch_size, sz, pattern):
    while True:
        X, y = mkbatchOnePattern(df, batch_size, sz, pattern)
        yield (X, y)
