import os
import random
from PIL import Image
import pandas as pd
import time


sz = (320, 480)  # images size width 320 height 480
path_img_small = 'data_test'


def getRandomImages():
    root = 'classified_fingerprint'
    symbol = [('Arch', '1'), ('Arch-Loop', '2'), ('Composite', '3'), ('Loop', '4'), ('Peacock', '5'),
              ('Tented Arch', '6'), ('Tented Arch-Loop', '7'), ('Whorl', '8')]

    image_list_filtered = []
    for s, t in symbol:
        print(s)
        print('===========================================================')
        path_images = os.path.join(root, s)
        #image_list = []
        for r, subdirs, filenames in os.walk(path_images):
            for file in filenames:
                path = os.path.join(r, file)

                if not (path.endswith('.png')):
                    continue
                #image_list.append(path)
                image_list_filtered.append([path, t])

        print(len(image_list_filtered))
        #for i in range(10):
        #   image_list_filtered.append([random.choices(image_list), t])
    return image_list_filtered


def load_fingerprint():
    dataset = []
    paths = getRandomImages()

    i = 0
    for p in paths:
        print(p[0])
        i += 1
        img = Image.open(p[0])
        width, height = img.size
        new_h = height
        new_w = width
        if height/width >= 1.5:
            new_w = int(height *2/3)
            new_h = height
        else:
            new_w = width
            new_h = int(width * 1.5)

        new_im = Image.new('RGBA', (new_w, new_h), 'white')
        offset = ((new_w - width) // 2, (new_h - height) // 2)
        new_im.paste(img, offset)
        new_im = new_im.resize(sz, Image.ANTIALIAS)

        dataset.append(dict(symbol=p[1], image=new_im))

        f_name = p[1] + '_' + str(i) + '.png'
        new_im.save(os.path.join(path_img_small, f_name))

    data = pd.DataFrame(dataset)
    return data



val = load_fingerprint()

