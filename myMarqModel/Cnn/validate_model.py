import random
import os

from sklearn.preprocessing import label_binarize

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import keras
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from skimage import img_as_ubyte
from skimage.transform import resize
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, roc_curve, auc
from sklearn.metrics import classification_report,confusion_matrix
from sklearn.utils.multiclass import unique_labels
from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt

class_names = ['Arch', 'Arch-Loop', 'Composite', 'Loop', 'Peacock', 'Tented Arch', 'Tented Arch-Loop', 'Whorl']
sz = (240, 160)

#df_val = pd.read_pickle("Cnn/data_val.pkl")
df_val = pd.read_pickle("Cnn/data_mm_val.pkl")

#df_val = pd.read_pickle("Cnn/data_val_scanner.pkl")

X = []
for _, img in df_val['image'].iteritems():
    img = resize(img, sz)
    img = img_as_ubyte(img)
    img = img[:, :, [2, 1, 0]]

    X.append(img)

X = np.array(X)
y = np.array(df_val['pattern'] - 1)


def validation_8output(modelname):
    print(modelname)

    model = keras.models.load_model(modelname, compile=False)
    #print(model.summary())

    try:
        predictions = model.predict_classes(X)

        cm = confusion_matrix(y, predictions)
        print(cm)

        cm_df = pd.DataFrame(cm, class_names, class_names)
        plt.figure(figsize=(8, 6), dpi=300)
        #plt.title('Confusion Matrix - First model continues trained - First Dataset')
        #plt.title('Confusion Matrix - First model continues trained - Second Dataset')

        #plt.title('Confusion Matrix - First model - First Dataset')
        #plt.title('Confusion Matrix - First model - Second Dataset')

        #plt.title('Confusion Matrix - Second model - First Dataset')
        #plt.title('Confusion Matrix - Second model - Second Dataset')

        #plt.title('Confusion Matrix - Third model - First Dataset')
        plt.title('Confusion Matrix - Third model - Second Dataset')

        sns.heatmap(cm_df, annot=True, cmap="Blues", fmt='d')
        plt.xlabel('Predicted Label')
        plt.ylabel('True Label')
        plt.show()

        #acc = accuracy_score(y, predictions)
        #print('acc:', acc)
        #pre = precision_score(y, predictions, average='weighted')
        #print('pre:', pre)
        #rec = recall_score(y, predictions, average='weighted')
        #print('rec', rec)
        #f1 = f1_score(y, predictions, average='weighted')
        #print('f1', f1)

        # Compute ROC curve and ROC area for each class
        fpr = dict()
        tpr = dict()
        roc_auc = dict()

        n_classes = 8

        #y_multi_true = label_binarize(y, classes=list(range(n_classes)))
        #y_multi_score = model.predict(X)
        #plt.figure(figsize=(12,9), dpi=300)
        #lw = 2
        #plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')

        #for i in range(n_classes):
        #    fpr[i], tpr[i], _ = roc_curve(y_multi_true[:, i], y_multi_score[:, i])
        #    roc_auc[i] = auc(fpr[i], tpr[i])
        #    plt.plot(fpr[i], tpr[i], lw=lw, label='%s - ROC (area = %0.2f)' % (class_names[i], roc_auc[i]))

        #Compute micro-average ROC curve and ROC area
        #fpr["micro"], tpr["micro"], _ = roc_curve(y_multi_true.ravel(), y_multi_score.ravel())
        #roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

        #plt.plot(fpr["micro"], tpr["micro"], lw=lw, label='micro - ROC curve (area = %0.2f)' % roc_auc["micro"])

        #plt.xlim([0.0, 1.0])
        #plt.ylim([0.0, 1.05])
        #plt.xlabel('False Positive Rate')
        #plt.ylabel('True Positive Rate')
        #plt.title(modelname)
        #plt.legend(loc="lower right")
        #plt.show()

    except Exception as e:
        print(e)

   # predictions = model.predict(X)

    #num_rows = 10
    #num_cols = 6
    #num_images = num_rows * num_cols

    #f = plt.figure(figsize=(2 * 2 * num_cols, 2 * num_rows))

    #random.seed = 42

    #for j in range(60):
    #    i = j
    #    plt.subplot(num_rows, 2 * num_cols, 2 * j + 1)
    #    plot_image(predictions[i], y[i], X[i], class_names)
    #    plt.subplot(num_rows, 2 * num_cols, 2 * j + 2)
    #    plot_value_array(predictions[i], y[i], class_names)
    #plt.tight_layout()
    #plt.show()
    #filename = 'Cnn/'+modelname.split('/')[1]+'.png'
    #f.savefig(filename)

def plot_image(predictions_array, true_label, img, class_names):
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])

    plt.imshow(img, cmap=plt.cm.binary)

    predicted_label = np.argmax(predictions_array)

    if predicted_label == true_label:
        color = 'blue'
    else:
        color = 'red'

    plt.xlabel("{} {:2.0f}% ({})".format(
        class_names[predicted_label],
        100 * np.max(predictions_array),
        class_names[true_label]),
        color=color
    )




def plot_value_array(predictions_array, true_label, class_names):
    plt.grid(False)
    plt.xticks(range(len(class_names)))
    plt.yticks([])
    thisplot = plt.bar(range(len(class_names)), predictions_array, color="#777777")
    plt.ylim([0, 1])
    predicted_label = np.argmax(predictions_array)

    thisplot[predicted_label].set_color('red')
    thisplot[true_label].set_color('blue')




#print('predict_pattern_20191214075545_all_16_VGG16.model')
#validation_8output("models_iside_2_90gradi/predict_pattern_20191214075545_all_16_VGG16.model")
#actual best ds1
#acc: 0.8693957115009746
#pre: 0.886323991569591
#rec 0.8693957115009746
#f1 0.8743615911384913



#validation_8output("models_iside_8_90gradi_mm/predict_pattern_20200111214801_all_16_mm_VGG16.model")
#ds1
#acc: 0.7212475633528265
#pre: 0.6968885769373035
#rec 0.7212475633528265
#f1 0.6946788256147145

#ds2
#acc: 0.7241379310344828
#pre: 0.7205933335626383
#rec 0.7241379310344828
#f1 0.7160048542253952



#validation_8output("models_iside_8_90gradi_mm/predict_pattern_20200112115145_all_16_ds1_ds2_VGG16.model")
#ds2
#acc: 0.8050682261208577
#pre: 0.82345177130005
#rec 0.8050682261208577
#f1 0.8097254571699609




validation_8output("models_iside_8_90gradi_mm/predict_pattern_20200112152337_all_16_ds1_ds2_VGG16.model")
#ds1
# acc: 0.8408057179987004
#pre: 0.8844764606072101
#rec 0.8408057179987004
#f1 0.8539996215428314

#ds2
#acc: 0.7701149425287356
#pre: 0.7897014479773101
#rec 0.7701149425287356
#f1 0.7778880023944502