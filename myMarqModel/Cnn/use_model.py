#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import skimage
import keras

def process_image(model_xy, image):
    image = image[:,:,::-1] # RGB to BGR
    image = skimage.transform.resize(image,(320,480))
    pattern = model_pattern.predict([image])
    return pattern
    


# usage
model_pattern = keras.models.load_model("model.final")

# carichi i modelli in __init__ del controller
# li usi nell'evento immagine