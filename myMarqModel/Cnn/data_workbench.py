import pandas as pd

df: pd.DataFrame = pd.read_pickle('Cnn/data_full.pkl')

# dp = df['pattern']
# dp.to_pickle('Cnn/data_full_y.pkl')

df_list = [df[df['pattern'] == i] for i in range(1, 9)]

df_train_list = []
df_test_list = []
df_val_list = []

for df in df_list:
    n = len(df)

    n_train = int(0.6 * n)
    n_test = int(0.2 * n) + n_train

    tr = df.iloc[:n_train]
    ts = df.iloc[n_train:n_test]
    vl = df.iloc[n_test:]

    df_train_list.append(tr)
    df_test_list.append(ts)
    df_val_list.append(vl)

    print('n:', n, '\tn_train:', len(tr), '\tn_test:', len(ts), '\tn_val:', len(vl))

df_train = pd.concat(df_train_list)
df_test = pd.concat(df_test_list)
df_val = pd.concat(df_val_list)

print('n_train:', len(df_train), '\nn_test:', len(df_test), '\nn_val:', len(df_val))

df_train.to_pickle('Cnn/data_train.pkl')
df_test.to_pickle('Cnn/data_test.pkl')
df_val.to_pickle('Cnn/data_val.pkl')
