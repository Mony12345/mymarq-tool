#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import keras
import skimage
import os
import time
import tqdm
import pandas as pd
import numpy as np
from os import listdir
from os.path import isfile

import matplotlib.pyplot as plt

from keras import Sequential

from functions import mkbatch, transform_complex
from functions import mygenerator, sample, transform_all

from keras.layers import Conv2D, MaxPooling2D, Dense, Dropout, Flatten, AveragePooling2D, Activation, BatchNormalization

# %%

finger_source = "data_test"

sz = (120, 80)  # network images size
batch_size = 64

int_sz = (240, 160)  # intermediate image size, save the memory!


# %%

def load_finger():
    images = [f for f in listdir(os.path.join(finger_source)) if isfile(os.path.join(finger_source, f))]
    print(len(images))
    dataset = []
    for image in tqdm.tqdm(images):
        # skip files that start with .
        if image.startswith("."):
            continue

        fileName = image.split("_")
        label_pattern = int(fileName[0])
        print(label_pattern)

        try:
            im = skimage.io.imread(os.path.join(finger_source, image))
        except (OSError, ValueError) as e:
            print("ignoring due to exception: ", str(e))
            continue

        # just convert the image to ubytes
        im = skimage.transform.resize(im, int_sz)
        im = skimage.img_as_ubyte(im)
        # plt.imshow(im);
        # plt.show();
        im = im[:, :, [2, 1, 0]]
        # plt.imshow(im);
        # plt.show();
        dataset.append(dict(file=image, pattern=label_pattern, image=im))
    data = pd.DataFrame(dataset)
    return data


def train_pattern_VGG16(sz):
    imshape = (sz[0], sz[1], 3)
    nb_filters = 64
    pool_size = (2, 2)  # size of pooling area for max pooling
    kernel_size = (3, 3)  # (5,5) # 3x3

    model = Sequential(name="finger_pattern")
    model.add(Conv2D(nb_filters, kernel_size, input_shape=imshape, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=pool_size))

    model.add(Conv2D(nb_filters * 2, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 2, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=pool_size))

    model.add(Conv2D(nb_filters * 4, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 4, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 4, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=pool_size))

    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=pool_size))

    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=pool_size))

    model.add(Flatten())
    model.add(Dropout(rate=0.1))
    model.add(Dense(4096))
    model.add(Activation('relu'))
    model.add(Dense(4096))
    model.add(Activation('relu'))
    model.add(Dense(2048))
    model.add(Activation('relu'))
    model.add(Dropout(rate=0.33))
    model.add(Dense(8))
    model.add(Activation('softmax'))

    model.compile(loss="categorical_crossentropy", optimizer=keras.optimizers.RMSprop(), metrics=['accuracy'])

    return model


# %%

# %%
def train_pattern_VGG19(sz):
    imshape = (sz[0], sz[1], 3)
    nb_filters = 64
    pool_size = (2, 2)  # size of pooling area for max pooling
    kernel_size = (3, 3)  # (5,5) # 3x3

    model = Sequential(name="finger_pattern")
    model.add(Conv2D(nb_filters, kernel_size, input_shape=imshape, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=pool_size))

    model.add(Conv2D(nb_filters * 2, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 2, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=pool_size))

    model.add(Conv2D(nb_filters * 4, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 4, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 4, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 4, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=pool_size))

    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=pool_size))

    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters * 8, kernel_size, padding="same"))
    # model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=pool_size))

    model.add(Flatten())
    model.add(Dropout(rate=0.1))
    model.add(Dense(4096))
    model.add(Activation('relu'))
    model.add(Dense(4096))
    model.add(Activation('relu'))
    model.add(Dense(2048))
    model.add(Activation('relu'))
    model.add(Dropout(rate=0.33))
    model.add(Dense(8))
    model.add(Activation('softmax'))

    model.compile(loss="categorical_crossentropy", optimizer=keras.optimizers.RMSprop(), metrics=['accuracy'])

    return model


# %%


# CNN creation

# load fingers image
# data = load_finger()
# data.to_pickle("Cnn/data_full.pkl")

# Prepare training and testing sets
train_set = pd.read_pickle("Cnn/data_train.pkl")
test_set = pd.read_pickle("Cnn/data_test.pkl")
# val_set = pd.read_pickle("Cnn/data_val.pkl")

# build and train the neural network
# print(train_pattern(sz).summary())

(X_test, y_test) = mkbatch(test_set, batch_size, sz)

modelid = time.strftime("%Y%m%d%H%M%S")
callbacks_list = [
    keras.callbacks.EarlyStopping(
        monitor='val_acc',
        patience=50),
    keras.callbacks.ModelCheckpoint(
        filepath='model_checkpoint_best_VGG16_{}.h5'.format(modelid),
        monitor='val_loss',
        save_best_only=True)
]

model = train_pattern_VGG16(sz)

history = model.fit_generator(mygenerator(train_set, batch_size, sz),
                              steps_per_epoch=5,
                              epochs=10,
                              verbose=1,
                              validation_data=(X_test, y_test),
                              callbacks=callbacks_list)

keras.models.save_model(model, "{}.model".format("predict_pattern_VGG16"))

# (X_test,y_test) = mkbatch(train_set, batch_size, sz)

# modelid = time.strftime("%Y%m%d%H%M%S")
# callbacks_list = [
#    keras.callbacks.EarlyStopping(
#        monitor='val_acc',
#        patience=50),
#    keras.callbacks.ModelCheckpoint(
#        filepath='model_checkpoint_best_VGG19_{}.h5'.format(modelid),
#        monitor='val_loss',
#        save_best_only=True)
# ]

# model = train_pattern_VGG19(sz)

# history=model.fit_generator(mygenerator(train_set, batch_size, sz),
#                    steps_per_epoch=1,
#                    epochs=1,
#                    verbose=1,
#                    validation_data=(X_test,y_test),
#                    callbacks=callbacks_list)

# keras.models.save_model(model,"{}.model".format("predict_pattern_VGG19"))


# val_x, val_y = transform_all(val_set, sz)


# %% TEST THE MODEL ON SOME truly left out data

# %%
# final_data = load_finger(0.06)
# final_data.to_pickle("finaldata")
# val_set = pd.read_pickle("data_validation")

# %%
# fin_x, fin_y = transform_all(val_set, sz)
# %%
# def preditct(model, fin_x, fin_y):
#    predictions = model.predict([fin_x])
#    predictions = pd.DataFrame(predictions, columns=['pattern'])
#    predictions['tx'] = -fin_y[:,0]
#    predictions['ty'] = fin_y[:,1]
#    predictions['x'] = -predictions['x']
#    return predictions

# def prepare_predict(m, data):
#    d_x,d_y = transform_all(data, sz)
#    return preditct(m, d_x, d_y), d_x, d_y
