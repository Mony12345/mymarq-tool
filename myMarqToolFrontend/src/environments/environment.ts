// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  baseUrl: 'http://mony-lenovo:8080',
  baseAPIPath: '/api/v1/tool',
  baseAdminAPIPath: '/api/v1/admin',
  clientId: 'mmtool-client',
  tablePageSize: 10
};
