export const environment = {
  production: true,
  baseUrl: 'http://fakerestapi.azurewebsites.net/api',
  baseAPIPath: '/api/v1/tool',
  baseAdminAPIPath: '/api/v1/admin',
  clientId: 'mmtool-client',
  tablePageSize: 10
};
