import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Component, NgModule, OnInit } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import 'rxjs/add/operator/finally';
import { AuthenticationService, LoginContext } from './../core/authentication/authentication.service';
import { ErrorResponse } from './../core/authentication/credentials-response';


@NgModule({
    imports: [
        FormsModule
    ]
})

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    isLoading = false;
    numberOfLoginTries = 0;
    redirectUrl: string;
    credentialsKey = 'credentials';

    constructor(private router: Router,
        private authenticationService: AuthenticationService,
        private toastrService: ToastrService,
        private translateService: TranslateService) {

    }
    ngOnInit() {
        
        if (this.authenticationService.isAuthenticated()) {
            this.loginSuccess();
        }
    }

    doLogin(loginForm: NgForm) {
        const loginContext = {
            username: loginForm.value.email,
            password: loginForm.value.password,
            remember: true
        } as LoginContext;

        this.authenticationService.login(loginContext)
            .finally(() => {
                this.isLoading = false;
            })
            .subscribe(result => {
                this.loginSuccess();
                this.numberOfLoginTries = 0;
            }, (errorRes: ErrorResponse) => {
                this.toastrService.error(this.translateService.instant('login.error'));
            });
    }

    loginSuccess() {
        this.router.navigate(['consultation']);
    }
}
