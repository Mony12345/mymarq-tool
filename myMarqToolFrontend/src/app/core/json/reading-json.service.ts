import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ReadingJsonService {

    private _countriesURL = 'assets/countries/countries-LANGUAGE.json';
    private _languagesURL = 'assets/languages/languages-LANGUAGE.json';

    constructor(private http: HttpClient) {
    }

    public getCountries(language: string): Observable<any> {
        return this.http.get(this._countriesURL.replace('LANGUAGE', language))
            .map((response: any) => response);
    }

    public getLanguages(language: string): Observable<any> {
        return this.http.get(this._languagesURL.replace('LANGUAGE', language))
            .map((response: any) => response);
    }
}
