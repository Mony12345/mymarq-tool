import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { AccessTokenService } from './../authentication/access-token.service';
import { AuthenticationService } from './../authentication/authentication.service';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/mergeMap';

@Injectable({
    providedIn: 'root'
})
export class HttpCustomInterceptor implements HttpInterceptor {
    headers: HttpHeaders;
    private compUnsubscribe$: Subject<void>;
    constructor(private accessTokenService: AccessTokenService, private authenticationService: AuthenticationService,
        private inj: Injector,
        // private store: Store<AppState>,
        private router: Router,
        private matDialog: MatDialog
    ) {
        this.compUnsubscribe$ = new Subject<void>();
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.accessTokenService.credentials &&
            this.getTokenExpDate(this.accessTokenService.credentials.refreshToken) < (Date.now() / 1000)) {
            this.authenticationService.logout()
                .takeUntil(this.compUnsubscribe$)
                .subscribe(() => {
                    this.compUnsubscribe$.next();
                    this.compUnsubscribe$.complete();
                });
        } else if (this.shouldRefreshToken(req.url)) {
            return this.authenticationService.refreshToken().mergeMap(() => {
                return next.handle(this.getAuthReq(req));
            });
        } else {
            return next.handle(this.getAuthReq(req));
        }
    }

    shouldRefreshToken(url: string): boolean {
        // don't refresh the token if it's an authentication or registration url
        if (!url.includes('/oauth/token') && !url.includes('/oauth/refresh_token')) {
            if (this.accessTokenService.credentials
                && this.getTokenExpDate(this.accessTokenService.credentials.token) < (Date.now() / 1000)) {
                return true;
            }
        }

        return false;
    }

    getAuthReq(req: HttpRequest<any>): HttpRequest<any> {
        const token = this.accessTokenService.credentials ? this.accessTokenService.credentials.token : '';
        return req.clone({
            headers: req.headers.set('Authorization', `Bearer ${token}`)
        });
    }

    getTokenExpDate(token: string): any {
        const jsonData = JSON.parse(atob(token.split('.')[1]));
        return jsonData.exp;
    }

}