import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import 'rxjs/add/observable/of';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { map, catchError, filter } from 'rxjs/operators';

import { ErrorResponse, CredentialsResponse } from '../authentication/credentials-response';
import { Credentials, AccessTokenService } from '../authentication/access-token.service';
import { environment } from '../../../environments/environment';

export interface LoginContext {
    username: string;
    password: string;
    remember?: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    private _loginSuccess$: ReplaySubject<Credentials>;
    headers: HttpHeaders;
    challenge: string;

    constructor(private httpClient: HttpClient, public accessTokenService: AccessTokenService) {
        this._loginSuccess$ = new ReplaySubject<Credentials>(1);
        this.headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
        this.accessTokenService.getCredentialFromStorage().subscribe((credentials) => {
            return this._loginSuccess$.next(this.accessTokenService.credentials);
        });
    }

    /**
     * Authenticates the user.
     */
    login(context: LoginContext): Observable<any> {
        return this.httpClient.post(this.getAuthenticationURL(),
            `grant_type=password`
            + `&username=${encodeURIComponent(context.username)}`
            + `&password=${encodeURIComponent(context.password)}`
            + `&client_id=${environment.clientId}`,
            { headers: this.headers }
        )
            .pipe(
                map((response: any) => {
                    return this.handleCredentialsResponse(response, context);
                }),
                catchError((error: any) => {
                    return this.handleErrorObservable(error);
                })
            );
    }

    /**
     * Logs out the user and clear credentials.
     */
    logout(): Observable<boolean> {
        this.accessTokenService.removeCredentials();
        this._loginSuccess$.next(null);
        return Observable.of(true);
    }

    refreshToken(): Observable<any> {
        return this.httpClient.post(this.getAuthenticationURL(),
            `grant_type=refresh_token`
            + `&refresh_token=${encodeURIComponent(this.accessTokenService.credentials.refreshToken)}`
            + `&client_id=${environment.clientId}`,
            { headers: this.headers }
        )
            .pipe(
                map((response: any) => {
                    return this.handleCredentialsResponse(response, { username: this.accessTokenService.credentials.username }, true);
                }),
                catchError(this.handleErrorObservable)
            );
    }

    refreshExpiredToken(): Observable<any> {
        if (this.getTokenExpDate(this.accessTokenService.credentials.token) < (Date.now() / 1000)) {
            return this.refreshToken();
        }

        return Observable.of(true);
    }

    getTokenExpDate(token: string): any {
        const jsonData = JSON.parse(atob(token.split('.')[1]));
        return jsonData.exp;
    }

    handleCredentialsResponse(response: any, context: any, refresh?: boolean): Observable<any> {
        const credentialsResponse = new CredentialsResponse(response);
        const tokenParts = JSON.parse(atob(credentialsResponse.accessToken.split('.')[1]));

        const data = {
            username: context.username,
            token: credentialsResponse.accessToken,
            expiresIn: credentialsResponse.expiresIn,
            refreshToken: credentialsResponse.refreshToken,
            groups: tokenParts.groups,
            userUUID: credentialsResponse.userUUID,
            language: credentialsResponse.language
        };
        this.accessTokenService.setCredentials(data);

        if (!refresh) {
            this._loginSuccess$.next(data);
        }

        return Observable.of(credentialsResponse);
    }

    private handleErrorObservable(httpError: HttpErrorResponse) {
        const errorResponse = new ErrorResponse(httpError.error.message, httpError);
        return ErrorObservable.create(errorResponse);
    }

    /**
     * Checks is the user is authenticated.
     */
    isAuthenticated(): boolean {
        return !!this.accessTokenService.credentials;
    }

    isAdmin(): boolean {
        return this.isAuthenticated() && this.accessTokenService.isAdmin();
    }

    getAuthenticationURL(): string {
        return `${environment.baseUrl}/oauth/token`;
    }
}
