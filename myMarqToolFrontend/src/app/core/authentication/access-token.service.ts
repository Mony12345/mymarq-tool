import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { ReadingJsonService } from '../json/reading-json.service';

export interface Credentials {
    // Customize received credentials here
    username: string;
    token: string;
    expiresIn: number;
    refreshToken: string;
    groups: string[];
    userUUID: any;
    language: any;
}
export enum Role {
    COMPANY = 'COMPANY',
    CONSULTANT = 'CONSULTANT',
    ADMIN = 'ADMIN'
}

export const credentialsKey = 'credentials';

@Injectable({
    providedIn: 'root'
})
export class AccessTokenService {
    private _credentials: Credentials;
    private _collectiveAccountCredentials: Credentials;

    constructor() {
        this._credentials = JSON.parse(sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey));
    }

    public getCredentialFromStorage(): Observable<Credentials> {
        try {
            this._credentials = JSON.parse(sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey));
        } catch (error) {
            console.log(error);
        } finally {
            return of(this._credentials);
        }
    }

    public setCredentials(credentials: Credentials, remember?: boolean) {
        this._credentials = credentials || null;
        const storage = remember ? localStorage : sessionStorage;
        storage.setItem(credentialsKey, JSON.stringify(credentials));
    }

    public removeCredentials() {
        this._credentials = null;
        this._collectiveAccountCredentials = null;
        sessionStorage.clear();
        localStorage.clear();
    }

    public getUserLanguage(): string {
        if (this._credentials) {
            return this._credentials.language;
        } else {
            return undefined;
        }
    }

    public getUserUUID(): any {
        if (this._credentials) {
            return this._credentials.userUUID;
        } else {
            return undefined;
        }
    }

    public isAdmin(): boolean {
        if (this._credentials && this._credentials.token) {
            return this.getDecodedToken().authorities.filter(authority => authority === Role.ADMIN).length > 0;
        }

        return false;
    }

    public isConsultant(): boolean {
        if (this._credentials && this._credentials.token) {
            return this.getDecodedToken().authorities.filter(authority => authority === Role.CONSULTANT).length > 0;
        }

        return false;
    }

    public isCompany(): boolean {
        if (this._credentials && this._credentials.token) {
            return this.getDecodedToken().authorities.filter(authority => authority === Role.COMPANY).length > 0;
        }

        return false;
    }

    private getDecodedToken(): any {
        return JSON.parse(atob(this._credentials.token.split('.')[1]));
    }

    get credentials(): Credentials {
        return this._credentials;
    }

    get collectiveaccountCredentials(): Credentials {
        return this._collectiveAccountCredentials;
    }
}

