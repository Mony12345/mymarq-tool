
export class CredentialsResponse {
    private _accessToken: string;
    private _tokenType: string;
    private _expiresIn: number;
    private _refreshToken: string;
    private _scope: string;
    private _payload: any;

    constructor(response?: any) {
        if (response) {
            this.setAccessToken(response['access_token'])
                .setTokenType(response['token_type'])
                .setExpiresIn(response['expires_in'])
                .setRefreshToken(response['refresh_token'])
                .setScope(response['scope']);

            this._payload = JSON.parse(atob(this._accessToken.split('.')[1]));
        }
    }

    public get accessToken(): string {
        return this._accessToken;
    }

    public setAccessToken(accessToken: string): CredentialsResponse {
        this._accessToken = accessToken;
        return this;
    }

    public get tokenType(): string {
        return this._tokenType;
    }

    public setTokenType(tokenType: string): CredentialsResponse {
        this._tokenType = tokenType;
        return this;
    }

    public get expiresIn(): number {
        return this._expiresIn;
    }

    public setExpiresIn(expiresIn: number): CredentialsResponse {
        this._expiresIn = expiresIn;
        return this;
    }

    public get refreshToken(): string {
        return this._refreshToken;
    }

    public setRefreshToken(refreshToken: string): CredentialsResponse {
        this._refreshToken = refreshToken;
        return this;
    }

    public get scope(): string {
        return this._scope;
    }

    public setScope(scope: string): CredentialsResponse {
        this._scope = scope;
        return this;
    }

    public get userUUID(): string {
        return this._payload['userUUID'];
    }

    public get language(): string {
        return this._payload['language'];
    }
}

export class ErrorResponse {
    constructor(public message: string, public detail: any = {}, public challenge?: string) { }
}
