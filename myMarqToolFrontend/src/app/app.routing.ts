import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthGuard } from './shared/guard';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'consultation',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        runGuardsAndResolvers: 'always',
    }, {
        path: '',
        component: AdminLayoutComponent,
        children: [{
            path: '',
            loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
        }],
        canActivate: [AuthGuard],
        runGuardsAndResolvers: 'always',
    },
    {
        path: 'login', component: LoginComponent,
        runGuardsAndResolvers: 'always',
    },

];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes, {
            useHash: true,
            onSameUrlNavigation: 'reload'
        })
    ],
    exports: [
        RouterModule
    ],
})
export class AppRoutingModule { }
