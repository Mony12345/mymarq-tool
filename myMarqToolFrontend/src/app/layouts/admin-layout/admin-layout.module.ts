import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatRippleModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatTooltipModule,
  MatExpansionModule,
  MatSliderModule,
  MatCheckboxModule,
  MatRadioModule,
  MatDatepickerModule,
  MatIconModule,
  MatSlideToggleModule
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { UserDetailComponent } from './../../user/detail/user-detail.component';
import { UserListComponent } from './../../user/user-list.component';
import { ConsultationListComponent } from '../../consultation/consultation-list.component';
import { ConsultationDetailComponent } from '../../consultation/detail/consultation-detail.component';
import { AdminLayoutRoutes } from './admin-layout.routing';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    TranslateModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatSliderModule,
    MatCheckboxModule,
    MatRadioModule,
    MatDatepickerModule,
    MatIconModule,
    MatSlideToggleModule
  ],
  declarations: [
    ConsultationListComponent,
    ConsultationDetailComponent,
    UserListComponent,
    UserDetailComponent
  ]
})

export class AdminLayoutModule { }
