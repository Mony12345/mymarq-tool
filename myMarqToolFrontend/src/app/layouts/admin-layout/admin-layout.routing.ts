import { AuthGuardAdmin } from './../../shared/guard/auth.guard.admin';
import { Routes } from '@angular/router';

import { ConsultationListComponent } from '../../consultation/consultation-list.component';
import { ConsultationDetailComponent } from '../../consultation/detail/consultation-detail.component';
import { UserListComponent } from '../../user/user-list.component';
import { UserDetailComponent } from '../../user/detail/user-detail.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'consultation', component: ConsultationListComponent },
    { path: 'consultation/detail', component: ConsultationDetailComponent },
    { path: 'consultation/detail/:uuid', component: ConsultationDetailComponent },
    { path: 'user', component: UserListComponent, canActivate: [AuthGuardAdmin] },
    { path: 'user/detail', component: UserDetailComponent, canActivate: [AuthGuardAdmin] },
    { path: 'user/detail/:uuid', component: UserDetailComponent }
];
