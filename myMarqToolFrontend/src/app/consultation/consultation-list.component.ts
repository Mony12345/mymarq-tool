import { CommonModule } from '@angular/common';
import { AuthenticationService } from './../core/authentication/authentication.service';
import { AfterViewInit, Component, OnInit, ViewChild, NgModule } from '@angular/core';
import { MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { tap } from 'rxjs/operators';
import { ConsultationDataSource } from '../shared/datasources/consultation.datasource';
import { Consultation } from '../shared/model/consultation';
import { ConsultationService } from '../shared/services/consultation.service';
import { ConsultationSearchQuery } from './../shared/services/consultation.service';
import { environment } from '../../environments/environment';
import { TouchSequence } from 'selenium-webdriver';

@Component({
    selector: 'app-consultation-list',
    templateUrl: './consultation-list.component.html',
    styleUrls: ['./consultation-list.component.css']
})

export class ConsultationListComponent implements AfterViewInit, OnInit {

    displayedColumns: string[];
    dataSource: ConsultationDataSource;
    searchQuery: ConsultationSearchQuery;

    isAdmin: boolean;
    userUUID: string;
    readonly pageSize = environment.tablePageSize;

    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

    constructor(
        public consultationService: ConsultationService,
        private router: Router,
        private toastService: ToastrService,
        private translateService: TranslateService,
        private authenticationService: AuthenticationService
    ) {
        this.displayedColumns = ['uuid', 'firstname', 'lastname', 'createdAt', 'updatedAt'];
        this.isAdmin = authenticationService.accessTokenService.isAdmin();
        this.userUUID = authenticationService.accessTokenService.getUserUUID();
    }

    ngOnInit() {
        this.searchQuery = new ConsultationSearchQuery();
        this.dataSource = new ConsultationDataSource(this.consultationService);
        this.dataSource.loadConsultations(0, this.pageSize, this.searchQuery);
       
    }

    ngAfterViewInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadConsultationsPage())
            )
            .subscribe();
    }

    loadConsultationsPage() {
        this.dataSource.loadConsultations(
            this.paginator.pageIndex,
            this.pageSize,
            this.searchQuery);
    }

    openDetail(consultation: Consultation) {
        this.router.navigate(['/consultation/detail/', consultation.uuid]);
    }

    createNewConsultation() {
        this.router.navigate(['/consultation/detail']);
    }

    exportConsultations(){
        this.consultationService.exportConsultations().subscribe((res: any) => {
			let filename = res.headers.get('Content-Disposition').split(';')[1].split('filename')[1].split('=')[1].trim();
			let blob = new Blob([res.body], {type: res.headers.get('Content-Type')});
			let fileUrl = URL.createObjectURL(blob);
			let a = document.createElement('a');
			a.href = fileUrl;
			a.download = filename;
			let e = document.createEvent('MouseEvents');
			e.initMouseEvent('click', true, false, document.defaultView, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
			a.dispatchEvent(e);
			a.remove();
		},
		(error: any) => {
			this.toastService.error(this.translateService.instant('message.error'));
		});
    }
}