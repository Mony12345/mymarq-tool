import { AccessTokenService } from "./../../core/authentication/access-token.service";
import { Component, OnInit, NgModule, ViewChild } from "@angular/core";
import { FormsModule, NgForm, ReactiveFormsModule } from "@angular/forms";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Consultation } from "./../../shared/model/consultation";
import { ConsultationService } from "./../../shared/services/consultation.service";
import { ReadingJsonService } from "./../../core/json/reading-json.service";
import { ToastrService } from "ngx-toastr";
import { TranslateService } from "@ngx-translate/core";
import { DomSanitizer, SafeStyle } from "@angular/platform-browser";
import { environment } from '../../../environments/environment';
import { AuthenticationService } from './../../core/authentication/authentication.service';

declare var onBodyLoad: any;
declare var beginOperation: any;

@NgModule({
  imports: [FormsModule, ReactiveFormsModule]
})
@Component({
  selector: "app-consultation-detail",
  templateUrl: "./consultation-detail.component.html",
  styleUrls: ["./consultation-detail.component.css"]
})


export class ConsultationDetailComponent implements OnInit {
  panelOpenState = true;
  countries: any;

  consultation: Consultation;
  serverConsultation: Consultation;
  userLanguage: string;
  consultationUUID: string;

  isCompany: boolean;
  
  isAuthenticatedUser: boolean;

  form: FormGroup;

  beURL_MMT: string;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private consultationService: ConsultationService,
    private readingJsonService: ReadingJsonService,
    private accessTokenService: AccessTokenService,
    private toastService: ToastrService,
    private translateService: TranslateService,
    private sanitizer: DomSanitizer,
    private fb: FormBuilder,
    private authenticationService: AuthenticationService
  ) {
    
    this.userLanguage = this.accessTokenService.getUserLanguage();
    this.isCompany = this.accessTokenService.isCompany();
    this.form = this.fb.group({
      name: ["1"],
      avatar: [null]
    });
  }

  ngOnInit() {
    this.readingJsonService.getCountries(this.userLanguage).subscribe(
      data => {
        this.countries = data;
      },
      error => console.log(error)
    );

    this.consultation = new Consultation();
    this.serverConsultation = new Consultation();
    this.consultationUUID = this.route.snapshot.params["uuid"];
    

    if (this.consultationUUID != null) {
    
      this.getConsultationDetails(this.consultationUUID).subscribe(
        consultation => {
          this.consultation = consultation;
          this.copyConsultation();
          if (this.isAuthenticatedUser || !this.consultationUUID){
            onBodyLoad(this.userLanguage);
          }
          
        },
        err => {
          this.toastService.error(
            this.translateService.instant("message.error")
          );
        }
      );
      for (let i = 1; i <= 10; i++) {
        this.consultationService.getFingerImage(this.consultationUUID, i.toString()).subscribe((image: any) => {});
      }     
    }
  }


  getConsultationDetails(consultationUUID) {
    return this.consultationService.getConsultation(consultationUUID);
  }

  goBack() {
    this.router.navigate(["consultation"]);
  }

  saveConsultation() {
    if (
      this.consultation.firstname != null &&
      this.consultation.firstname !== ""
    ) {
      
            if (this.consultation.uuid != null) {
        const patchRequest = this.mapPatchRequest();
        if (Array.isArray(patchRequest) && patchRequest.length) {
          this.consultationService
            .updateConsultation(this.consultation.uuid, patchRequest)
            .subscribe(
              response => {
                this.toastService.success(
                  this.translateService.instant("message.save.ok")
                );
                this.getConsultationDetails(this.consultationUUID).subscribe(
                  consultation => {
                    this.consultation = consultation;
                    this.copyConsultation();
                    onBodyLoad();
                    
                  },
                  err => {
                    this.toastService.error(
                      this.translateService.instant("message.error")
                    );
                  }
                );
              },
              err => {
                this.toastService.error(
                  this.translateService.instant("message.save.error")
                );
              }
            );
        } else {
          this.toastService.info(
            this.translateService.instant("message.save.nothing")
          );
        }
      } else {
        this.consultationService
          .createConsultation(this.consultation)
          .subscribe(
            (response:any) => {
              this.toastService.success(
                this.translateService.instant("message.save.ok")
              );
              //TODO trovare un altro modo per avere uuid 
              this.router.navigateByUrl('/consultation/detail/' +response.body.uuid);

            },
            err => {
              this.toastService.error(
                this.translateService.instant("message.save.error")
              );
            }
          );
      }
    }
  }

  mapPatchRequest(): any {
    const patchRequest = [];
    if (this.serverConsultation.firstname !== this.consultation.firstname) {
      patchRequest.push({
        op: "replace",
        path: "/firstname",
        value: this.consultation.firstname
      });
    }
    if (this.serverConsultation.lastname !== this.consultation.lastname) {
      patchRequest.push({
        op: "replace",
        path: "/lastname",
        value: this.consultation.lastname
      });
    }


    if (this.serverConsultation.dateBirth !== this.consultation.dateBirth) {
      patchRequest.push({
        op: "replace",
        path: "/dateBirth",
        value: this.consultation.dateBirth + "T00:00:00Z"
      });
    }
    if (this.serverConsultation.gender !== this.consultation.gender) {
      patchRequest.push({
        op: "replace",
        path: "/gender",
        value: this.consultation.gender
      });
    }
    if (this.serverConsultation.nationality !== this.consultation.nationality) {
      patchRequest.push({
        op: "replace",
        path: "/nationality",
        value: this.consultation.nationality
      });
    }
    if (this.serverConsultation.civilStatus !== this.consultation.civilStatus) {
      patchRequest.push({
        op: "replace",
        path: "/civilStatus",
        value: this.consultation.civilStatus
      });
    }
    if (this.serverConsultation.numberChild !== this.consultation.numberChild) {
      patchRequest.push({
        op: "replace",
        path: "/numberChild",
        value: this.consultation.numberChild
      });
    }
    if (
      this.serverConsultation.professional !== this.consultation.professional
    ) {
      patchRequest.push({
        op: "replace",
        path: "/professional",
        value: this.consultation.professional
      });
    }
    if (this.serverConsultation.education !== this.consultation.education) {
      patchRequest.push({
        op: "replace",
        path: "/education",
        value: this.consultation.education
      });
    }
    if (this.serverConsultation.email !== this.consultation.email) {
      patchRequest.push({
        op: "replace",
        path: "/email",
        value: this.consultation.email
      });
    }
    if (this.serverConsultation.street !== this.consultation.street) {
      patchRequest.push({
        op: "replace",
        path: "/street",
        value: this.consultation.street
      });
    }
    if (
      this.serverConsultation.streetNumber !== this.consultation.streetNumber
    ) {
      patchRequest.push({
        op: "replace",
        path: "/streetNumber",
        value: this.consultation.streetNumber
      });
    }
    if (this.serverConsultation.cap !== this.consultation.cap) {
      patchRequest.push({
        op: "replace",
        path: "/cap",
        value: this.consultation.cap
      });
    }
    if (this.serverConsultation.city !== this.consultation.city) {
      patchRequest.push({
        op: "replace",
        path: "/city",
        value: this.consultation.city
      });
    }
    if (this.serverConsultation.country !== this.consultation.country) {
      patchRequest.push({
        op: "replace",
        path: "/country",
        value: this.consultation.country
      });
    }
    if (this.serverConsultation.phoneNumber !== this.consultation.phoneNumber) {
      patchRequest.push({
        op: "replace",
        path: "/phoneNumber",
        value: this.consultation.phoneNumber
      });
    }
    if (
      this.serverConsultation.fingerprint1 !== this.consultation.fingerprint1
    ) {
      patchRequest.push({
        op: "replace",
        path: "/fingerprint1",
        value: this.consultation.fingerprint1
      });
    }
    if (
      this.serverConsultation.fingerprint2 !== this.consultation.fingerprint2
    ) {
      patchRequest.push({
        op: "replace",
        path: "/fingerprint2",
        value: this.consultation.fingerprint2
      });
    }
    if (
      this.serverConsultation.fingerprint3 !== this.consultation.fingerprint3
    ) {
      patchRequest.push({
        op: "replace",
        path: "/fingerprint3",
        value: this.consultation.fingerprint3
      });
    }
    if (
      this.serverConsultation.fingerprint4 !== this.consultation.fingerprint4
    ) {
      patchRequest.push({
        op: "replace",
        path: "/fingerprint4",
        value: this.consultation.fingerprint4
      });
    }
    if (
      this.serverConsultation.fingerprint5 !== this.consultation.fingerprint5
    ) {
      patchRequest.push({
        op: "replace",
        path: "/fingerprint5",
        value: this.consultation.fingerprint5
      });
    }
    if (
      this.serverConsultation.fingerprint6 !== this.consultation.fingerprint6
    ) {
      patchRequest.push({
        op: "replace",
        path: "/fingerprint6",
        value: this.consultation.fingerprint6
      });
    }
    if (
      this.serverConsultation.fingerprint7 !== this.consultation.fingerprint7
    ) {
      patchRequest.push({
        op: "replace",
        path: "/fingerprint7",
        value: this.consultation.fingerprint7
      });
    }
    if (
      this.serverConsultation.fingerprint8 !== this.consultation.fingerprint8
    ) {
      patchRequest.push({
        op: "replace",
        path: "/fingerprint8",
        value: this.consultation.fingerprint8
      });
    }
    if (
      this.serverConsultation.fingerprint9 !== this.consultation.fingerprint9
    ) {
      patchRequest.push({
        op: "replace",
        path: "/fingerprint9",
        value: this.consultation.fingerprint9
      });
    }
    if (
      this.serverConsultation.fingerprint10 !== this.consultation.fingerprint10
    ) {
      patchRequest.push({
        op: "replace",
        path: "/fingerprint10",
        value: this.consultation.fingerprint10
      });
    }
    return patchRequest;
  }

  copyConsultation() {
    if(this.consultation.dateBirth  != null){
      this.consultation.dateBirth = this.consultation.dateBirth.substr(0, 10);
    }
    this.serverConsultation = new Consultation();
    this.serverConsultation.uuid = this.consultation.uuid;
    this.serverConsultation.firstname = this.consultation.firstname;
    this.serverConsultation.lastname = this.consultation.lastname;
    this.serverConsultation.email = this.consultation.email;
    this.serverConsultation.street = this.consultation.street;
    this.serverConsultation.streetNumber = this.consultation.streetNumber;
    this.serverConsultation.cap = this.consultation.cap;
    this.serverConsultation.city = this.consultation.city;
    this.serverConsultation.country = this.consultation.country;
    this.serverConsultation.phoneNumber = this.consultation.phoneNumber;
    this.serverConsultation.dateBirth = this.consultation.dateBirth;
    this.serverConsultation.gender = this.consultation.gender;
    this.serverConsultation.nationality = this.consultation.nationality;
    this.serverConsultation.civilStatus = this.consultation.civilStatus;
    this.serverConsultation.numberChild = this.consultation.numberChild;
    this.serverConsultation.professional = this.consultation.professional;
    this.serverConsultation.education = this.consultation.education;
    this.serverConsultation.fingerprint1 = this.consultation.fingerprint1;
    this.serverConsultation.fingerprint2 = this.consultation.fingerprint2;
    this.serverConsultation.fingerprint3 = this.consultation.fingerprint3;
    this.serverConsultation.fingerprint4 = this.consultation.fingerprint4;
    this.serverConsultation.fingerprint5 = this.consultation.fingerprint5;
    this.serverConsultation.fingerprint6 = this.consultation.fingerprint6;
    this.serverConsultation.fingerprint7 = this.consultation.fingerprint7;
    this.serverConsultation.fingerprint8 = this.consultation.fingerprint8;
    this.serverConsultation.fingerprint9 = this.consultation.fingerprint9;
    this.serverConsultation.fingerprint10 = this.consultation.fingerprint10;
    this.serverConsultation.handyman = this.consultation.handyman;
    this.serverConsultation.leader = this.consultation.leader;
    this.serverConsultation.organized = this.consultation.organized;
    this.serverConsultation.performer = this.consultation.performer;
    this.serverConsultation.communicator = this.consultation.communicator;
    this.serverConsultation.getstuck = this.consultation.getstuck;
    this.serverConsultation.doubts = this.consultation.doubts;
    this.serverConsultation.opposition = this.consultation.opposition;
    this.serverConsultation.passivity = this.consultation.passivity;
    this.serverConsultation.taciturn = this.consultation.taciturn;
    this.serverConsultation.supporter = this.consultation.supporter;
    this.serverConsultation.motivator = this.consultation.motivator;
    this.serverConsultation.mentor = this.consultation.mentor;
    this.serverConsultation.innovator = this.consultation.innovator;
    this.serverConsultation.catalyst = this.consultation.catalyst;
    this.serverConsultation.retreat = this.consultation.retreat;
    this.serverConsultation.tobear = this.consultation.tobear;
    this.serverConsultation.guiltfeelings = this.consultation.guiltfeelings;
    this.serverConsultation.hide = this.consultation.hide;
    this.serverConsultation.submit = this.consultation.submit;
    this.serverConsultation.harmonizer = this.consultation.harmonizer;
    this.serverConsultation.enterprising = this.consultation.enterprising;
    this.serverConsultation.mediator = this.consultation.mediator;
    this.serverConsultation.enthusiast = this.consultation.enthusiast;
    this.serverConsultation.defender = this.consultation.defender;
    this.serverConsultation.userUUID = this.consultation.userUUID;
    
    this.isAuthenticatedUser = this.consultation.userUUID === this.accessTokenService.getUserUUID();
    this.beURL_MMT = environment.baseUrl + environment.baseAPIPath + '/consultation/' + this.consultation.uuid;
    
  }

  exportConsultation(){
		this.consultationService.exportConsultation(this.consultationUUID).subscribe((res: any) => {
			let filename = res.headers.get('Content-Disposition').split(';')[1].split('filename')[1].split('=')[1].trim();
			let blob = new Blob([res.body], {type: res.headers.get('Content-Type')});
			let fileUrl = URL.createObjectURL(blob);
			let a = document.createElement('a');
			a.href = fileUrl;
			a.download = filename;
			let e = document.createEvent('MouseEvents');
			e.initMouseEvent('click', true, false, document.defaultView, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
			a.dispatchEvent(e);
			a.remove();
		},
		(error: any) => {
			this.toastService.error(this.translateService.instant('message.error'));
		});
  }

  deleteConsultation() {
    this.consultationService
      .deleteConsultation(this.consultationUUID)
      .subscribe(
        response => {
          this.toastService.success(
            this.translateService.instant("message.delete.ok")
          );
          this.goBack();
        },
        err => {
          if (err.status === 403) {
            this.toastService.error(
              this.translateService.instant("message.delete.forbidden")
            );
          } else {
            this.toastService.error(
              this.translateService.instant("message.delete.error")
            );
          }
        }
      );
  }

  uploadFile(files: FileList, fingerNo: string) {
    if (files[0].size > 1023356) {
      this.toastService.error(
        this.translateService.instant("message.upload.maxsize")
      );
    } else {
      this.consultationService
        .uploadFile(this.consultationUUID, files, fingerNo)
        .subscribe(
          response => {
            this.toastService.success(
              this.translateService.instant("message.upload.ok")
            );
            this.getConsultationDetails(this.consultationUUID).subscribe(
              consultation => {
                this.consultation = consultation;
                this.copyConsultation();
                onBodyLoad();

                var canvas = <HTMLCanvasElement> document.getElementById('fingerframe' + fingerNo);
                var context = canvas.getContext("2d");

                var reader= new FileReader();
                reader.onload = function(e) {
                  var img: any = <HTMLImageElement> new Image();
                  img.addEventListener("load", function() {
                    context.drawImage(img, 0, 0 ,img.width, img.height, 0, 0, 100, 150);
                  });
                  img.src = reader.result;
                };       
                reader.readAsDataURL(files[0] );
              },
              err => {
                this.toastService.error(
                  this.translateService.instant("message.error")
                );
              }
            );
          },
          err => {
            if (err.status === 403) {
              this.toastService.error(
                this.translateService.instant("message.upload.forbidden")
              );
            } else {
              this.toastService.error(
                this.translateService.instant("message.upload.error")
              );
            }
          }
        );
    }
  }

  updateConsultation(){
    this.getConsultationDetails(this.consultationUUID).subscribe(
      consultation => {
        this.consultation = consultation;
        this.copyConsultation();
        onBodyLoad();
        
      },
      err => {
        this.toastService.error(
          this.translateService.instant("message.error")
        );
      }
    );
  }

  errorshow(){
    this.toastService.error(
      this.translateService.instant("message.error")
    );
  }


  callScanner(finger: string){
    const BEurl=environment.baseUrl+  environment.baseAPIPath+ '/consultation/' + this.consultationUUID ;
    this.authenticationService.refreshExpiredToken();
    beginOperation('capture', finger, BEurl, this.accessTokenService.credentials.token, this.userLanguage);
  }

}
