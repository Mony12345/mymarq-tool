import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../shared/model/user';
import { UserDataSource } from './../shared/datasources/user.datasource';
import { UserSearchQuery, UserService } from './../shared/services/user.service';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.css']
})

export class UserListComponent implements AfterViewInit, OnInit {

    displayedColumns: string[];
    dataSource: UserDataSource;
    searchQuery: UserSearchQuery;

    readonly pageSize = environment.tablePageSize;

    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

    constructor(public userService: UserService,
        private router: Router) {

        this.displayedColumns = ['email', 'role', 'createdAt', 'enabled'];

    }

    ngOnInit() {
        this.searchQuery = new UserSearchQuery();
        this.dataSource = new UserDataSource(this.userService);
        this.dataSource.loadUsers(0, this.pageSize, this.searchQuery);
    }

    ngAfterViewInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadUsersPage())
            )
            .subscribe();
    }

    loadUsersPage() {
        this.dataSource.loadUsers(
            this.paginator.pageIndex,
            this.pageSize,
            this.searchQuery
        );
    }

    openDetail(user: User) {
        this.router.navigate(['/user/detail/', user.uuid]);
    }

    createNewUser() {
        this.router.navigate(['/user/detail']);
    }
}
