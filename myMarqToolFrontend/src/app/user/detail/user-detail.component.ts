import { AuthenticationService } from './../../core/authentication/authentication.service';
import { LanguageTranslationModule, ChangeLanguageComponent } from './../../shared/modules/language-translation/language-translation.module';
import { PasswordValidators, ConfirmValidParentMatcher, regExps, errorMessages } from '../../core/validator/password-validation.';
import { Component, NgModule, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { AccessTokenService, Role } from './../../core/authentication/access-token.service';
import { ReadingJsonService } from './../../core/json/reading-json.service';
import { User } from './../../shared/model/user';
import { UserService } from './../../shared/services/user.service';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule
  ]
})
@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})

export class UserDetailComponent extends ChangeLanguageComponent implements OnInit {

  panelOpenState = true;
  roles = Object.keys(Role).map(role => role.toLowerCase().replace('_', ''));

  changePasswordForm: FormGroup;
  confirmValidParentMatcher = new ConfirmValidParentMatcher();
  errors = errorMessages;

  user: User;
  serverUser: User;
  userUUID: string;
  isAuthenticatedUser: boolean;
  isAdmin: boolean;
  userLanguageUpdate: boolean;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private toastService: ToastrService,
    private translateService: TranslateService,
    private formBuilder: FormBuilder,
    authenticationService: AuthenticationService,
    readingJsonService: ReadingJsonService,
    languageTranslationModule: LanguageTranslationModule) {

    super(authenticationService, readingJsonService, languageTranslationModule, (language: string): void => {
      if (this.user && this.isAuthenticatedUser) {
        this.user.language = language;
      }

    });
    this.isAdmin = authenticationService.accessTokenService.isAdmin();
    this.userLanguageUpdate = false;
  }

  ngOnInit() {
    this.user = new User();
    this.serverUser = new User();
    this.userUUID = this.route.snapshot.params['uuid'];
    this.isAuthenticatedUser = this.userUUID === this.authenticationService.accessTokenService.getUserUUID();

    this.createChangePasswordForm();

    if (this.userUUID != null) {
      this.getUserDetails(this.userUUID).subscribe(user => {
        this.user = user;
        this.copyUser();
      }, err => {
        this.toastService.error(this.translateService.instant('message.error'));
      });
    }
  }

  getUserDetails(userUUID) {
    return this.userService.getUser(userUUID, !this.isAuthenticatedUser);
  }

  goBack() {
    this.router.navigate(['user']);
  }

  saveUser() {
    if (this.user.email != null && this.user.email !== '') {
      if (this.user.uuid != null) {
        const patchRequest = this.mapPatchRequest();
        if (Array.isArray(patchRequest) && patchRequest.length) {
          this.userService.updateUser(this.user.uuid, patchRequest, !this.isAuthenticatedUser).subscribe(response => {
            this.toastService.success(this.translateService.instant('message.save.ok'));
            if (this.userLanguageUpdate) {
              super.changeLang(this.user.language, false);
              this.userLanguageUpdate = false;
            }

            this.goBack();
          }, err => {
            this.toastService.error(this.translateService.instant('message.save.error'));
          });
        } else {
          this.toastService.info(this.translateService.instant('message.save.nothing'));
        }
      } else {
        this.user.password = this.changePasswordForm.value.passwordGroup.newPassword;
        this.userService.createUser(this.user).subscribe(response => {
          this.toastService.success(this.translateService.instant('message.save.ok'));
          this.goBack();
        }, err => {
          if(err.error.message == "User already exists"){
            this.toastService.error(this.translateService.instant('message.save.error.duplicate'));
          } else{
          this.toastService.error(this.translateService.instant('message.save.error'));
          }
        });
      }
    }
  }

  mapPatchRequest(): any {
    const patchRequest = [];
    if (this.serverUser.email !== this.user.email) {
      patchRequest.push({ op: 'replace', path: '/email', value: this.user.email });
    }
    if (this.serverUser.firstname !== this.user.firstname) {
      patchRequest.push({ op: 'replace', path: '/firstname', value: this.user.firstname });
    }
    if (this.serverUser.lastname !== this.user.lastname) {
      patchRequest.push({ op: 'replace', path: '/lastname', value: this.user.lastname });
    }
    if (this.serverUser.language !== this.user.language) {
      patchRequest.push({ op: 'replace', path: '/language', value: this.user.language });
      if (this.isAuthenticatedUser) {
        this.userLanguageUpdate = true;
      }
    }
    if (this.serverUser.role !== this.user.role) {
      patchRequest.push({ op: 'replace', path: '/role', value: this.user.role });
    }
    if (this.serverUser.enabled !== this.user.enabled) {
      patchRequest.push({ op: 'replace', path: '/enabled', value: this.user.enabled });
    }
    if (this.changePasswordForm && this.changePasswordForm.value.passwordGroup.newPassword !== '') {
      if (this.changePasswordForm.valid) {
        patchRequest.push({ op: 'replace', path: '/password', value: this.changePasswordForm.value.passwordGroup.newPassword });
      }
    }


    return patchRequest;
  }

  copyUser() {
    this.serverUser = new User();
    this.serverUser.uuid = this.user.uuid;
    this.serverUser.email = this.user.email;
    this.serverUser.password = this.user.password;
    this.serverUser.firstname = this.user.firstname;
    this.serverUser.lastname = this.user.lastname;
    this.serverUser.language = this.user.language;
    this.serverUser.role = this.user.role;
    this.serverUser.enabled = this.user.enabled;
  }

  deleteUser() {
    this.userService.deleteUser(this.userUUID).subscribe(response => {
      this.toastService.success(this.translateService.instant('message.delete.ok'));
      this.goBack();
    }, err => {
      if (err.status === 403) {
        this.toastService.error(this.translateService.instant('message.delete.forbidden'));
      } else {
        this.toastService.error(this.translateService.instant('message.delete.error'));
      }
    });
  }

  createChangePasswordForm() {
      this.changePasswordForm = this.formBuilder.group({
        passwordGroup: this.formBuilder.group({
          newPassword: ['', [
            Validators.required,
            Validators.pattern(regExps.password)
          ]],
          confirmPassword: ['', Validators.required]
        }, { validator: PasswordValidators.childrenEqual })
      });
    
  }

  disableSaveButton(): boolean {
    if (!this.userUUID) {
      return this.changePasswordForm.invalid;
    } else {
      if (this.isAuthenticatedUser) {
        if (this.changePasswordForm.value.passwordGroup.newPassword) {
          return this.changePasswordForm.invalid;
        }
      }
    }

    return false;
  }
}
