import { AuthenticationService } from './../../core/authentication/authentication.service';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuardAdmin implements CanActivate {
    constructor(private router: Router, private authenticationService: AuthenticationService) { }

    canActivate() {
        if (this.authenticationService.isAdmin()) {
            return true;
        }

        return false;
    }
}
