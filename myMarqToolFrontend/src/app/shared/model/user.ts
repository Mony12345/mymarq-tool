export class User {
    uuid: string;
    email: string;
    password: string;
    firstname: string;
    lastname: string;
    language: string;
    role: string;
    enabled: boolean;
    lastUpdateAt: string;
    createdAt: string;
}
