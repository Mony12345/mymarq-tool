import { CollectionViewer } from '@angular/cdk/collections';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { Consultation } from './../model/consultation';
import { ConsultationSearchQuery, ConsultationService } from './../services/consultation.service';

export class ConsultationDataSource implements DataSource<Consultation> {

    private consultations = new BehaviorSubject<Consultation[]>([]);
    private loadingConsultations = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingConsultations.asObservable();
    public maxResultCount: any = 0;

    constructor(private consultationService: ConsultationService) { }

    connect(collectionViewer: CollectionViewer): Observable<Consultation[]> {
        return this.consultations.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.consultations.complete();
        this.loadingConsultations.complete();
    }

    loadConsultations(from: number, pageSize: number, searchQuery: ConsultationSearchQuery) {

        this.loadingConsultations.next(true);

        this.consultationService.searchConsultations(from, pageSize, searchQuery).pipe(
            catchError(() => of([])),
            finalize(() => this.loadingConsultations.next(false))
        ).subscribe((response: any) => {
            // Mark first call
            if (response.status === 206 || response.status === 200) {
                console.log(response.headers);
                this.maxResultCount = parseInt(response.headers.get('Content-Range').split('/')[1], 10);
                this.consultations.next(response.body);
            } else if (response.status === 204) {
                // 204 = nothing found
                this.consultations.next([]);
            }
        });

    }
}
