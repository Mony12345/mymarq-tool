import { catchError, finalize } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { CollectionViewer } from '@angular/cdk/collections';
import { UserService, UserSearchQuery } from './../services/user.service';
import { BehaviorSubject, of } from 'rxjs';
import { DataSource } from '@angular/cdk/table';
import { User } from './../model/user';

export class UserDataSource implements DataSource<User> {

    private users = new BehaviorSubject<User[]>([]);
    private loadingUsers = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingUsers.asObservable();
    public maxResultCount: any = 0;

    constructor(private userService: UserService) { }

    connect(collectionViewer: CollectionViewer): Observable<User[]> {
        return this.users.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.users.complete();
        this.loadingUsers.complete();
    }

    loadUsers(from: number, pageSize: number, searchQuery: UserSearchQuery) {

        this.loadingUsers.next(true);

        this.userService.searchUsers(from, pageSize, searchQuery).pipe(
            catchError(() => of([])),
            finalize(() => this.loadingUsers.next(false))
        ).subscribe((response: any) => {
            // Mark first call
            if (response.status === 206 || response.status === 200) {
                console.log(response.headers);
                this.maxResultCount = parseInt(response.headers.get('Content-Range').split('/')[1], 10);
                this.users.next(response.body);
            } else if (response.status === 204) {
                // 204 = nothing found
                this.users.next([]);
            }
        });

    }
}
