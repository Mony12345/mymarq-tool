import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { environment } from '../../../environments/environment';
import { Consultation } from './../model/consultation';
import { ConsultationService } from './consultation.service';




describe('ConsultationService', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    let consultationService: ConsultationService;

    beforeEach(() => {

        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [ConsultationService]
        });

        httpClient = TestBed.get(HttpClient);
        httpTestingController = TestBed.get(HttpTestingController);
        consultationService = TestBed.get(ConsultationService);
    });

    afterEach(() => {
        httpTestingController.verify();
    });


    it('should get consultation', (done) => {
        const expectedConsultation: Consultation = new Consultation();
        expectedConsultation.uuid = 'aaca28eb-221d-461c-be29-6b46d8a98727';
        expectedConsultation.firstname = 'consultation';
        expectedConsultation.lastname = 'test';
        expectedConsultation.email = 'consultation@mymarq.ch';

        consultationService.getConsultation(expectedConsultation.uuid).subscribe(
            consultation => {
                expect(consultation.uuid).toEqual(expectedConsultation.uuid);
                expect(consultation.firstname).toEqual(expectedConsultation.firstname);
                expect(consultation.lastname).toEqual(expectedConsultation.lastname);
                expect(consultation.email).toEqual(expectedConsultation.email);
                done();
            });

        // const request = httpTestingController.expectOne('/courses.json');
        const request = httpTestingController.expectOne(environment.baseUrl + environment.baseAPIPath + '/consultation/' + expectedConsultation.uuid);
        request.flush(expectedConsultation);
    });
});
