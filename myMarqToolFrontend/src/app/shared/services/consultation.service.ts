import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { Consultation } from '../model/consultation';
import { DomSanitizer } from '@angular/platform-browser';

export class ConsultationSearchQuery {
    firstname: string;
    lastname: string;
}

@Injectable({
    providedIn: 'root'
})
export class ConsultationService {

    // Base url
    baseurl = environment.baseUrl;
    basePath = environment.baseAPIPath;
    baseAdminPath = environment.baseAdminAPIPath;
    patchHeaders: HttpHeaders;

    constructor(private http: HttpClient,
        private domSanitizer: DomSanitizer) {
        this.patchHeaders = new HttpHeaders().set('Content-Type', 'application/json-patch+json');
    }

    // Http Headers
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    // search consultations
    searchConsultations(pageNumber: number, pageSize: number, searchQuery: ConsultationSearchQuery): any {
        const endIndex = pageSize - 1;
        let headers = new HttpHeaders();
        const from = pageNumber * pageSize;
        headers = headers.set('Range', 'members=' + from + '-' + (from + endIndex));

        return this.http.post<any>(this.baseurl +  this.basePath + '/consultation/search/',
            searchQuery,
            { observe: 'response', headers: headers }
        );
    }

    // get Consultation
    getConsultation(consultationUUID: string): Observable<Consultation> {
        return this.http.get<Consultation>(this.baseurl +  this.basePath + '/consultation/' + consultationUUID);
    }

    // export Consultation
    exportConsultation(consultationUUID: string){
        let headers = new HttpHeaders();
        return this.http.get(this.baseurl +  this.basePath + '/consultation/' + consultationUUID + '/export', {  responseType: 'blob',  observe: 'response', headers: headers });
    
    }

    // export Consultations
    exportConsultations(){
        let headers = new HttpHeaders();
        return this.http.get(this.baseurl +  this.basePath + '/consultation/exportList', {  responseType: 'blob',  observe: 'response', headers: headers });
    
    }

    // delete Consultation
    deleteConsultation(consultationUUID: string): any {
        return this.http.delete<any>(this.baseurl + this.basePath + '/consultation/' + consultationUUID);
    }

    // save new consultation
    createConsultation(consultation: Consultation) {
        return this.http.post(this.baseurl + this.basePath + '/consultation/',
            consultation, { observe: 'response' }
        );
    }

    // update consultation
    updateConsultation(consultationUUID: string, patchRequest: any) {
        return this.http.patch(this.baseurl +  this.basePath+ '/consultation/' + consultationUUID,
            patchRequest,
            { headers: this.patchHeaders }
        );
    }

       // Error handling
    errorHandle(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }


    uploadFile(consultationUUID: string, files: FileList, fingerNo: string){
        let headers = new HttpHeaders();
        headers.set('Content-Type', null);
        headers.set('Accept', "multipart/form-data");
        let params = new HttpParams();
        const formData: FormData = new FormData();
        formData.append('file', files[0], files[0].name);

        return this.http.post(this.baseurl +  this.basePath+ '/consultation/' + consultationUUID + '/' + fingerNo,
        formData, { params, headers }
        );
    }


    getFingerImage(consultationUUID: string, fingerNo: string){
        return new Observable((observer: any) => {
            return this.http.get(this.baseurl +  this.basePath+ '/consultation/' + consultationUUID + '/' + fingerNo , {observe: 'body', responseType: 'blob'}).subscribe(data => {
                if (data) {
                    let blob = new Blob([data], {type: data.type});
                    let reader = new FileReader();
                    var canvas = <HTMLCanvasElement> document.getElementById('fingerframe' + fingerNo);
                    var context = canvas.getContext("2d");

                    reader.onload = function(e) {
                        var img : any = new Image();
                        img.addEventListener("load", function() {
                          context.drawImage(img, 0, 0 ,img.width, img.height, 0, 0, 100, 150);
                        });
                        img.src = reader.result;
                      }; 

                    if (data) {
                        reader.readAsDataURL(data);
                    }

                } else {
                    observer.complete();
                }
            }, (error) => {
                observer.complete();
            });
        });
        
    }

}
