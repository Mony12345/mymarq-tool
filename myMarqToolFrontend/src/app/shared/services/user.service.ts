import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { throwError, Observable } from 'rxjs';
import { User } from '../model/user';

export class UserSearchQuery {
    email: string;
    firstname: string;
    lastname: string;
    language: string;
    role: string;
    disabled: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class UserService {

    // Base url
    baseurl = environment.baseUrl;
    basePath = environment.baseAPIPath;
    baseAdminPath = environment.baseAdminAPIPath;
    patchHeaders: HttpHeaders;

    constructor(private http: HttpClient) {
        this.patchHeaders = new HttpHeaders().set('Content-Type', 'application/json-patch+json');
    }

    // Http Headers
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    // search users
    searchUsers(pageNumber: number, pageSize: number, searchQuery: UserSearchQuery): any {
        const endIndex = pageSize - 1;
        let headers = new HttpHeaders();
        const from = pageNumber * pageSize;
        headers = headers.set('Range', 'members=' + from + '-' + (from + endIndex));

        return this.http.post<any>(this.baseurl + this.baseAdminPath + '/user/search/',
            searchQuery,
            { observe: 'response', headers: headers }
        );
    }

    // get User
    getUser(userUUID: string, isAdmin: boolean): Observable<User> {
        return this.http.get<User>(this.baseurl + (isAdmin ? this.baseAdminPath : this.basePath) + '/user/' + userUUID);
    }

    // delete User
    deleteUser(userUUID: string): any {
        return this.http.delete<any>(this.baseurl + this.baseAdminPath + '/user/' + userUUID);
    }

    // save new user
    createUser(user: User) {
        return this.http.post(this.baseurl + this.baseAdminPath + '/user/',
            user, { observe: 'response' }
        );
    }

    // update user
    updateUser(userUUID: string, patchRequest: any, isAdmin: boolean) {
        return this.http.patch(this.baseurl + (isAdmin ? this.baseAdminPath : this.basePath) + '/user/' + userUUID,
            patchRequest,
            { headers: this.patchHeaders }
        );
    }

    // get User
    getUserList(): any {
        return this.http.get<any>(this.baseurl + this.basePath + '/user/list');
    }

    // Error handling
    errorHandle(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }
}
