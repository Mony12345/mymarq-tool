import { HttpClient } from '@angular/common/http';
/**
 * This module is used to language translations.
 * The translations are saved in a json file in /src/app/assets/i18n directory
 * Docs: https://www.codeandweb.com/babeledit/tutorials/how-to-translate-your-angular7-app-with-ngx-translate
 */
import { NgModule } from '@angular/core';
// import ngx-translate and the http loader
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from './../../../core/authentication/authentication.service';
import { UserService } from './../../services/user.service';
import { ReadingJsonService } from './../../../core/json/reading-json.service';


// ngx-translate - required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

@NgModule({
    declarations: [],
    imports: [
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    exports: [
        TranslateModule
    ],
})
export class LanguageTranslationModule {

    componentsWhoNeedRefresh: ChangeLanguageComponent[];

    constructor(
        private translate: TranslateService,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private toastrService: ToastrService
    ) {
        this.componentsWhoNeedRefresh = [];
        if (authenticationService.accessTokenService.getUserLanguage()) {
            translate.use(authenticationService.accessTokenService.getUserLanguage());
        } else {
            // Gets Default language from browser if available, otherwise set English ad default
            this.translate.addLangs(['en', 'it', 'de']);
            this.translate.setDefaultLang('en');
            const browserLang = this.translate.getBrowserLang();
            this.translate.use(browserLang.match(/en|it|de/) ? browserLang : 'en');
        }
    }

    changeLanguage(language: string, saveToBackEnd: boolean) {
        if (saveToBackEnd) {
            const patchRequest = [];
            patchRequest.push({ op: 'replace', path: '/language', value: language });

            // save selected language to user profile
            this.userService.updateUser(this.authenticationService.accessTokenService.getUserUUID(), patchRequest, false)
                .subscribe(response => {
                    this.toastrService.success(this.translate.instant('message.save.ok'));
                    this.refreshUserTokenAndRereshComponents(language);

                }, err => {
                    this.toastrService.error(this.translate.instant('message.save.error'));
                });
        } else {
            this.refreshUserTokenAndRereshComponents(language);
        }
    }

    refreshUserTokenAndRereshComponents(language: string) {
        // refresh user token
        this.authenticationService.refreshToken().subscribe(resp => {
            this.translate.use(language);
            localStorage.setItem('language', language);
            this.refreshComponents();
        });
    }

    refreshComponents() {
        if (this.componentsWhoNeedRefresh && this.componentsWhoNeedRefresh.length > 0) {
            this.componentsWhoNeedRefresh.forEach(component => component.refresh(false));
        }
    }

    registerComponentToBeRefreshed(component: ChangeLanguageComponent) {
        this.componentsWhoNeedRefresh.push(component);
    }
}

export class ChangeLanguageComponent {
    public languages: any;
    public userLanguage: string;

    constructor(
        protected authenticationService: AuthenticationService,
        private readingJsonService: ReadingJsonService,
        private languageTranslationModule: LanguageTranslationModule,
        private additionalOperation: (language: string) => any
    ) {
        this.languageTranslationModule.registerComponentToBeRefreshed(this);
        this.refresh(true);
    }

    public refresh(init: boolean) {
        this.userLanguage = this.authenticationService.accessTokenService.getUserLanguage();
        this.readingJsonService.getLanguages(this.userLanguage).subscribe(data => {
            this.languages = data;
        }, error => {
            console.log(error);
        });

        if (!init && this.additionalOperation) {
            this.additionalOperation(this.userLanguage);
        }
    }

    protected changeLang(language: string, saveToBackEnd: boolean) {
        this.languageTranslationModule.changeLanguage(language, saveToBackEnd);
    }

    protected getAuthenticationService(): AuthenticationService {
        return this.authenticationService;
    }
}
