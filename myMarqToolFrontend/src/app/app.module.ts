import { AuthGuardAdmin } from './shared/guard/auth.guard.admin';
import { LoginComponent } from './login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { HttpCustomInterceptor } from './core/http/http.interceptor';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthGuard } from './shared/guard';
import { LanguageTranslationModule } from './shared/modules/language-translation/language-translation.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        ComponentsModule,
        RouterModule,
        AppRoutingModule,
        HttpClientModule,
        ToastrModule.forRoot({
            timeOut: 2000,
            positionClass: 'toast-top-center',
            preventDuplicates: true,
        }),
        LanguageTranslationModule,
        MatDialogModule,
        BrowserModule,
        TranslateModule
    ],
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        LoginComponent

    ],
    providers: [AuthGuard, 
        AuthGuardAdmin,
         { provide: HTTP_INTERCEPTORS, useClass: HttpCustomInterceptor, multi: true } ],
    bootstrap: [AppComponent]
})
export class AppModule { }
