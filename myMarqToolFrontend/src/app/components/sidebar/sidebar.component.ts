import { LanguageTranslationModule, ChangeLanguageComponent } from './../../shared/modules/language-translation/language-translation.module';
import { ToastrService } from 'ngx-toastr';
import { UserService } from './../../shared/services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ReadingJsonService } from './../../core/json/reading-json.service';
import { AuthenticationService } from './../../core/authentication/authentication.service';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    visible: boolean;
}
export const ROUTES: RouteInfo[] = [
    { path: '/consultation', title: 'consultations', icon: 'apartment', class: '', visible: true },
    { path: '/consultation/detail', title: 'consultation.detail', icon: 'apartment', class: '', visible: false }
];
export const ADMI_ROUTES: RouteInfo[] = [
    { path: '/user', title: 'users', icon: 'group', class: '', visible: true },
    { path: '/user/detail', title: 'user.detail', icon: 'group', class: '', visible: false }
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent extends ChangeLanguageComponent implements OnInit {
    menuItems: any[];
    adminMenuItems: any[];
    isAdmin: boolean;

    constructor(private router: Router,
        private translate: TranslateService,
        private userService: UserService,
        private toastrService: ToastrService,
        private translateService: TranslateService,
        authenticationService: AuthenticationService,
        readingJsonService: ReadingJsonService,
        languageTranslationModule: LanguageTranslationModule) {

        super(authenticationService, readingJsonService, languageTranslationModule, null);
        this.isAdmin = authenticationService.accessTokenService.isAdmin();
    }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem.visible);

        if (this.isAdmin) {
            this.adminMenuItems = ADMI_ROUTES.filter(adminMenuItem => adminMenuItem.visible);
        }
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }

    openProfile() {
        this.router.navigate(['/user/detail/', super.getAuthenticationService().accessTokenService.getUserUUID]);
    }
}
