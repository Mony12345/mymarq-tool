var fpHTTSrvOpEP = "http://127.0.0.1:15270/fpoperation"

var btnCapture;
var textResult;
var fingerFrame;
var finger = 0;
var beURL_MMT;
var token;
var language;
var newCanvas;
var lastInitOp;


function sendImage() {
    var req = new XMLHttpRequest();
    req.timeout = 2000;
    req.onreadystatechange = function(e) {
        if (req.readyState === 4) {
            if (req.status === 200) {
                document.getElementById("updateCons").click();
            } else {
                document.getElementById("problemCons").click();
            }
        }
    }
    req.open("POST", beURL_MMT + '/' + finger);
    req.setRequestHeader('Authorization', 'Bearer ' + token)
    var img = newCanvas.toDataURL("image/png");
    img = img.replace(/^data:image\/(png|jpg);base64,/, "")
    req.send(img);
}

function fixError(errorText) {
    textResult = document.getElementById("result" + finger);
    textResult.style = "color:red"
    textResult.innerHTML = errorText;
}

function setAskTest(textMes) {
    textResult = document.getElementById("result" + finger);
    textResult.style = "color:blue"
    textResult.innerHTML = textMes;
}

function setOperationResult(textMes) {
    textResult = document.getElementById("result" + finger);
    textResult.style = "color:green"
    textResult.innerHTML = textMes;
}

function beginOperation(opName, f, beURL, t, l) {
    finger = f
    beURL_MMT = beURL;
    token = t
    language = l;
    fingerFrame = document.getElementById("fingerframe" + f);

    var json = JSON.stringify({ operation: opName, lfd: "no", invert: "yes" });
    enableControlsForOp(true);


    var req = new XMLHttpRequest();
    req.open("POST", fpHTTSrvOpEP);
    req.setRequestHeader('Content-type', 'application/json; charset=utf-8');

    req.onload = function() {
        if (req.status == 200) {
            t_view = "Operation begin";
            if (language == 'it') { t_view = "Inizio operazione"; } else if (language == 'de') { t_view = "Operation beginnt"; }

            setAskTest(t_view);
            parseOperationDsc(JSON.parse(req.response));
        } else {
            t_view = "Error";
            if (language == 'it') { t_view = "Errore"; } else if (language == 'de') { t_view = "Fehler"; }
            fixError(t_view);
            enableControlsForOp(false);

        }
    };
    req.onerror = function() {
        t_view = "FPHttpServer not available";
        if (language == 'it') { t_view = "FPHttpServer non disponibile"; } else if (language == 'de') { t_view = "FPHttpServer nicht verfügbar"; }
        for (i = 1; i <= 10; i++) {
            finger = i
            fixError(t_view);
            enableControlsForOp(false);
        }
    };
    req.send(json);
}


function getOperationState(opId) {
    var url = fpHTTSrvOpEP + '/' + opId;
    var req = new XMLHttpRequest();
    req.open('GET', url);
    req.onload = function() {
        if (req.status == 200) {
            if (req.readyState == 4) {
                parseOperationDsc(JSON.parse(req.response));
            }
        } else {
            t_view = "Error";
            if (language == 'it') { t_view = "Errore"; } else if (language == 'de') { t_view = "Fehler"; }
            fixError(t_view);
            enableControlsForOp(false);

        }
    };
    req.onerror = function() {
        t_view = "FPHttpServer not available";
        if (language == 'it') { t_view = "FPHttpServer non disponibile"; } else if (language == 'de') { t_view = "FPHttpServer nicht verfügbar"; }
        for (i = 1; i <= 10; i++) {
            finger = i
            fixError(t_view);
            enableControlsForOp(false);
        }
    };
    req.send();
}

function getOperationImg(opId, frameWidth, frameHeight) {
    var url = fpHTTSrvOpEP + '/' + opId + '/image';
    var req = new XMLHttpRequest();
    req.open('GET', url);
    req.onload = function() {
        if (req.status == 200) {
            imageByte = new Uint8Array(req.response);
            drawFingerFrame(new Uint8Array(req.response), opId, frameWidth, frameHeight);
        } else {
            enableControlsForOp(false);

        }
    };
    req.onerror = function() {
        enableControlsForOp(false);
    };
    req.send();
    req.responseType = "arraybuffer";
}

function parseOperationDsc(opDsc) {
    var res = true;

    if (opDsc.state == 'done') {
        enableControlsForOp(false);

        if (opDsc.status == 'success') {
            t_view = "Fingerprint acquired";
            if (language == 'it') { t_view = "Impronta acquisita"; } else if (language == 'de') { t_view = "Fingerabdruck erfasst"; }
            setOperationResult(t_view);
            if (opDsc.operation == 'capture') {
                setTimeout(sendImage, 500);
            }
        }

        if (opDsc.status == 'fail') {
            t_view = opDsc.errorstr;
            if (opDsc.errornum == 2) {
                if (language == 'it') { t_view = "Dispositivo non collegato"; } else if (language == 'de') { t_view = "Gerät nicht angeschlossen"; }
            } else {
                t_view = "Error";
                if (language == 'it') { t_view = "Errore"; } else if (language == 'de') { t_view = "Fehler"; }
            }
            fixError(t_view)
            res = false;
        }
    } else if (opDsc.state == 'init') {
        lastInitOp = opDsc.id
        setTimeout(getOperationState, 1000, opDsc.id);
        setTimeout(getOperationImg, 1000, opDsc.id, parseInt(opDsc.devwidth), parseInt(opDsc.devheight));
    } else if (opDsc.state == 'inprogress') {
        if (opDsc.fingercmd == 'puton') {
            t_view = "Put finger on scanner";
            if (language == 'it') { t_view = "Metti il dito sullo scanner"; } else if (language == 'de') { t_view = "Legen Sie den Finger auf den Scanner"; }
            setAskTest(t_view);
        }
        setTimeout(getOperationState, 1000, opDsc.id);
        setTimeout(getOperationImg, 1000, opDsc.id, parseInt(opDsc.devwidth), parseInt(opDsc.devheight));
    }
    return res;
}

function drawFingerFrame(frameBytes, opId, frameWidth, frameHeight) {
    var ctx = fingerFrame.getContext('2d');

    newCanvas = $("<canvas>")
        .attr("width", 320)
        .attr("height", 480)[0];
    var context = newCanvas.getContext("2d");
    var imgData = context.createImageData(320, 480);

    for (var i = 0; i < frameBytes.length; i++) {
        // red
        imgData.data[4 * i] = frameBytes[i];
        // green
        imgData.data[4 * i + 1] = frameBytes[i];
        // blue
        imgData.data[4 * i + 2] = frameBytes[i];
        //alpha
        imgData.data[4 * i + 3] = 255;
    }

    context.putImageData(imgData, 0, 0, 0, 0, 320, 480);
    ctx.drawImage(newCanvas, 0, 0, 320, 480, 0, 0, 100, 150);
}



function enableControlsForOp(opBegin) {
    btnCapture = document.getElementById("CaptureBtn" + finger);
    btnCapture.disabled = opBegin;
}

function CheckFPHttpSrvConnection() {
    var req = new XMLHttpRequest();
    req.open('GET', fpHTTSrvOpEP);
    req.onload = function() {
        if (req.status == 200) {
            t_view = "Ready";
            if (language == 'it') { t_view = "Pronto"; } else if (language == 'de') { t_view = "Bereit"; }
            var i;
            for (i = 1; i <= 10; i++) {
                finger = i
                enableControlsForOp(false);
                setAskTest(t_view);
            }

        }
    };
    req.onerror = function() {
        var i;
        t_view = "FPHttpServer not available";
        if (language == 'it') { t_view = "FPHttpServer non disponibile"; } else if (language == 'de') { t_view = "FPHttpServer nicht verfügbar"; }
        for (i = 1; i <= 10; i++) {
            finger = i
            fixError(t_view);
        }
        setTimeout(CheckFPHttpSrvConnection, 1000);
    };
    req.send();
}

function onBodyLoad(l) {
    language = l;
    CheckFPHttpSrvConnection();
}